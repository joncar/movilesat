## PARAMS
## LAT DOUBLE (11,6)
## LNG DOUBLE (11,6)
## COORDS VARCHAR(255) FORMAT: (Lat,Lng)
BEGIN
DECLARE distance DOUBLE(11,6);
DECLARE _Lat DOUBLE(11,6);
DECLARE _Lng DOUBLE(11,6);

SELECT getPosition('Lat',Coords), getPosition('Lng',Coords) INTO _Lat,_Lng;

SELECT(3959*acos(cos(radians(Lat))*cos(radians(_Lat))*cos(radians(_Lng)-radians(Lng))+sin(radians(Lat))*sin(radians(_Lat)))) INTO distance;


RETURN distance;
END