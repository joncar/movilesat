<!DOCTYPE html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Moviles SAT®' : $title ?></title>
	<meta charset="utf-8">
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($description) ?'': $description ?>" /> 	

	

	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<script>window.wURL = window.URL; var URL = '<?= base_url() ?>'; var lang = '<?= $_SESSION['lang'] ?>';</script>
	
	<!-- Fonts
	============================================= -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700%7CRaleway:100,200,300,400,500,600,700,800%7CDroid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- Stylesheets
	============================================= -->
	<link href="<?= base_url() ?>theme/theme/assets/css/external.css" rel="stylesheet">
	<!-- Extrnal CSS -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Boostrap Core CSS -->
	<link href="<?= base_url() ?>css/bootstrap.datepicker.css" rel="stylesheet">
	<link href="<?= base_url() ?>css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/css/calentim.min.css">
	<link href="<?= base_url() ?>theme/theme/assets/css/style_custom.css?v=1.1" rel="stylesheet">
	<!-- Style CSS -->
	<link rel="stylesheet" href="<?= base_url() ?>js/lightgallery/css/lightgallery.css"/>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
	      <script src="<?= base_url() ?>theme/theme/assets/js/html5shiv.js"></script>
	      <script src="<?= base_url() ?>theme/theme/assets/js/respond.min.js"></script>
	    <![endif]-->

	<!-- Document Title
	    ============================================= -->
	<script src="<?= base_url() ?>theme/theme/assets/js/jquery-2.1.1.min.js"></script>	
	<meta property="og:url"  content="<?= empty($url)?base_url():$url ?>" />
	<meta property="og:type"  content="article" />
	<meta property="og:title" content="<?= empty($title) ? 'Moviles SAT®' : $title ?>" />
	<meta property="og:description" content="<?= empty($description) ?'': $description ?>" />
	<meta property="og:image" content="<?= empty($image)?base_url().'theme/theme/assets/images/sliders/9.jpg': $image ?>" />
	<script src='https://www.google.com/recaptcha/api.js?render=6LdBXIwUAAAAAM-YueAzXx9UoUuDcVCMDcZTvhF2'></script>
	<link rel="stylesheet" href="<?= base_url() ?>cutter/example/css/slim.min.css">
</head>

<body>
	<a href="https://wa.me/34660074797/?text=Hola+deseo+contactarlos+por+informaci%C3%B3n+sobre+" class="boton_wats"><i class="fa fa-whatsapp"></i></a>
	<a href="<?= base_url() ?>tienda.html" class="boton_reparar"><i class="fa fa-wrench"></i> REPARA TU MOVIL</a>
	<?php 
		if(empty($editor)){
			$this->load->view($view); 
		}else{
			echo $view;
		}
	?>		

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '1838246033118930');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=1838246033118930&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<script>
		window.onload = function(){		
			<?php 
				if(empty($_SESSION['lat']) || empty($_SESSION['lng'])){
					echo 'localStorage.removeItem("lat"); localStorage.removeItem("lng");';
				}
			?>
			
			if(localStorage.lat==undefined || localStorage.lng==undefined){			
				navigator.geolocation.getCurrentPosition(function(e){
					localStorage.lat = e.coords.latitude;
					localStorage.lng = e.coords.longitude;					
					$.post(URL+'paginas/frontend/setPosition/'+localStorage.lat+'/'+localStorage.lng,{},function(data){
						data = JSON.parse(data);
						<?php if(!empty($link) && $link=='main'): ?>
							$("#footerAll").replaceWith(data.info);
						<?php endif ?>
					});				
				});
			}		
		};
	</script>
</body>
</html>

