<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getRedes($string){
		$str = array();
		foreach(explode(',',$string) as $r){
			if(strpos($r,'facebook')){
				$str[] = '<a href="'.$r.'" target="_new"><i class="fa fa-facebook"></i></a>';
			}
			if(strpos($r,'twitter')){
				$str[] = '<a href="'.$r.'" target="_new"><i class="fa fa-twitter"></i></a>';
			}
		}

		return $str;
	}

	function getMapa($string){
		$str = array();
		$string = str_replace(array('(',')'),array('',''),$string);
		foreach(explode(',',$string) as $r){
			$str[] = trim($r);
		}

		return $str;
	}

	function galeria($galeria_id){
		$gal = $this->db->get_where('galeria_fotos',array('galeria_id'=>$galeria_id));
		foreach($gal->result() as $n=>$v){
			$gal->row($n)->foto = base_url('img/galeria/'.$v->foto);
		}
		return $gal;
	}

	function equipo($where = array()){
		$this->db->order_by('orden','ASC');
		$gal = $this->db->get_where('equipo',$where);
		foreach($gal->result() as $n=>$v){
			$gal->row($n)->foto = base_url('img/equipo/'.$v->foto);
		}
		return $gal;
	}

	function franquicias($where = array()){
		$gal = $this->db->get_where('franquicias',$where);
		foreach($gal->result() as $n=>$v){
			$fotos = array();
			foreach(explode(',',$v->fotos) as $f){
				if(!empty($f)){
					$fotos[] = base_url('img/franquicias/'.$f);
				}
			}
			$gal->row($n)->fotos = $fotos;
			$gal->row($n)->redes_sociales = $this->getRedes($v->redes_sociales);
			$gal->row($n)->mapa = $this->getMapa($v->mapa);
		}
		return $gal;
	}

	function cursos($where = array()){
		$this->db->order_by('orden','ASC');
		$gal = $this->db->get_where('cursos',$where);
		foreach($gal->result() as $n=>$v){			
			$gal->row($n)->foto = base_url('img/cursos/'.$v->foto);
			$gal->row($n)->icono = base_url('img/cursos/'.$v->icono);
			$gal->row($n)->foto_destacado = base_url('img/cursos/'.$v->foto_destacado);
			$gal->row($n)->fechas = $this->db->get_where('cursos_fechas',array('cursos_id'=>$v->id));
		}
		return $gal;
	}

	function mergeVersiones($where = array()){
		$this->db->order_by('orden','ASC');
		$this->db->select("tienda_modelos.nombre as modelo,GROUP_CONCAT(tienda_versiones.nombre SEPARATOR ',') as version");
		$this->db->join('tienda_modelos','tienda_modelos.id = tienda_versiones.tienda_modelos_id');
		$this->db->join('tienda_marcas','tienda_marcas.id = tienda_modelos.tienda_marcas_id');
		$this->db->group_by('tienda_modelos.id');
		ob_start();
		sqlToHtml($this->db->get_where('tienda_versiones',$where));
		$str = ob_get_contents();
		ob_end_clean();
		return $str;
	}

	function marcas($where = array()){		
		$gal = $this->db->get_where('tienda_marcas',$where);
		foreach($gal->result() as $n=>$v){			
			$gal->row($n)->foto = base_url('img/tienda/'.$v->foto);			
			$gal->row($n)->modelos = $this->modelos(array('tienda_marcas_id'=>$v->id))->json;
			$gal->row($n)->allVersions = $this->mergeVersiones(array('tienda_marcas.id'=>$v->id));
		}
		$gal->json = $this->toArray($gal);
		return $gal;
	}

	function modelos($where = array()){
		$this->db->order_by('orden','ASC');
		$modelos = $this->db->get_where('tienda_modelos',$where);
		foreach($modelos->result() as $n=>$v){
			$modelos->row($n)->foto = base_url('img/tienda/'.$v->foto);
			$modelos->row($n)->versiones = $this->versiones(array('tienda_modelos_id'=>$v->id))->json;
		}
		$modelos->json = $this->toArray($modelos,'orden');
		return $modelos;
	}

	function versiones($where = array()){
		$versiones = $this->db->get_where('tienda_versiones',$where);
		foreach($versiones->result() as $n=>$v){			
			$versiones->row($n)->averias = $this->averias(array('tienda_versiones_id'=>$v->id))->json;
		}
		$versiones->json = $this->toArray($versiones);		
		return $versiones;
	}

	function averias($where = array()){
		$this->db->select('tienda_averias.*, averias.nombre, averias.foto');
		$this->db->join('averias','averias.id = tienda_averias.averias_id');
		$this->db->order_by('tienda_averias.orden','ASC');
		$averias = $this->db->get_where('tienda_averias',$where);
		$x = 0;
		foreach($averias->result() as $n=>$v){
			$averias->row($n)->orden = $x;
			$averias->row($n)->foto = base_url('img/tienda/'.$v->foto);	
			$averias->row($n)->foto_real = base_url('img/tienda/'.$v->foto_real);			
			$averias->row($n)->_precio = moneda($v->precio);		
			$averias->row($n)->_precio_privado = moneda($v->precio_privado);
			$x++;
		}
		$averias->json = $this->toArray($averias,'orden');		
		return $averias;
	}

	function provincias($where = array()){
		$provincias = $this->db->get_where('provincias',$where);
		$provincias->json = $this->toArray($provincias);
		return $provincias;
	}

    function getHoras($pr){
        $horas = $this->db->query("
    		SELECT * FROM 
    		(
    			Select * from reservas 
    			WHERE provincias_id = '".$pr."' OR provincias_id IS NULL
				UNION ALL
				SELECT NULL,STR_TO_DATE(fecha,'%d/%m/%Y'),hora,'Reservado',provincia,'0' FROM tienda_ventas WHERE procesado = 2 AND provincia = '".$pr."'
				UNION ALL
				SELECT NULL,STR_TO_DATE(fecha,'%d/%m/%Y'),hora,'Reservado',provincia,'0' FROM tienda_ventas_empresas WHERE provincia = '".$pr."'
			) AS cd WHERE cd.fecha >= '".date("Y-m-d")."'"
		);
		$h = array();
		foreach($horas->result() as $n=>$hh){
			$h[] = $hh;
		}
    	return $h;
    }

    function testimonios($where = array()){
		$this->db->order_by('orden','ASC');
		$gal = $this->db->get_where('testimonios',$where);
		foreach($gal->result() as $n=>$v){			
			$gal->row($n)->foto = base_url('img/testimonios/'.$v->foto);
		}
		return $gal;
	}

	function testimonios_videos($where = array()){
		$this->db->order_by('orden','ASC');
		$gal = $this->db->get_where('testimonios_videos',$where);
		foreach($gal->result() as $n=>$v){			
			//$gal->row($n)->foto = base_url('img/testimonios/'.$v->foto);
		}
		return $gal;
	}







	function toArray($obj,$field = 'id'){
		$data = array();
		foreach($obj->result() as $o){			
			$data[$o->{$field}] = $o;
		}
		return $data;
	}

	function blog(){				
		$this->db->limit(3);
		$this->db->order_by('fecha','DESC');
		$this->db->where('idioma',$_SESSION['lang']);
		$blog = $this->db->get_where('blog',array('status'=>1));
		foreach($blog->result() as $nn=>$b){
			$b->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	        $b->foto = base_url('img/blog/'.$b->foto);
	        $b->fecha = strftime('%d %b %Y',strtotime($b->fecha));
	        $b->texto = strip_tags($b->texto);
	        $b->texto = cortar_palabras($b->texto,25);
	        $b->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();            
	        $b->descripcion = cortar_palabras(strip_tags($b->texto),20);
	        $b->posicion = $nn==0?'left':'';
	        $b->posicion = $nn==1?'center':$b->posicion;
	        $b->posicion = $nn==2?'right':$b->posicion;
		}

		return $blog;
	}
}