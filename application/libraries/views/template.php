<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = '<?= base_url() ?>';</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <!-- FONTS -->    
		<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800|Poppins:200,300,400,500,600,700,800" rel="stylesheet">
       
    <!-- CSS -->

    <?php 
    if(!empty($css_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>    
    <?php endif; ?>

	<!-- CSS Plugins -->
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/plugins/bootstrap/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/plugins/slick-carousel/slick/slick.css" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/plugins/animate.css/animate.min.css" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/plugins/animsition/dist/css/animsition.min.css" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/css/isotope.css"/>
	<link rel="stylesheet" href="<?= base_url() ?>js/uikit/css/uikit.css"/>
	<!-- CSS Icons -->
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/css/themify-icons.css" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/plugins/font-awesome/css/font-awesome.min.css" />

	<!-- CSS Theme -->
	<link id="theme" rel="stylesheet" href="<?= base_url() ?>theme/theme/assets/css/themes/style.css" />
</head>

<body>
	<a href="whatsapp://send?text=Hola Mundo&phone=+34609306713" style="font-size:18px;padding:8px 8px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;right:0;bottom:0px;z-index: 1000;"><i class="fa fa-whatsapp"></i> whatsapp</a>
	<!-- Body Wrapper -->
	<div id="body-wrapper" class="animsition">
		<?php 
			if(empty($editor)){
				$this->load->view($view); 
			}else{
				echo $view;
			}
		?>	
	</div>
	<?php $this->load->view('includes/template/scripts') ?>
</body>
</html>