<h5 style="color:#1f789a;margin-bottom: 5px;margin-top: 45px;">Selecciona una fecha</h5>
<!-- product #1 -->
<div class="col-xs-12 col-sm-12 col-md-12 clearfix" style="margin-bottom: 30px;">
	
	<div class="form-group" style="display: inline-block;">			
		<div class="input-group main-cal">				
			<input id="fecha_reparacion" type="text" placeholder="Fecha de reparación" name="fecha" class="form-control" value="<?= @$_SESSION['carrito'][0]->datos['fecha'] ?>">
			<input type="hidden" name="hora" value="<?= @$_SESSION['carrito'][0]->datos['hora'] ?>" id="hora_reparacion">			
		</div>
	</div>
	<h5 style="color:#1f789a;  text-align: left;  margin-bottom: -20px;margin-top: 30px;">Selecciona una hora</h5>
	<div class="form-group">			
		<ul class="timeHora">
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">09:00 - 10:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">10:00 - 11:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">11:00 - 12:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">12:00 - 13:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">13:00 - 14:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">14:00 - 15:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">15:00 - 16:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">16:00 - 17:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">18:00 - 19:00</a></li>
			<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">19:00 - 20:00</a></li>				
		</ul>
	</div>
</div>
<div class="msj"></div>
<div class="col-xs-12 col-md-12">
	<a href="javascript:selFecha(1)" class="btn btn-primary btn-black btn-block">Confirmar</a>
</div>