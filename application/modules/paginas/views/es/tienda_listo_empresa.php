<div class="col-xs-12 visible-xs" style="text-align: center;margin-bottom: 30px;">
	<a class="btn btn-secondary" href="#datos_cliente" data-toggle="tab" style="width: auto;padding: 0 20px;">Volver a seleccionar datos</a>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>7. Orden almacenada con éxito
			<span class="color-theme"> Hemos terminando</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row">
	<div class="col-xs-6 col-md-6">
		<div class="cart-total-amount" style="display: inline-block">
			<h6>Tu dispositivo</h6>
			<div class="cart-product-img">
				<img src="<?= $car->modelo->foto ?>" alt="product" style="width:100%;">
			</div>
			<div class="cart-product-name">
				<h6><?= $car->modelo->nombre ?> <?= mb_strtolower($car->datos['color']) ?></h6>				
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6 cart-total-amount-parent"  style="margin-bottom: 60px;">
		<div class="cart-total-amount">
			<h6>Datos de contacto</h6>
			<ul class="list-unstyled" style="margin:4px">
				<li>Nombre :
					<span class="pull-right text-right"><?= $car->datos['nombre'] ?></span>
				</li>
				<li>Provincia :
					<span class="pull-right text-right"><?= $this->db->get_where('provincias',array('id'=>$car->datos['provincia']))->row()->nombre ?></span>
				</li>
				<li>Dia y hora :
					<span class="pull-right text-right"><?= $car->datos['fecha'].' '.$car->datos['hora'] ?></span>
				</li>
				<li>Email :
					<span class="pull-right text-right"><?= $car->datos['email'] ?></span>
				</li>
				
				
			</ul>
		</div>
	</div>
</div>
<div class="row" style="margin-top:20px;">
	<div class="col-xs-12 mt-20">
		<h3>Reparaciones seleccionadas y precio</h3>
		<div class="cart-table table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr class="cart-product">
						<th class="cart-product-item">Equipo</th>
						<th class="hidden-xs cart-product-price">Precio</th>
						<th class="hidden-xs cart-product-quantity">Cantidad</th>
						<th class="hidden-xs cart-product-total">Total</th>
					</tr>
				</thead>
				<tbody>
					<?php $total = 0;  $c = (object)$car;
						foreach($car->averias as $a): $total+= $a->precio_privado*1; 
					?>
						<tr class="cart-product">
							<td class="cart-product-item">								
								<div class="cart-product-img">
									<img src="<?= $c->modelo->foto ?>" alt="product" style="width:60px; margin-right:10px;"/>
								</div>
								<div class="cart-product-name">
									<h5 class="carProductTableName" style=" margin-bottom: 5px; "><?= $c->modelo->nombre ?>, <?= $c->datos['color'] ?></h5><h6 style=" color: #8f8f8f;  "><?= $a->nombre ?>
									<br>
									<?= $c->datos['fecha'] ?>___<?= $c->datos['hora'] ?><br>
									<?php if(!empty($c->datos['problema'])): ?>
										<h5 style=" margin-top: 18px; font-size: 13px;text-decoration: underline; text-transform: initial;color: #5c5c5c; margin-bottom: 5px;">
											Descripción del problema 
										</h5>
										<small style=" margin-top:-15px; font-size: 13px; text-transform: initial;color: #5c5c5c;">
											<?= $c->datos['problema'] ?>
										</small>
									<?php endif ?>
								</div>
							</td>
							<td class="hidden-xs cart-product-price"><?= moneda($a->precio_privado) ?></td>
							<td class="hidden-xs cart-product-price">
								<?= empty($c->cantidad)?1:$c->cantidad ?>
							</td>
							<td class="hidden-xs cart-product-total"><?= moneda($a->precio_privado*1) ?></td>
						</tr>
					<?php endforeach ?>					
					<?php 
						//$iva = $total*0.21;
						$iva = 0;
						$subtotal = $total;
						$total = $subtotal+$iva;
					?>					
					<tr class="cart-product">
						<td  colspan="3" class="cart-product-item"style="border-color: #ececec;background: #1f789a;color: white; font-size: 18px;font-weight: 600;text-transform: uppercase;olor: white;" >
							<span class="hidden-xs">Precio Final</span>
							<span class="visible-xs">Precio final: <?= moneda($total) ?></span>
						</td>					
						<td class="hidden-xs cart-product-total" style="border-color: #ececec;background: #1f789a;color: white; font-size: 18px;font-weight: 600;text-transform: uppercase;olor: white;"><?= moneda($total) ?></td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
	
</div>

<div class="row" style="margin-top:30px;">
	<div class="col-xs-12 col-sm-12 col-md-12 p-0">		
		<a href="<?= base_url('area-privada.html') ?>" id="submit-message" class="btn btn-primary btn-black btn-block">Cargar otra solicitud</a>
	</div>
	<div>&nbsp;</div>		
</div>