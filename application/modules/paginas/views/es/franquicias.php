[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/333.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>Franquíciate</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Inicio</a>
						</li>
						<li class="active">franquicias</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>


<!-- Call To Action #8
============================================= -->
<section id="cta-8" class="bg-overlay bg-overlay-theme bg-overlay-theme2 cta cta-6">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/call/1.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="text-capitalize mb-0 color-white">QUIERES FORMAR PARTE DE NUESTRO EQUIPO</p>
				<h2 class="mb-xs color-white">NO ESPERES MÁS, <br>TE OFRECEMOS LO QUE OTROS NO PUEDEN, TE DAMOS LOS QUE OTROS NO SABEN.</h2>
				<a class="btn btn-secondary btn-white" href="#" data-toggle="modal" data-target="#model-quote6">PIDE INFORMACIÓN</a>				
			</div>
			<!-- .col-md-8 end -->
			<div class="col-md-5 hidden-xs hidden-sm">
				<div class="cta-img">
					<img src="<?= base_url() ?>theme/theme/assets/images/call/1.png" alt="call to action"/>
				</div>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #cta-8 end -->
<!-- Service Block #6
============================================= -->
<section id="service-6" class="service service-6 bg-gray pt-0 pb-0">
	<div class="container-fluid p-0">
		<div class="row" style="margin-left:0px; margin-right: 0px;">
			<div class="col-xs-12 col-sm-12 col-md-6 p-0">
				<div id="googleMap" style="width:100%;height: 783px;">
				</div> 
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-12 col-md-6 p-0">
				<div class="row pr-xs" style="margin-left:0px; margin-right: 0px;">
					<!--- service block #1 -->
					<div class="col-xs-12 col-sm-6 col-md-6 service-block service-block2">
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/normativa.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>NORMATIVA</h4>
								<p>Según la legislación vigente, con una antelación mínima de 20 días hábiles a la firma del contrato o precontrato, se entregará al potencial franquiciado una información veraz y no engañosa. Pídenos nuestro dossier de franquiciado</p>
								<a class="read-more" href="#" href="#" data-toggle="modal" data-target="#model-quote6"><i class="fa fa-plus"></i>
									<span>contactar</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-6 end -->
					<!--- service block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-6 service-block service-block2">
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/nosotros.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>NOSOTROS</h4>
								<p>Móviles SAT® es un modelo de negocio enfocado a la reparación casi inmediata de móviles y tablets, además de las reparaciones a domicilio. Nuestro éxito se basa en 3 pilares, Profesionalidad, Formación y Atención personalizada. </p>
								<a class="read-more" href="#" href="#" data-toggle="modal" data-target="#model-quote6"><i class="fa fa-plus"></i>
									<span>contactar</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-6 end -->
					<!--- service block #3 -->
					<div class="col-xs-12 col-sm-6 col-md-6 service-block service-block2">
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/fran.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>LA FRANQUICIA</h4>
								<p>Móviles SAT®, centro de reparación de terminales tecnológicos, aplicamos todos los métodos para cubrir las necesidades del cliente. 
En nuestros establecimientos ofrecemos todos los servicios en reparación de terminales móviles.</p>
								<a class="read-more" href="#" href="#" data-toggle="modal" data-target="#model-quote6"><i class="fa fa-plus"></i>
									<span>contactar</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-6 end -->
					<!--- service block #3 -->
					<div class="col-xs-12 col-sm-6 col-md-6 service-block service-block2">
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/smartphone.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>EL SECTOR</h4>
								<p>Estamos en una sociedad cada vez más dependiente de las redes sociales y aplicaciones de mensajería instantánea y es por ese motivo que no pueden estar sin su terminal.
Nuestro País cuenta con más terminales móviles que ordenadores</p>
								<a class="read-more" href="#" href="#" data-toggle="modal" data-target="#model-quote6"><i class="fa fa-plus"></i>
									<span>contactar</span>
								</a>
							</div>
						</div>
					</div>
					
					
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-6 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>






<section class="single-service" id="resumen">
	<div class="container">
		<div class="row">
			<div class="heading heading-4">
				<div class="heading-bg heading-right">
					<p class="mb-0">CONOCE LOS QUE YA FORMAN PARTE</p>
					<h2>todos nuestros franquiciados</h2>
				</div>
			</div>
			<div class="hidden-xs sidebar sidebar-services col-xs-12 col-sm-4 col-md-3">
				<div class="widget widget-categories">
					<div class="widget-content">
						<ul class="list-unstyled">
							
							<?php foreach($this->elements->franquicias(array('confianza'=>0))->result() as $n=>$f): ?>
								<li id="linkTab2<?= $f->id ?>" <?= $n==0?' class="active"':'' ?> data-toggle="tab" aria-controls="home" role="tab" data-target="#tab<?= $f->id ?>" style="cursor:pointer;">
									<a href="#tab<?= $f->id ?>" id="linkTab<?= $f->id ?>" data-toggle="tab" aria-controls="home" role="tab"><?= $f->nombre ?></a>
								</li>
							<?php endforeach ?>

						</ul>
					</div>
				</div>
				<div class="widget widget-download"></div>
				<div class="widget widget-categories">
					<div class="widget-title">
						<h3>CENTROS DE CONFIANZA</h3>
					</div>
					<div class="widget-content">
						<ul class="list-unstyled">
							<?php foreach($this->elements->franquicias(array('confianza'=>1))->result() as $f): $n++; ?>
								<li id="linkTab2<?= $f->id ?>" <?= $n==0?' class="active"':'' ?> data-toggle="tab" aria-controls="home" role="tab" data-target="#tab<?= $f->id ?>" style="cursor:pointer;">
									<a href="#tab<?= $f->id ?>" id="linkTab<?= $f->id ?>" data-toggle="tab" aria-controls="home" role="tab"><?= $f->nombre ?></a>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>
			<!-- .sidebar end -->
			<div class="hidden-xs col-xs-12 col-sm-8 col-md-9">
				<div class="row tab-content">
					
					<?php foreach($this->elements->franquicias()->result() as $i=>$f): ?>
						<div id="tab<?= $f->id ?>" class="col-xs-12 col-sm-12 col-md-12 tab-pane <?= $i==0?'active':'' ?>">
							<div class="service-img" style="margin-bottom:0px">
								<div id="franqslider<?= $f->id ?>" class="project-carousel" style="margin-bottom: 0">									
									<?php foreach($f->fotos as $ff): ?>
										<div class="item">
											<img src="<?= $ff ?>" alt="Project">
										</div>
									<?php endforeach ?>
									<?php if(!empty($f->mapa)): ?>									
										<div class="hidden-xs hidden-sm item">
											<div id="mapa<?= $f->id ?>" style="width:100%; height:512px;"></div>										
										</div>
									<?php endif ?>
								</div>
							</div>
							<div class="col-xs-12 col-md-12 pl-0 top-bar" style=" margin-bottom:30px;">
								<ul class="list-inline top-widget" style="margin-left:0px;">
									<li class="top-social">
										<?php foreach($f->redes_sociales as $r): ?>
											<?= $r ?>
										<?php endforeach ?>
										<a href="javascript:void(0)" onclick="franqslider<?= $f->id ?>.trigger('to.owl.carousel',[3,500,true]);"><i class="fa fa-map-marker"></i></a>
									</li>
								</ul>
							</div>
							<div class="col-xs-12 col-md-7 pl-0" style=" padding-right: 30px;">
								<div class="service-content">
									<div id="mapa22<?= $f->id ?>" style="width:100%; height:242px;"></div>
								</div>
							</div>
							<div class="col-xs-12 col-md-5 p-0">
								<div class="single-project cuadroFranquicia">
									<div class=" project-block">
										<div class="project-title">
											<h3><?= $f->nombre ?></h3>
										</div>
										<div class="project-desc">
											<ul class="list-unstyled">												
												<li>Dirección:
													<span><?= $f->direccion ?></span>
												</li>
												<li>Población:
													<span><?= $f->poblacion ?></span>
												</li>
												<li>Provincia:
													<span><?= $f->provincia ?></span>
												</li>
												<li>Telefono:
													<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->telefono) ?>"><?= $f->telefono ?></a></span>
												</li>
												<li>Móvil:
													<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->movil) ?>"><?= $f->movil ?></a></span>
												</li>
												<li>Email:
													<span><a href="mailto:<?= $f->email ?>"><?= $f->email ?></a></span>
												</li>
												<li>web:
													<span><a href="http://<?= $f->web ?>"><?= $f->web ?></a></span>
												</li>
												<li>&nbsp;</li>
												<li>
													<button type="submit" class="btn btn-primary btn-black btn-block" data-toggle="modal" data-target="#franModal<?= $f->id ?>">Contacto</button>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach ?>
					
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-9 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>


<!---- Collapse Mobile Responsive ----------->


<section class="visible-xs single-service pt-0" id="resumen">
	<div class="container">
		<div class="widget widget-categories">
			<div class="widget-content">
				<?php foreach($this->elements->franquicias(array('confianza'=>0))->result() as $i=>$f): ?>
					<ul class="list-unstyled" id="#headp<?= $f->id ?>">					
						<li id="linkTab2<?= $f->id ?>" class="respCollapse" data-toggle="collapse" aria-controls="home" role="tab" data-target="#tabv<?= $f->id ?>" style="cursor:pointer;">
							<?= $f->nombre ?>
						</li>					
					</ul>
					<div id="tabv<?= $f->id ?>" class="tab-pane panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="single-project">
							<div class=" project-block">								
								<div class="project-desc">
									<ul class="list-unstyled">
										<li>
											<img src="<?= $f->fotos[0] ?>" alt="Project" style="width:100%;">
										</li>												
										<li>Dirección:
											<span><?= $f->direccion ?></span>
										</li>
										<li>Población:
											<span><?= $f->poblacion ?></span>
										</li>
										<li>Provincia:
											<span><?= $f->provincia ?></span>
										</li>
										<li>Telefono:
											<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->telefono) ?>"><?= $f->telefono ?></a></span>
										</li>
										<li>Móvil:
											<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->movil) ?>"><?= $f->movil ?></a></span>
										</li>
										<li>Email:
											<span><a href="mailto:<?= $f->email ?>"><?= $f->email ?></a></span>
										</li>
										<li>web:
											<span><a href="http://<?= $f->web ?>"><?= $f->web ?></a></span>
										</li>
										<li>
											<div class="mapa" id="mapc<?= $f->id ?>" data-lat="<?= $f->mapa[0] ?>" data-lon="<?= $f->mapa[1] ?>"></div>
										</li>										
										<li>
											<button type="submit" class="btn btn-primary btn-black btn-block" data-toggle="modal" data-target="#franModal<?= $f->id ?>">Contacto</button>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="widget widget-download"></div>
		<div class="widget widget-categories">
			<div class="widget-title">
				<h3>CENTROS DE CONFIANZA</h3>
			</div>
			<div class="widget-content">
				<?php foreach($this->elements->franquicias(array('confianza'=>1))->result() as $i=>$f): ?>
					<ul class="list-unstyled" id="#headp<?= $f->id ?>">					
						<li id="linkTab2<?= $f->id ?>" class="respCollapse" data-toggle="collapse" aria-controls="home" role="tab" data-target="#tabp<?= $f->id ?>" style="cursor:pointer;">
							<?= $f->nombre ?>
						</li>					
					</ul>
					<div id="tabp<?= $f->id ?>" class="tab-pane panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="single-project">
							<div class=" project-block">								
								<div class="project-desc">
									<ul class="list-unstyled">	
										<li>
											<img src="<?= $f->fotos[0] ?>" alt="Project" style="width:100%;">
										</li>											
										<li>Dirección:
											<span><?= $f->direccion ?></span>
										</li>
										<li>Población:
											<span><?= $f->poblacion ?></span>
										</li>
										<li>Provincia:
											<span><?= $f->provincia ?></span>
										</li>
										<li>Telefono:
											<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->telefono) ?>"><?= $f->telefono ?></a></span>
										</li>
										<li>Móvil:
											<span><a href="tel:<?= str_replace(array(' ','+'),array('',''),$f->movil) ?>"><?= $f->movil ?></a></span>
										</li>
										<li>Email:
											<span><a href="mailto:<?= $f->email ?>"><?= $f->email ?></a></span>
										</li>
										<li>web:
											<span><a href="http://<?= $f->web ?>"><?= $f->web ?></a></span>
										</li>	
										<li>
											<div class="mapa" id="mapc<?= $f->id ?>" data-lat="<?= $f->mapa[0] ?>" data-lon="<?= $f->mapa[1] ?>"></div>
										</li>									
										<li>
											<button type="submit" class="btn btn-primary btn-black btn-block" data-toggle="modal" data-target="#franModal<?= $f->id ?>">Contacto</button>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>











<!-- Modal -->
<div class="modal fade model-quote" id="model-quote6" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-store"></i>
				</div>
				<div class="model-divider">
					<div class="model-title">
						<p>AQUI ESTAMOS HAZNOS TU</p>
						<h6>CONSULTA</h6>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<form id="pop-quote-form" action="assets/php/sendpopquote.php" method="post">
					<input type="text" class="form-control" name="quote-name" id="name" placeholder="Nombre" required/>
					<input type="email" class="form-control" name="quote-email" id="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="quote-telephone" id="telephone" placeholder="Teléfono" required/>
					<textarea class="form-control" name="quote-message"  id="quote" placeholder="Mensaje" rows="2" required></textarea>
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
					<!--Alert Message-->
					<div id="pop-quote-result" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>



<!-- .model-quote end -->
<?php foreach($this->elements->franquicias()->result() as $i=>$f): ?>
	<!-- Modal -->
	<div class="modal fade model-quote" id="franModal<?= $f->id ?>" tabindex="-1" role="dialog" aria-labelledby="modelquote">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<div class="model-icon">
						<i class="lnr lnr-store"></i>
					</div>
					<div class="model-divider">
						<div class="model-title">
							<p><?= $f->nombre ?></p>
							<h6>Haz tu consulta</h6>
						</div>
					</div>
				</div>
				<!-- .model-header end -->
				<div class="modal-body">
					<form id="pop-quote-form" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#pop-quote-result<?= $f->id ?>');" method="post">
						<input type="text" class="form-control" name="nombre" id="name" placeholder="Nombre" required/>
						<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
						<input type="text" class="form-control" name="telefono" id="telephone" placeholder="Teléfono" required/>
						<textarea class="form-control" name="extras[mensaje]"  id="quote" placeholder="Mensaje" rows="2" required></textarea>
						<input type="hidden" name="asunto" value="Consulta realizada a <?= $f->nombre ?>">
						<input type="hidden" name="to" value="<?= $f->email ?>">
						<label for="">
							<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
						</label>
						<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
						<!--Alert Message-->
						<div id="pop-quote-result<?= $f->id ?>" class="mt-xs">
						</div>
					</form>
				</div>
				<!-- .model-body end -->
			</div>
		</div>
	</div>
<?php endforeach ?>



[footer]




<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script> 
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript">

	var styleCluster = 
	[
		{url:URL+'img/clusters/m1.png',textColor:'white',width:'53',height:'52',textSize:'18'},
		{url:URL+'img/clusters/m2.png',textColor:'white',width:'56',height:'55',textSize:'18'},
		{url:URL+'img/clusters/m3.png',textColor:'white',width:'66',height:'65',textSize:'18'},
		{url:URL+'img/clusters/m4.png',textColor:'white',width:'78',height:'77',textSize:'18'},
		{url:URL+'img/clusters/m5.png',textColor:'white',width:'90',height:'89',textSize:'18'}
	];
	
	function onShowTab(i,lat,lng,confianza){

		console.log("mapa"+i);
		var content = document.getElementById("mapa"+i);
		var content2 = document.getElementById("mapa22"+i);
		var opts = {
			center: new google.maps.LatLng(lat,lng),
			zoom:16,
			styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
		};

		var mapa = new google.maps.Map(content,opts);
		var mapa22 = new google.maps.Map(content2,opts);
		var icon = confianza==1?URL+'img/map-marker2.png':URL+'img/map-marker.png';
		var marker = new google.maps.Marker({
			position:new google.maps.LatLng(lat,lng),
			map:mapa,
			icon:icon
		});
			marker = new google.maps.Marker({
			position:new google.maps.LatLng(lat,lng),
			map:mapa22,
			icon:icon
		});

		var franqslider = $("#franqslider"+i).owlCarousel({
			loop: false,
	        margin: 22,
	        nav: true,
	        dots: false,
	        dotsSpeed: 200,
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
	        responsive: {
	            0: {
	                items: 1
	            },
	            600: {

	                items: 1
	            },
	            1000: {
	                items: 1
	            }
	        }	    
		});
	}
	
	
	<?php foreach($this->elements->franquicias()->result() as $i=>$f): ?>
		$("#linkTab<?= $f->id ?>,#linkTab2<?= $f->id ?>").on('shown.bs.tab',function(){
			onShowTab('<?= $f->id ?>','<?= $f->mapa[0] ?>','<?= $f->mapa[1] ?>',<?= $f->confianza ?>);
		});		
		<?php if($i==0): ?>
			onShowTab('<?= $f->id ?>','<?= $f->mapa[0] ?>','<?= $f->mapa[1] ?>',<?= $f->confianza ?>);
		<?php endif ?>
		
	<?php endforeach ?>

	var franqs = [];
	var content = document.getElementById('googleMap');
	var opts = {
		center: new google.maps.LatLng(40.2040038,-2.8388287),
		zoom:5,
		styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
	};



	var map = new google.maps.Map(content,opts);	
	<?php foreach($this->elements->franquicias()->result() as $i=>$f): ?>		
		
		franqs[<?= $f->id ?>] = new google.maps.Marker({
			position:new google.maps.LatLng('<?= $f->mapa[0] ?>','<?= $f->mapa[1] ?>'),
			map:map,
			icon:'<?= $f->confianza==1?base_url('img/map-marker2.png'):base_url('img/map-marker.png') ?>'
		});
		franqs[<?= $f->id ?>].contenido = '<b><?= $f->nombre ?></b><br/>Tel: +34<?= $f->telefono ?><br/>Email: <?= $f->email ?>';
		franqs[<?= $f->id ?>].infowindow = new google.maps.InfoWindow({
          content: franqs[<?= $f->id ?>].contenido
        });
		franqs[<?= $f->id ?>].addListener('click', function() {
          this.infowindow.open(map, franqs[<?= $f->id ?>]);
        });        
	<?php endforeach ?>
	var markerCluster = new MarkerClusterer(map, franqs,{imagePath: URL+'img/clusters/m','styles':styleCluster});



	$(".respCollapse").on('shown.bs.collapse',function(){
		console.log('12');
	});


</script>