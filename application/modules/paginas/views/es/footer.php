<div id="footerAll">
	<?php 
		$lugar  = empty($_SESSION['franquicia'])?'08700 Igualada':$_SESSION['franquicia']->direccion;
		$email = empty($_SESSION['franquicia'])?'movilessat@movilessat.es':$_SESSION['franquicia']->email;
		$tel = empty($_SESSION['franquicia'])?'93 803 19 24':$_SESSION['franquicia']->telefono;
	?>
	<footer id="footer" class="footer-1" style="margin-top:50px;">
		<!-- Contact Bar
		============================================= -->
		<div class="container footer-widgtes">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: -50px;">
					<div class="widgets-contact">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 widget">
								<div class="widget-contact-icon pull-left">
									<i class="lnr lnr-map"></i>
								</div>
								<div class="widget-contact-info">
									<p class="text-white">Visítanos en</p>
									<p class="text-capitalize font-heading"><a href="<?= base_url() ?>franquicias.html">Nuestras franquicias</a></p>
									
								</div>
							</div>
							<!-- .widget end -->
							<div class="col-xs-12 col-sm-12 col-md-4 widget">
								<div class="widget-contact-icon pull-left">
									<i class="lnr lnr-envelope"></i>
								</div>
								<div class="widget-contact-info">
									<p class="text-capitalize text-white">email</p>
									<p class="text-capitalize font-heading"><a href="mailto:<?= $email ?>"><?= $email ?></a></p>
								</div>
							</div>
							<!-- .widget end -->
							<div class="col-xs-12 col-sm-12 col-md-4 widget">
								<div class="widget-contact-icon pull-left">
									<i class="lnr lnr-phone"></i>
								</div>
								<div class="widget-contact-info">
									<p class="text-capitalize text-white">Llámanos al</p>
									<p class="text-capitalize font-heading"><a href="tel:34<?= str_replace(' ','',$tel) ?>">+34 <?= $tel ?></a></p>
								</div>
							</div>
							<!-- .widget end -->
						</div>
						<!-- .row end -->
					</div>
					<!-- .widget-contact end -->
				</div>
				<!-- .col-md-12 end -->
			</div>
			<!-- .row end -->
		</div>
		<!-- .container end -->
		
		
		<!-- Social bar
		============================================= -->
		<div class="widget-social">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 mb-30-xs mb-30-sm">
						<div class="widget-social-info pull-left text-capitalize pull-none-xs mb-15-xs">
							<p class="mb-0">Síguenos<br>
								en las redes sociales</p>
						</div>
						<div class="widget-social-icon pull-none-xs">
							<a href="https://www.facebook.com/movilessat/">
								<i class="fa fa-facebook"></i><i class="fa fa-facebook"></i>
							</a>
							
							<a href="https://twitter.com/movilessat" >
								<i class="fa fa-twitter"></i><i class="fa fa-twitter"></i>
							</a>
							
							<a href="https://www.instagram.com/moviles_sat" >
								<i class="fa fa-instagram"></i><i class="fa fa-instagram"></i>
							</a>
							
							
							
							
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="widget-newsletter-info pull-left text-capitalize pull-none-xs mb-15-xs">
							<p class="mb-0">subscríbete<br>
								a nuestra newsletter</p>
						</div>
						<div class="widget-newsletter-form pull-right text-right">
							
							<!-- Mailchimp Form 
							=============================================-->
							<form class="mailchimp">
								<div class="subscribe-alert">
								</div>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Escribe tu email">
									<span class="input-group-btn">
									<button class="btn text-capitalize" type="button">enviar</button>
									</span>
								</div>
								<!-- /input-group -->
							</form>
							<!--Mailchimp Form End-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Copyrights
		============================================= -->
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 copyrights text-center">
					<p class="text-capitalize">© 2019 - móviles SAT. Todos los derechos reservados</p>
					<a href="<?= base_url() ?>aviso-legal.html">Aviso Legal</a> / 
					<a href="<?= base_url() ?>garantias-y-devoluciones.html">Garantías y devoluciones</a> / 
					<a href="<?= base_url() ?>politicas-de-privacidad.html">Política de privacidad</a> / 
					<a href="<?= base_url() ?>cookies.html">Cookies</a>
					<p class="text-capitalize">
						Web by
						<a href="http://www.jordimagana.com">jordimagaña.com</a>
					</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?= base_url() ?>theme/theme/assets/js/plugins.js?v=1.0.0"></script>
	<script src="<?= base_url() ?>theme/theme/assets/js/functions.js?v=1.2.0"></script>
	<script src='<?= base_url() ?>js/lightgallery/js/lightgallery.js'></script>
	<script src="<?= base_url() ?>js/frame.js?v=1.2.0"></script>
	<script>	
	    function addToCart(producto_id,cantidad){
	        $.post('<?= base_url() ?>tt/addToCart/'+producto_id+'/'+cantidad,{},function(data){
	            $(".menubar-cart").html(data);      
	            $("#header-cart .header-cart-content").addClass('showed'); 
	            $("#header-cart-mobile").addClass('showed');   
	            $(".cart-box").addClass('activado');  
	        });


	    }
	    function remToCart(producto_id){
	        $.post('<?= base_url() ?>tt/delToCart/'+producto_id,{},function(data){
	            $(".cartdetail").html(data);
	        }); 
	    }

	    function remCart(producto_id){
	        $.post('<?= base_url() ?>tt/delToCartNav/'+producto_id,{},function(data){
	            $(".menubar-cart").html(data);
	        });    
	    }

	    function closeCart(){
	        $('.cart-box').removeClass('activado');
	    }
	</script>
	
</div>