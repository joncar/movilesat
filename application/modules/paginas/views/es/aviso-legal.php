[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/22.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>Aviso legal</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Inicio</a>
						</li>
						<li class="active">Aviso legal</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Shortcode #10
============================================= -->
<section id="shortcode-10" class="shortcode-10">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="rte">
<?= l('aviso_legal')?>
	</div>
			</div>
		</div>
		
	</div>
	<!-- .container End -->
</section>
<!-- #shortcode end-->
[footer]