[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/22.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>QUIENES SOMOS</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Home</a>
						</li>
						<li class="active">nosotros</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Shortcode #10
============================================= -->
<section id="shortcode-10" class="shortcode-10">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">TODO SOBRE NOSOTROS</p>
						<h2>LAS VENTAJAS DE MOVILES SAT</h2>
					</div>
					<p class="mb-0 italic">Móviles Sat cuenta con más de 40 técnicos repartidos por diferentes ciudades de la península para cubrir de manera rápida la alta demanda en reparación de terminales a domicilio.</p>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-8 end -->
			
		</div>
		<!-- .row End -->
		<div class="row text-center">
			<div class="col-xs-12 col-sm-6 col-md-3 feature feature-1">
				<div class="feature-icon">
					<i class="lnr lnr-calendar-full font-40 color-heading"></i>
				</div>
				<h4 class="text-uppercase font-16">Siempre disponibles</h4>
				<p>Disponibles 24/7 desde nuestra web para que puedas solicitar tu presupuesto y cita previa.</p>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 feature feature-1">
				<div class="feature-icon">
					<i class="lnr lnr-briefcase font-40 color-heading"></i>
				</div>
				<h4 class="text-uppercase font-16">Técnicos cualificados</h4>
				<p>Más de 15 años de experiencia en la electrónica, hacen que las reparaciones sean fáciles.</p>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 feature feature-1">
				<div class="feature-icon">
					<i class="lnr lnr-rocket font-40 color-heading"></i>
				</div>
				<h4 class="text-uppercase font-16">Super veloces</h4>
				<p>Nuestra mayoría de reparaciones en domicilio se realizan en una media de 30 minutos.</p>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 feature  feature-1">
				<div class="feature-icon">
					<i class="lnr lnr-cart font-40 color-heading"></i>
				</div>
				<h4 class="text-uppercase font-16">Ofertas increibles</h4>
				<p>Disponemos de las mejores ofertas en reparaciones a domicilio. Calidad y rapidez.</p>
			</div>
		</div>
		<!-- .row end -->
		<div class="row">
			<!-- 
<div class="col-xs-12 col-sm-12 col-md-12 text-center mt-50">
				<a class="btn btn-secondary" href="#">read more <i class="fa fa-plus ml-xs"></i></a>
			</div>
 -->
			<!-- .col-md-12 end -->
		</div>
		<!-- .row End -->
	</div>
	<!-- .container End -->
</section>
<!-- #shortcode end-->


<!-- Call To Action #3
============================================= -->
<section id="cta-3" class="parallax-section cta cta-3 bg-overlay-theme3 text-center" data-parallax="scroll" data-image-src="<?= base_url() ?>theme/theme/assets/images/call/2.jpg">	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<h2>reparaciones 100%</h2>
				<p>Damos un servicio técnico a domicilio muy especial, sin costes adicionales, con un trato agradable y explicando al cliente todas las dudas que le surjan durante la reparación. Nuestras reparaciones están 100% garantizadas. Vas a quedar encatand@ con nuestros técnicos a domicilio. </p>
				<div class="signiture">
					<img src="<?= base_url() ?>theme/theme/assets/images/call/sign.png" alt="signiture"/>
				</div>
			</div>
			<!-- .col-md-8 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>



<!-- Team
============================================= -->
<section id="team" class="team pb-0">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">LO DAMOS TODO</p>
						<h2>NUESTRO EQUIPO</h2>
					</div>
					<p class="mb-0 italic">Nuestro equipo está compuesto por profesionales altamente formados y entregados a su profesión. Están comprometidos con nuestra misión, resolver los problemas de los dispositivos de cada uno de nuestros clientes como si de nuestros dispositivos se tratase. Transparencia, Calidad y Profesionalidad.</p>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-8 end -->
			
		</div>
		<div class="row">
		
		<!-- Member #2 -->
			
			<?php foreach($this->elements->equipo()->result() as $e): ?>
			<div class="col-xs-6 col-sm-6 col-md-3 member">
				<div class="member-img">
					<img src="<?= $e->foto ?>" alt="member"/>
					<div class="member-bg">
					</div>
					<div class="member-overlay">
						
						<p style="color: #ffffff;"><?= $e->nombre ?></p>
					</div>
				</div>
				<!-- .member-img end -->
				<div class="member-bio">
					<h3><?= $e->nombre ?></h3>
					<p><?= $e->cargo ?></p>
				</div>
				<!-- .member-bio end -->
			</div>
			<!-- .member end -->
			<?php endforeach ?>		
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Testimonials #4
============================================= -->
<section class="testimonial testimonial-4 bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-3 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿QUÉ OPINAN NUESTROS CLIENTES?</p>
						<h2>testimonios</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<div id="testimonial-oc-4" class="testimonial-slide">
					
					<?php foreach($this->elements->testimonios(array('mostrar_empresa'=>1))->result() as $t): ?>
						<div class="testimonial-item">
							<div class="testimonial-content">
								<p><?= $t->testimonio ?></p>
							</div>
							<div class="testimonial-meta">
								<strong><?= $t->nombre ?></strong>, <?= $t->red ?>
							</div>
						</div>
					<?php endforeach ?>
					
				</div>
			</div>
			<!-- .col-md-8 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>



<!-- Call To Action #8
============================================= -->
<section id="cta-8" class="bg-overlay bg-overlay-theme cta cta-6">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/call/1.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="text-capitalize mb-0 color-white">¿QUIERES FORMAR PARTE DE NUESTRO EQUIPO?</p>
				<h2 class="mb-xs color-white">NO ESPERES MÁS,<br>TE OFRECEMOS LO QUE OTROS NO PUEDEN, TE DAMOS LOS QUE OTROS NO SABEN.</h2>
				<a class="btn btn-secondary btn-white" href="<?= base_url() ?>franquicias.html">Franquíciate</a>				
			</div>
			<!-- .col-md-8 end -->
			<div class="col-md-5 hidden-xs hidden-sm">
				<div class="cta-img">
					<img src="<?= base_url() ?>theme/theme/assets/images/call/1.png" alt="call to action"/>
				</div>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #cta-8 end -->

<!-- Contact #2
============================================= -->
<section id="contact" class="contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-4">
					<div class="heading-bg heading-right">
						<p class="mb-0">QUEREMOS OIRTE!</p>
						<h2>Contáctanos</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 widgets-contact mb-60-xs">
						<div class="widget">
							<div class="widget-contact-icon pull-left">
								<i class="lnr lnr-store"></i>
							</div>
							<div class="widget-contact-info">
								<p class="">Visita nuestras tiendas</p>
								<p class="text-capitalize font-heading">
									<a href="<?= base_url() ?>franquicias.html#resumen"><u>Todas nuestras Tiendas</u></a>
								</p>
							</div>
						</div>
						<!-- .widget end -->
						<div class="clearfix">
						</div>
						<div class="widget">
							<div class="widget-contact-icon pull-left">
								<i class="lnr lnr-envelope"></i>
							</div>
							<div class="widget-contact-info">
								<p class="text-capitalize ">email</p>
								<p class="text-capitalize font-heading"><a href="mailto:movilessat@movilessat.es">movilessat@movilessat.es</a></p>
							</div>
						</div>
						<!-- .widget end -->
						<div class="clearfix">
						</div>
						<div class="widget">
							<div class="widget-contact-icon pull-left" style="margin-bottom: 20px;">
								<i class="lnr lnr-phone"></i>
							</div>
							<div class="widget-contact-info">
								<p class="text-capitalize">Llámanos al</p>
								<p class="text-capitalize font-heading"><a href="tel:+34938053738">+ 34 93 803 19 24</a><br>
								<span class="text-capitalize font-heading"><a href="tel:+34660074797">+ 34 660 07 47 97</a></span>
							</div>
						</div>
						<!-- .widget end -->
					</div>
					<!-- .col-md-4 end -->
					<div class="col-xs-12 col-sm-12 col-md-8">
						<div class="row">
							<form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#contact-result');" method="post" onsubmit="return false;">
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" name="nombre" id="name" placeholder="Nombre" required/>
								</div>
								<div class="col-md-6">
									<input type="email" class="form-control mb-30" name="email" id="email" placeholder="Email" required/>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" name="telefono" id="telephone" placeholder="Teléfono" required/>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" name="asunto" id="subject" placeholder="Tema" required/>
								</div>
								<div class="col-md-12">
									<textarea class="form-control mb-30" name="extras[mensaje]" id="message" rows="2" placeholder="Detalle del mensaje" required></textarea>
								</div>
								<label for="" style="font-weight: 300; margin-bottom: 20px;margin-left: 16px; font-size: 13px;">
									<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
								</label>
								<div class="col-md-12">
									<button type="submit" id="submit-message" class="btn btn-primary btn-black btn-block">Enviar Mensaje</button>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 mt-xs">
									<!--Alert Message-->
									<div id="contact-result">
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- .col-md-8 end -->
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- #clients end-->

[footer]

<script type="text/javascript" src="<?= base_url() ?>theme/theme/assets/js/jquery.backgroundparallax.min.js"></script>
<script>	
	$(document).on('ready',function(){		
		$(".parallax-section").parallax();
	})
</script>