[menu]

<!-- Hero #2
============================================= -->
<section id="hero-2" class="hero-2">
	<div id="hero-slider" class="hero-slider">
		
		<?php 
			$this->db->order_by('orden','ASC');
			foreach($this->db->get_where('slider',array('activo'=>1))->result() as $s): 
		?>
		<!-- Slide Item #1 -->
		<div class="item">
			<div class="item-bg bg-overlay">
				<div class="bg-section">
					<img src="<?= base_url('img/slider/'.$s->foto) ?>" alt="Background"/>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="hero-slide">
							<div class="slide-heading">
								<p><?= $s->subtitulo ?></p>
							</div>
							<div class="slide-title">
								<h2><?= $s->titulo ?></h2>
							</div>
							<div class="slide-action">
								<?php if(!empty($s->boton1) && count(explode(',',$s->boton1)==2)): list($label,$enlace) = explode(',',$s->boton1); ?>
									<a class="btn btn-primary" href="<?= base_url().trim($enlace) ?>"><?= $label ?></a>
								<?php endif ?>
								<?php if(!empty($s->boton1) && count(explode(',',$s->boton2)==2)): list($label,$enlace) = explode(',',$s->boton2); ?>
									<a class="btn btn-secondary pull-right" href="<?= base_url().trim($enlace) ?>"><?= $label ?></a>
								<?php endif ?>
							</div>
						</div>
					</div>
					<!-- .col-md-12 end -->
				</div>
				<!-- .row end -->
			</div>
			<!-- .container end -->
		</div>
		<!-- .item end -->
		<?php endforeach ?>
		
	</div>
	<!-- #hero-slider end -->
</section>

<!-- Call To Action #2
============================================= -->
<section id="cta-2" class="cta pb-0 pt-0">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="cta-2 bg-theme">
					<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7">
							<div class="cta-icon">
								<img src="[base_url]theme/theme/assets/images/ser/21.png" alt="" style="width:90px">
							</div>
							<div class="cta-devider" style="margin-left: 110px";>
							</div>
							<div class="cta-desc">
								<p>SERVICIO A DOMICILIO EXPRESS</p>
								<h5>¿DONDE Y A QUÉ HORA QUIERES QUEDAR PARA REPARAR TU MÓVIL?</h5>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 cta-action text-right pull-right pull-none-xs">
							<a class="btn btn-primary btn-white mr-sm" href="<?= base_url() ?>tienda.html" ">Solicitar</a>
							<a class="btn btn-secondary" href="#" data-toggle="modal" data-target="#model-quote" id="modelquote"">CONTACTAR</a>							
						</div>
					</div>
				</div>
				<!-- .cta-1 end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #cta-2 end -->

<!-- Shortcode #8 
============================================= -->
<section id="shortcode-8" class="shortcode-8 pb-0 pb-60-sm ">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-2 mb-0 text-center">
					<div class="heading-bg">
						<p class="mb-0">CÓMO FUNCIONA</p>
						<h2>Nuestro servicio</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
		</div>
		<!-- .row end -->
		<div class="clearfix mb-50">
		</div>
		<div class="row">
			<!-- .col-md-12 end -->
			<div class="col-xs-12 col-sm-6 col-md-4 text-right">
				<div class="row">
					<div class="col-xs-6 col-sm-12 col-md-12 feature">
						<a href="<?= base_url() ?>tienda.html">
							<div class="feature-icon right">
								<img src="[base_url]theme/theme/assets/images/ser/22.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase">Selecciona dispositivo</h4>
							<p>Selecciona tu dispositivo y la avería <br/>para valorar la reparación.</p>
						</a>
					</div>
					<!-- .col-md-12 end -->
					<div class="col-xs-6 col-sm-12 col-md-12 feature">
						<a href="<?= base_url() ?>tienda.html">
							<div class="feature-icon right">
								<img src="[base_url]theme/theme/assets/images/ser/23.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase">Comprueba el precio</h4>
							<p>Puedes comprobar el precio <br class="hidden-xs">con solamente 2 clics, sin sorpresas</p>
						</a>
					</div>
					<!-- .col-md-12 end -->
					
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12 col-sm-4 col-md-4 hidden-xs hidden-sm">
				<div class="feature-img">
					<img src="<?= base_url() ?>theme/theme/assets/images/features/3.png" alt="image"/>
				</div>
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="row">
					<div class="col-xs-6 col-sm-12 col-md-12 feature">
						<a href="<?= base_url() ?>tienda.html">
						<div class="feature-icon">
							<img src="[base_url]theme/theme/assets/images/ser/24.png" alt="" style="width:45px; margin-bottom: 14px;">
						</div>
						<h4 class="text-uppercase">Desplazamiento a tu domicilio</h4>
						<p>Un técnico se desplazará al sitio <br class="hidden-xs">y hora indicado a reparar tu terminal</p>
						</a>
					</div>
					<!-- .col-md-4 end -->
					<div class="col-xs-6 col-sm-12 col-md-12 feature">
						<a href="<?= base_url() ?>tienda.html">
							<div class="feature-icon">
								<img src="[base_url]theme/theme/assets/images/ser/25.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase">Reparación dispositivo</h4>
							<p>Sin perderlo de vista, en 30 minutos<br class="hidden-xs">tu terminal estará reparado</p>
						</a>
					</div>
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-4 end-->
		</div>
	</div>
</section>

<!-- Service Block #8
============================================= -->
<section id="service-8" class="service service-2 service-8 bg-gray pb-0">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿SE TE HA ROTO EL MÓVIL?</p>
						<h2>TE LO REPARAMOS DONDE NOS DIGAS Y SOLO EN 30 MINUTOS</h2>
					</div>
					<p class="mb-0 mt-sm">Selecciona la marca de tu dispositivo y sigue el proceso de reparación que te pedimos a continuación, si tu dispositivo no está en nuestro listado de marcas o tienes una avería que no contemplamos, no dudes en ponerte en contacto con nosotros y te podremos ayudar.</p>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-6 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<!-- Nav Tab #1 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/1ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/1y.png" alt="icon"/>
								</div>
								apple</a>
						</li>
						<!-- Nav Tab #2 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/2ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/2y.png" alt="icon"/>
								</div>
								samsung </a>
						</li>
						<!-- Nav Tab #3 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/3ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/3y.png" alt="icon"/>
								</div>
								xiaomi </a>
						</li>
						<!-- Nav Tab #4 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/4ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/4y.png" alt="icon"/>
								</div>
								huawei </a>
						</li>
						<!-- Nav Tab #5 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/5ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/5y.png" alt="icon"/>
								</div>
								bq </a>
						</li>
						<!-- Nav Tab #6 -->
						<li class="col-xs-6 col-sm-4 col-md-2" role="presentation">
							<a href="[base_url]tienda.html">
								<div class="service-icon">
									<img src="[base_url]theme/theme/assets/images/marques/6ay.png" alt="icon"/>
									<img src="[base_url]theme/theme/assets/images/marques/6y.png" alt="icon"/>
								</div>
								OTROS </a>
						</li>
					</ul>
				</div>
			</div>
			<!-- .col-md-12 end-->
		</div>
		<!-- .row end -->
	</div>
</section>









<!-- Service Block #8
============================================= -->
<section id="reparacionSection" class="reparacionSection service service-2 service-8 bg-gray p-0">	
	<!-- .container end -->
	<div class="container-fluid bg-theme p-0">
		<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/111.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/111.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/2w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>CAMBIO DE PANTALLAS</h3>				
				<p>Si tienes el cristal agrietado, el táctil no funciona, la pantalla no da imagen, manchas en la pantalla o barras de colores...</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>

		<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/11.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/11.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/3w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>DAÑOS LÍQUIDOS</h3>
				<p>Averías de móvil más comunes cuando se moja un móvil: El teléfono no enciende, la pantalla no responde correctamente, no carga, cámara, micrófono, altavoz o botones no funcionan de repente, la vibración de tu teléfono, el wifi o la cobertura dejan de funcionar, cuando conectas tus auriculares no escuchas nada.</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>

		<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/1111.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/1111.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/4w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>SUSTITUCIÓN DE BATERÍA</h3>
				<p>Esta pieza suele fallar por desgaste, manipulación, un golpe o por fallo eléctrico de la pieza. Hace falta cambiarla cuando la duración de la batería no sea la adecuada, se descargue velozmente o bien no se cargue (puede ser problema de placa base o conector de carga.</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>

<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/1.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/1.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/1w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>SUSTITUCIÓN CONECTOR DE CARGA</h3>
				<p>Esta reparación no soluciona problemas de placa base o batería, ni ningún otro problema salvo el indicado.</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>

		<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/111111.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/11111.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/10w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>BOTÓN DE ENCENDIDO</h3>
				<p>En ocasiones tenemos que ejercer mucha presión sobre el botón de encendido, es por ello que se ha de sustituir el pulsador Power. También puede ser por un problema en placa base, batería o cargador.</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>

		<!-- Panel #1 -->
		<div role="tabpanel" class="bg-overlay ">
			<div class="bg-section"  style="background:url([base_url]theme/theme/assets/images/services/full/1115.jpg) no-repeat; background-size:cover;">
				<img src="[base_url]theme/theme/assets/images/services/full/1115.jpg " alt="Background"/>
			</div>
			<div class="col-md-6 col-md-offset-6 col-content">
				<img src="[base_url]theme/theme/assets/images/services/icons/i48/11w.png" alt="" style="width:48px; margin-bottom:10px;">
				<h3>ERRORES DE SISTEMA</h3>
				<p>Los síntomas más habituales son los bloqueo del terminal, lentitud de sitema o incluso que se quede el logo o se reinicie constantemente el sistema.</p>				
			</div>
			<!-- .col-md-6 end -->
			
		</div>
	</div>
	<!-- .container-fluid end -->
</section>







<!-- Shortcode #6 
============================================= -->
<section id="shortcode-6" class="shortcode-6 text-center-xs testimonial-1"style=" background-color: rgba(63, 158, 182, 0.13) !important;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 mb-0-sm">
				<div class="heading heading-4">
					<div class="heading-bg heading-right">
						<p class="mb-0">NUESTRA EXPERIENCIA NOS AVALA !</p>
						<h2>SERVICIO TÉCNICO Profesional</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<h5>Móviles Sat servicio líder de reparación de móviles a domicilio.<br>Tu nos dices donde y cuando.</h5>
				<p>Contamos con más de 15 años de experiencia en el sector de la electrónica. Más de 52.000 dispositivos reparados en los 7 años que llevamos dedicados solo a la reparación de móviles y tablets.</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 text-center-sm mb-30-sm">
				<div class="feature feature-2 feature-bordered">
					<div class="feature-icon">
						<i class="lnr lnr-license font-40 color-theme"></i>
					</div>
					<h4 class="text-uppercase font-16">Buenos Profesionales</h4>
				</div>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 text-center-sm mb-30-sm">
				<div class="feature feature-2 feature-bordered">
					<div class="feature-icon">
						<i class="lnr lnr-diamond font-40 color-theme"></i>
					</div>
					<h4 class="text-uppercase font-16">La mejor Calidad</h4>
				</div>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 text-center-sm mb-30-sm">
				<div class="feature feature-2 feature-bordered">
					<div class="feature-icon">
						<i class="lnr lnr-funnel font-40 color-theme"></i>
					</div>
					<h4 class="text-uppercase font-16">Precio inmejorables</h4>
				</div>
			</div>
			<!-- .col-md-6 end -->
			<div class="col-xs-12 col-sm-6 col-md-3 text-center-sm mb-30-sm">
				<div class="feature feature-2 feature-bordered">
					<div class="feature-icon" style=" margin-bottom: 4px;">
						<i class="lnr lnr-smile font-40 color-theme"></i>
					</div>
					<h4 class="text-uppercase font-16">Garantía 6 meses* </h4>
					<p style=" font-size: 12px;line-height: 8px;">*consulta tu comunidad</p>
				</div>
			</div>
			<!-- .col-md-6 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Shortcode #7 
============================================= -->
<section id="shortcode-7" class="shortcode-7 section-img bg-gray">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5 col-img">
				<div class="col-divider">
				</div>
				<div class="col-bg">
					<img src="<?= base_url() ?>theme/theme/assets/images/features/11.jpg" alt="Background"/>
				</div>
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12 cpl-sm-12 col-md-3 bg-theme col-heading">
				<h2 style=" color: #ffffff;display: inline-block;">PRUEBA NUESTRO SERVICIO ESTRELLA Y SOLICITA LA REPARACIÓN.</h2>
				<h5 style=" color: #ffffff;display: inline-block;">En tu casa, la oficina, en tu bar o donde más te apetezca. Somos expertos en iPhone, iPad, Huawei y Samsung.</h5>
				
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-progress" style=" background-color: rgba(63, 158, 182, 0.8) !important;">
				
				<!-- progress 1 -->
				<div class="progressbar">
					<div class="progress-title">
						<span class="title">PANTALLAS ROTAS</span>
						<span class="value">75%</span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
						</div>
					</div>
				</div>
				<!-- .progressbar end -->
				
				<!-- progress 2 -->
				<div class="progressbar">
					<div class="progress-title">
						<span class="title">Tabletas</span>
						<span class="value">50%</span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
						</div>
					</div>
				</div>
				<!-- .progressbar end -->
				
				<!-- progress 3 -->
				<div class="progressbar">
					<div class="progress-title">
						<span class="title">Móviles mojados</span>
						<span class="value">90%</span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
						</div>
					</div>
				</div>
				<!-- .progressbar end -->
				
				<!-- progress 4 -->
				<div class="progressbar">
					<div class="progress-title">
						<span class="title">Reemplazo de Baterías</span>
						<span class="value">60%</span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						</div>
					</div>
				</div>
				<!-- .progressbar end -->
				
				<!-- progress 5 -->
				<div class="progressbar">
					<div class="progress-title">
						<span class="title">Sistemas operativos</span>
						<span class="value">95%</span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
						</div>
					</div>
				</div>
				<!-- .progressbar end -->
				
			</div>
			<!-- .col-md-4 end -->
		</div>
	</div>
</section>


<!-- Testimonials #1
============================================= -->
<section id="testimonials" class="testimonial testimonial-1 bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-3 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿QUÉ OPINAN NUESTROS CLIENTES?</p>
						<h2>TESTIMONIOS</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div id="testimonial-oc" class="testimonial-carousel">
					
					<!-- testimonial item #1 -->
					<?php foreach($this->elements->testimonios(array('mostrar_main'=>1))->result() as $t): ?>
						<div class="testimonial-item">
							<div class="testimonial-content">
								<div class="testimonial-img">
									<i class="fa fa-quote-left"></i>
									<img src="<?= $t->foto ?>" alt="author"/>
								</div>
								<p><?= $t->testimonio ?></p>
							</div>
							<div class="testimonial-divider">
							</div>
							<div class="testimonial-meta">
								<strong><?= $t->nombre ?></strong>, <?= $t->red ?>
							</div>
						</div>
						<!-- .testimonial-item end -->
					<?php endforeach ?>
					
					
				</div>
			</div>
			<!-- .col-md-12 end -->
			
		</div>
		<!-- .row end -->
		
	</div>
	<!-- .container end -->
	
</section>
<!-- #testimonials end -->

<!-- Shortcode #5 
============================================= -->
<section id="shortcode-5" class="shortcode-5 pb-0">
	<div class="container">
		
		<!-- .row end -->
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="heading heading-4">
							<div class="heading-bg heading-right">
								<p class="mb-0">TIENES DUDAS DE MÓVILES SAT</p>
								<h2>FAQ</h2>
							</div>
						</div>
						<!-- .heading end -->
					</div>
				</div>
				<div class="panel-group accordion" id="accordion02" role="tablist" aria-multiselectable="true">
					<?php 
						$this->db->limit(4);
						foreach($this->db->get_where('faqs')->result() as $t): 
					?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse<?= $t->id ?>" aria-expanded="false" aria-controls="collapse01"> <?= $t->titulo ?> </a>
									<span class="icon"></span>
								</h4>
							</div>
							<div id="collapse<?= $t->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<?= $t->respuesta ?>
								</div>
							</div>
						</div>
					<?php endforeach ?>					
				</div>
				<!-- End .Accordion-->
				<a class="btn btn-secondary btn-block" href="[base_url]faq.html">VER TODAS LAS PREGUNTAS FRECUENTES <i class="fa fa-plus"></i></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="feature">
					<div class="shortcode-2 about-home">
						<div class="cta-form">
							<div class="cta cta-2 bg-theme">
								<div class="cta-icon">
									<i class="lnr lnr-bullhorn" style="line-height: 10px;"></i>
								</div>
								<div class="cta-devider">
								</div>
								<div class="cta-desc cta-descpl">
									<p>¿TIENES DUDAS?</p>
									<h5>RELLENA Y ENVIA</h5>
								</div>
								
							</div>
							<!-- .cta-2 end -->
							<div class="form">
								<form action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'#quote-result'); return false;">
									<input type="text" class="form-control" name="nombre" id="name" placeholder="Nombre"/>
									<input type="email" class="form-control" name="email" id="email" placeholder="Email"/>
									<input type="text" class="form-control" name="telefono" id="telephone" placeholder="Teléfono"/>
									<textarea class="form-control" name="extras[mensaje]"  id="quote" placeholder="Mensaje" rows="2"></textarea>
									<label for="" style="font-weight:3s00;font-size: 13px;margin-bottom: 20px;">
										<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
									</label>
									<button type="submit" class="btn btn-primary btn-black btn-block">ENVIAR</button>
									<!--Alert Message-->
									<div id="quote-result" class="mt-xs" style="max-width: 266px;">
									</div>
								</form>
							</div>
						</div>
					</div>
					<img class="img-responsive hidden-xs hidden-sm" src="<?= base_url() ?>theme/theme/assets/images/features/2.jpg" alt="feature">
				</div>
			</div>
			
		</div>
	</div>
</section>




<!-- Shortcode #5 
============================================= -->
<section id="shortcode-5" class="shortcode-5 pb-50">
	
		
		<!-- .row end -->
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12">				
				<div class="panel-group accordion" id="pasosacoord" role="tablist" aria-multiselectable="true">					
					<!-- Panel 01 -->
					<div class="panel panel-default" style="padding:0; border: 0;">
						<div class="" role="tab" id="headingOne">
							<section id="cta-1" class="cta pb-0">
								<div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="cta-1 bg-theme">
												<div class="container">
													<div class="row">
														<div class="bannerMainBarraRecogida col-xs-12 col-sm-12 col-md-1">
															<div class="cta-img">
																<img src="<?= base_url() ?>theme/theme/assets/images/call/cta-1.png" alt="cta">
															</div>
														</div>
														<!-- .col-md-2 end -->
														<div class="col-xs-12 col-sm-12 col-md-7 cta-devider text-center-xs">
															<div class="cta-desc">
																<p>SOLICITUD DE RECOGIDA!</p>
																<h5>¿QUIERES QUE RECOJAMOS TU DISPOSITIVO?</h5>
															</div>
														</div>
														<!-- .col-md-7 end -->
														<div class="col-xs-12 col-sm-12 col-md-2 pull-right pull-none-xs">
															<div class="cta-action">
																<a class="btn btn-secondary" data-toggle="modal" href="#recogidaModal" aria-expanded="true" aria-controls="collapse02-1">SOLICITA RECOGIDA</a>																
															</div>
														</div>
														
														<!-- .col-md-2 end -->
													</div>
												</div>
											</div>
											<!-- .cta-1 end -->
										</div>
										<!-- .col-md-12 end -->
									</div>
									<!-- .row end -->
								</div>
								<!-- .container end -->
							</section>
						</div>


						<div id="pasosacoord-1" class="panel-collapse collapse in bg-gray" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" style="padding: 50px 0;">
								<section id="service-3" class="service service-3 p-0">
									<div class="container">
										<div class="row">											
											
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="row">
													<!-- Service Block #1 -->
													<div class="col-xs-12 col-sm-6 col-md-3 service-block service-blockMain">
														<div class="service-img">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/1y.png" alt="icons">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/1.png" alt="icons">
														</div>
														<div class="service-content">
															<div class="service-desc">
																<h4>1. Ponte en contacto con nosotros</h4>
																<p>Rellena el formulario y te llamamos, o marca el 660 07 47 97 y una operadora atenderá tus dudas. </p>
																
																
															</div>
														</div>
													</div>
													<!-- .col-md-3 end -->
													
													<!-- Service Block #2 -->
													<div class="col-xs-12 col-sm-6 col-md-3 service-block service-blockMain">
														<div class="service-img">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/2y.png" alt="icons">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/2.png" alt="icons">
														</div>
														<div class="service-content">
															<div class="service-desc">
																<h4>2. Recogida de tu dispositivo</h4>
																<p>Nuestra empresa de mensajería recoge el terminal en tu domicilio o trabajo en menos de 24 horas.</p>
																
																
															</div>
														</div>
													</div>
													<!-- .col-md-3 end -->
													
													<!-- Service Block #3 -->
													<div class="col-xs-12 col-sm-6 col-md-3 service-block service-blockMain">
														<div class="service-img">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/3y.png" alt="icons">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/3.png" alt="icons">
														</div>
														<div class="service-content">
															<div class="service-desc">
																<h4>3. REPARACIÓN de tu dispositivo</h4>
																<p>Un técnico lo recibirá y empezará con la valoración para su presupuesto y/o reparación.</p>
																
																
															</div>
														</div>
													</div>
													<!-- .col-md-3 end -->
													
													<!-- Service Block #4 -->
													<div class="col-xs-12 col-sm-6 col-md-3 service-block service-blockMain">
														<div class="service-img">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/4y.png" alt="icons">
															<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/4.png" alt="icons">
														</div>
														<div class="service-content">
															<div class="service-desc">
																<h4>4. Te devolvemos tu dispositivo EN TU CASA</h4>
																<p>Una vez reparado, lo recibirás en casa o trabajo. Así de fácil y rápido es reparar tu dispositivo con Móviles SAT.</p>
																
															</div>
														</div>
													</div>
													<!-- .col-md-3 end -->
													
												</div>
											</div>
											<!-- .col-md-12 end -->
											
										</div>
										<!-- .row end -->
										<div style="margin-top:30px">
											<a class="btn btn-secondary btn-block"  data-toggle="modal" href="#recogidaModal" aria-expanded="true" aria-controls="collapse02-1">Solicita recogida <i class="fa fa-plus"></i></a>
										</div>
									</div>													

									<!-- .container end -->
								</section>
								
							</div>







						</div>
					</div>
					
				</div>
				<!-- End .Accordion-->				
			</div>
			
			
		</div>
	
</section>








<!-- Blog
============================================= -->
<section id="blog">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
				<div class="heading heading-3 text-center">
					<div class="heading-bg">
						<p class="mb-0">ACTUALIDAD MÓVILES SAT</p>
						<h2>Blog y Noticias</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-8 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			[foreach:blog]
			<!-- Entry Article #4 -->
			<div class="col-xs-12 col-sm-6 col-md-4 entry">
				<div class="entry-img">
					<a href="[link]">
						<img src="[foto]" alt="title"/>
					</a>
				</div>
				<!-- .entry-img end -->
				<div class="entry-meta clearfix">
					<ul class="pull-left">
						<li class="entry-format">
							<i class="fa fa-pencil"></i>
						</li>
						<li class="entry-date">
							[fecha]
						</li>
					</ul>
					<ul class="pull-right text-right">
						<li class="entry-cat">Por:
							<span>
							<a href="#">[user]</a>
							</span>
						</li>						
						<li class="entry-views"><i class="fa fa-eye"></i> [vistas]</li>
					</ul>
				</div>
				<!-- .entry-meta end -->
				<div class="entry-title">
					<h3>
						<a href="[link]">[titulo]</a>
					</h3>
				</div>
				<!-- .entry-title end -->
				<div class="entry-content">
					<p>[texto]</p>
					<a class="entry-more" href="[link]"><i class="fa fa-plus"></i>
						<span>Leer más</span>
					</a>
				</div>
				<!-- .entry-content end -->
			</div>
			<!-- .entry end -->
			[/foreach]
			<!-- Entry Article #5 -->
			
			
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<a class="btn btn-secondary" href="<?= base_url('blog') ?>">Ver más <i class="fa fa-plus"></i></a>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Call To Action #8
============================================= -->
<section id="cta-8" class="bg-overlay bg-overlay-theme cta cta-6">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/call/1.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="text-capitalize mb-0 color-white">¿QUIERES FORMAR PARTE DE NUESTRO EQUIPO?</p>
				<h2 class="mb-xs color-white">NO ESPERES MÁS, <br>TE OFRECEMOS LOS QUE OTROS NO PUEDEN, TE DAMOS LOS QUE OTROS NO SABEN.</h2>
				<a class="btn btn-secondary btn-white" href="<?= base_url() ?>franquicias.html">Franquíciate</a>
				
			</div>
			<!-- .col-md-8 end -->
			<div class="col-md-5 hidden-xs hidden-sm">
				<div class="cta-img">
					<img src="<?= base_url() ?>theme/theme/assets/images/call/1.png" alt="call to action"/>
				</div>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #cta-8 end -->

<!-- Shortcode #9 
============================================= -->
<section id="clients" class="shortcode-9">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">TRABAJAMOS CON LOS MEJORES</p>
						<h2>NUESTRAS MARCAS</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/1.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
			
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/2.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
			
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/3.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
			
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/4.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
			
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/5.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
			
			<!-- Client Item -->
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="brand last">
					<img class="img-responsive center-block" src="<?= base_url() ?>theme/theme/assets/images/clients/6.png" alt="brand">
				</div>
			</div>
			<!-- .col-md-2 end -->
		</div>
		<!-- .row End -->
	</div>
	<!-- .container end -->
</section>
<!-- #clients end-->

<!-- Shortcode #9 
============================================= -->
<section id="clients" class="shortcode-9 bg-gray pb-0" style="margin-bottom: -50px;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿DÓNDE PUEDES ENCONTRARNOS?</p>
						<h2>TU tienda MÁS CERCANA</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			<div id="googleMap" style="width:100%;height: 433px;"></div>
		</div>
		<!-- .row End -->
	</div>
	<!-- .container end -->
</section>
<!-- #clients end-->

<!-- Modal -->
<div class="modal fade model-quote" id="model-quote" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-home"></i>
				</div>
				<div class="model-divider" >
					<div class="model-title">
						<p>¿SE TE HA ROTO EL MÓVIL?</p>
						<h6>DINOS QUÉ LE OCURRE</h6>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<form action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'#pop-quote-result2'); return false;">
					<input type="text" class="form-control" name="nombre" id="name" placeholder="Nombre" required/>
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="telefono" id="telephone" placeholder="Teléfono" required/>
					<textarea class="form-control" name="extras[mensaje]"  id="quote" placeholder="Mensaje" rows="2" required></textarea>
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
					<!--Alert Message-->
					<div id="pop-quote-result2" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>
<!-- .model-quote end -->

<div class="modal fade model-quote" id="recogidaModal" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-rocket"></i>
				</div>
				<div class="model-divider">
					<div class="model-title">
						<p>RECOGEMOS TU TERMINAL</p>
						<h6>FORMULARIO DE RECOGIDA.</h6>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<form action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'#pop-quote-result'); return false;">
					<input type="text" class="form-control" name="nombre" id="name" placeholder="Nombre" required/>
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="telefono" id="telephone" placeholder="Teléfono" required/>
					<input type="text" class="form-control" name="extras[direccion]" id="telephone" placeholder="Dirección" required/>
					<input type="text" class="form-control" name="extras[poblacion]" id="telephone" placeholder="Población" required/>
					<input type="text" class="form-control" name="extras[provincia]" id="telephone" placeholder="Provincia" required/>
					<input type="text" class="form-control" name="extras[terminal]" id="telephone" placeholder="Marca y modelo del terminal" required/>
					<input type="hidden" name="asunto" value="<b>Solicitud de recogida</b>">
					<textarea class="form-control" name="extras[mensaje]" placeholder="¿Que problema tiene tu terminal?" rows="2" required></textarea>
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
					<!--Alert Message-->
					<div id="pop-quote-result" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>
<!-- .model-quote end -->

<div class="modal fade model-quote" id="model-quote6" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-rocket"></i>
				</div>
				<div class="model-divider">
					<div class="model-title">
						<p>ÚNETE A MOVILSAT</p>
						<h6>PIDE INFORMACIÓN.</h6>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<form id="pop-quote-form" action="assets/php/sendpopquote.php" method="post">
					<input type="text" class="form-control" name="quote-name" id="name" placeholder="Nombre" required/>
					<input type="email" class="form-control" name="quote-email" id="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="quote-telephone" id="telephone" placeholder="Telfono" required/>
					<textarea class="form-control" name="quote-message"  id="quote" placeholder="Mensaje" rows="2" required></textarea>
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
					<!--Alert Message-->
					<div id="pop-quote-result" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>
<!-- .model-quote end -->
[footer]

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script> 
<script type="text/javascript" src="[base_url]theme/theme/assets/js/jquery.gmap.min.js"></script>
<script type="text/javascript">
	var pos = new google.maps.LatLng(<?php 
		if(empty($_SESSION['franquicia'])){
			echo '41.5799931,1.6080985';
		}else{
			echo $_SESSION['franquicia']->lat.','.$_SESSION['franquicia']->lng;
		}
	?>);
	var content = document.getElementById('googleMap');
	var opts = {
		center: pos,
		zoom:16,
		styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
	};

	var map = new google.maps.Map(content,opts);
	<?php foreach($this->elements->franquicias()->result() as $f): ?>
		var marker<?= $f->id ?> = new google.maps.Marker({
			position:new google.maps.LatLng(<?= $f->mapa[0] ?>,<?= $f->mapa[1] ?>),
			map:map,
			icon:URL+'img/map-marker.png'
		});
	<?php endforeach ?>

	$("#reparacionSection > div").owlCarousel({
		items:1,
		autoplay:true,
		loop: true,
	});
</script>