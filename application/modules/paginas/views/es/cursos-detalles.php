[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/444.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>CURSOS</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">CURSOS DETALLES</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<section class="hidden-xs single-service">
	<div class="container">
		<div class="row">
			<div class="heading heading-4">
				<div class="heading-bg heading-right">
					<p class="mb-0">CONOCE FECHAS Y PRECIOS </p>
					<h2>CONTENIDO DE LOS CURSOS</h2>
				</div>
			</div>
			<div class="sidebar sidebar-services col-xs-12 col-sm-4 col-md-3">
				
				<!-- Categories
				============================================= -->
				<div class="widget widget-categories">
					<div class="widget-content">
						<ul class="list-unstyled">
							<?php
								$cursos = $this->elements->cursos(array('pack'=>0));
								foreach($cursos->result() as $n=>$c):
							?>
							<li <?= $n==0?'class="active"':'' ?> data-toggle="tab" aria-controls="home" role="tab" data-target="#curso<?= $c->id ?>" style="cursor:pointer">
								<h5 href="#curso<?= $c->id ?>" data-toggle="tab" aria-controls="home" role="tab" style=" margin-bottom: 0px;" ><?= $c->nombre ?></h5>
								<a href="#curso<?= $c->id ?>" data-toggle="tab" aria-controls="home" role="tab" ><?= $c->subtitulo ?></a>
							</li>
							<?php endforeach ?>
							
						</ul>
					</div>
				</div>
				
				<!-- Download
				============================================= -->
				<div class="widget widget-download">
					<div class="widget-title">
						<h3>PACKS</h3>
					</div>
					<p class="mb-10">SELECCIONA TU PACK AHORRO Y COMIENZA GANANDO </p>
					<div class="widget-content">
						
						<?php
							$packs = $this->elements->cursos(array('pack'=>1));
							foreach($packs->result() as $c): $n++;
						?>
						<div class="download download-pdf preciosPacks">
							<a href="#pack<?= $c->id ?>" data-toggle="tab" aria-controls="home" role="tab">
								<div class="download-desc">
									<div class="download-desc-icon">
										<img src="<?= $c->icono ?>" alt="icon" style=" margin-top: 9.4px;"><h4 style=" float: right;margin-left: 10px;margin-top: 15px;n-left: 10px; color:rgba(234, 229, 231, 0.43);">PACK </h4>
									</div>
									
									<h4><?= $c->nombre ?></h4>
								</div>
								<div class="download-icon">
									<i></i>
									<?php if(!empty($c->precio_tachado)): ?>
									<div class="preciotachado">
										<span>
											<?= $c->precio_tachado ?>€
										</span>
									</div>
									<?php endif ?>
									<span style="color:white; font-size:28px; font-weight: 600">
										<?= moneda($c->precio) ?>
									</span>
								</div>

							</a>							
						</div>
						<?php if(!empty($c->regalo)): ?>
							<div class="cursoDestacadoStickInner">
								Regalo
								<span><?= $c->regalo ?></span>
							</div>
						<?php endif ?>
						<?php endforeach ?>
						
						<!-- .download-pdf end -->
						
						
						<!-- .download-pdf end -->
					</div>
					
					
				</div>
				<div class="widget widget-download" style=" margin-bottom: 20px;">
					<div class="widget-title">
						<h3>FINANCIA TU CURSO</h3>
					</div>
					<h5  style=" color: #9dbc1b;">PAGO FRACCIONADO DESDE 26€ AL MES. <p>Pregúntanos a través del chat de forma fácil y rápida <p></h5>						
					<div class="widget-content">
						<img src="<?= base_url() ?>img/sequra-pago-fraccionado-150x150.jpeg" alt="">
					</div>					
				</div>
					
				<div class="panel-group accordion" id="accordion02" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
							
							<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse1fr" aria-expanded="false" aria-controls="collapse01">  Sin papeleos </a>
							<span class="icon"></span>
							</h4>
						</div>
						<div id="collapse1fr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>Como suena. No son necesarios papeles, ni nóminas, ni avales,... NADA de NADA. Lo único que necesitas es tu DNI o NIE y y facilitarnos unos datos para solicitar la FINANCIACION.</p>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse2fr" aria-expanded="false" aria-controls="collapse01"> Alta en 5 minutos </a>
								<span class="icon"></span>
							</h4>
						</div>
						<div id="collapse2fr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>Bueno quizá en 10 minutos o en 4 minutos, dependera de los rápido que nos escribas. Una vez te hemos dado de alta en un breve formulario, en cuestión segundos te llegara un SMS de cinfirmación.</p>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
							<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse3fr" aria-expanded="false" aria-controls="collapse01"> Hasta 12 meses </a>
							<span class="icon"></span>
							</h4>
						</div>
						<div id="collapse3fr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>El plazo de financiación es de 3, 6 o 12 meses, tu eleges en cuantos meses lo quieres financiar. Puedes solicitar financiación para el importe completo del curso o sólo una parte.</p>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
							<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse4fr" aria-expanded="false" aria-controls="collapse01"> ¿Cuánto vale financiar? </a>
							<span class="icon"></span>
							</h4>
						</div>
						<div id="collapse4fr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>El coste de la financiación depende de los meses que quieras financiar el curso, independientemente de la cantidad a financiar. Por cada mes financiado la tarifa fija de interés es de mínimo 5€.</p>								
							</div>
						</div>
						
					</div>
									
				</div>
				<!-- .sidebar end -->
			</div>


			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="row tab-content">
					
					<?php foreach($this->elements->cursos()->result() as $i=>$c): ?>
					<div id="<?= $c->pack==1?'pack':'curso'?><?= $c->id ?>" class="col-xs-12 col-sm-12 col-md-12 tab-pane <?= $i==0?'active':'' ?>">
						<?php $this->load->view($this->theme.'_cursos_content.php',array('c'=>$c)); ?> 
					</div>
					<?php endforeach ?>
					
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-9 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>



<!-- Services
============================================= -->
<section class="visible-xs cursosResp">
	<div class="container">
		<div class="panel-group accordion" id="accordion03" role="tablist" aria-multiselectable="false">
					
			<?php foreach($this->elements->cursos(array('pack'=>0))->result() as $i=>$c): ?>
				<!-- Panel 01 -->
				<div class="panel panel-default"  id="head<?= $c->id ?>">
					<div class="panel-heading" role="tab" id="headingOne">

						<h4 class="panel-title">
							<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#head<?= $c->id ?>" href="#collapsed<?= $c->id ?>" aria-expanded="false" aria-controls="collapse01"> 
								<?= $c->nombre ?> <br/>
								<small><?= $c->subtitulo ?></small>
							</a>
							<span class="icon"></span>
						</h4>
					</div>
					<div id="collapsed<?= $c->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<?= $c->texto_col1 ?>
							<?= $c->texto_col2 ?>
							<?php $this->load->view('_cursos_fechas',array('c'=>$c)); ?>
						</div>
					</div>
				</div>
				<!-- .panel end -->
			<?php endforeach ?>			
		</div>
		<!-- End .Accordion-->
	</div>
	<!-- .container end -->
</section>


<div class="visible-xs sidebar sidebar-services" style="border:none">

	<!-- Download
	============================================= -->
	<div class="widget widget-download">
		<div class="widget-title">
			<h3>PACKS</h3>
		</div>
		<p class="mb-10">SELECCIONA TU PACK AHORRO Y COMIENZA GANANDO </p>
		<div class="widget-content">
			
			<?php
				$packs = $this->elements->cursos(array('pack'=>1));
				foreach($packs->result() as $c): $n++;
			?>
					<div class="download download-pdf preciosPacks" id="headp<?= $c->id ?>">
						<a href="#collapsep<?= $c->id ?>" aria-controls="home" role="tab" role="button" data-toggle="collapse" data-parent="#headp<?= $c->id ?>" aria-expanded="false" aria-controls="collapse01">
							<div class="download-desc">
								<div class="download-desc-icon">
									<img src="<?= $c->icono ?>" alt="icon" style=" margin-top: 9.4px;"><h4 style=" float: right;margin-left: 10px;margin-top: 15px;n-left: 10px; color:rgba(234, 229, 231, 0.43);">PACK </h4>
								</div>
								
								<h4><?= $c->nombre ?></h4>
							</div>
							<div class="download-icon">
								<i></i>
								<?php if(!empty($c->precio_tachado)): ?>
								<div class="preciotachado">
									<span>
										<?= $c->precio_tachado ?>€
									</span>
								</div>
								<?php endif ?>
								<span style="color:white; font-size:28px; font-weight: 600">
									<?= moneda($c->precio) ?>
								</span>
							</div>
						</a>
					</div>
					<?php if(!empty($c->regalo)): ?>
						<div class="cursoDestacadoStickInner">
							Regalo
							<span><?= $c->regalo ?></span>
						</div>
					<?php endif ?>
					<div id="collapsep<?= $c->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<?= $c->texto_col1 ?>
							<?= $c->texto_col2 ?>
							<?php $this->load->view('_cursos_fechas',array('c'=>$c)); ?>
						</div>
					</div>	


			<?php endforeach ?>
			
			<!-- .download-pdf end -->
			
			
			<!-- .download-pdf end -->
		</div>
		
		
	</div>


	<div class="widget widget-download" style=" margin-bottom: 20px;">
		<div class="widget-title">
			<h3>FINANCIA TU CURSO</h3>
		</div>
		<h5  style=" color: #9dbc1b;">PAGO FRACCIONADO DESDE 26€ AL MES. <p>Pregúntanos a través del chat de forma fácil y rápida <p></h5>						
		<div class="widget-content">
			<img src="<?= base_url() ?>img/sequra-pago-fraccionado-150x150.jpeg" alt="">
		</div>					
	</div>
		
	<div class="panel-group accordion" id="accordionFr" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
				
				<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordionFr" href="#collapse1" aria-expanded="false" aria-controls="collapse01">  Sin papeleos </a>
				<span class="icon"></span>
				</h4>
			</div>
			<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<p>Como suena. No son necesarios papeles, ni nóminas, ni avales,... NADA de NADA. Lo único que necesitas es tu DNI o NIE y y facilitarnos unos datos para solicitar la FINANCIACION.</p>								
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordionFr" href="#collapse2" aria-expanded="false" aria-controls="collapse01"> Alta en 5 minutos </a>
					<span class="icon"></span>
				</h4>
			</div>
			<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<p>Bueno quizá en 10 minutos o en 4 minutos, dependera de los rápido que nos escribas. Una vez te hemos dado de alta en un breve formulario, en cuestión segundos te llegara un SMS de cinfirmación.</p>								
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
				<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordionFr" href="#collapse3" aria-expanded="false" aria-controls="collapse01"> Hasta 12 meses </a>
				<span class="icon"></span>
				</h4>
			</div>
			<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<p>El plazo de financiación es de 3, 6 o 12 meses, tu eleges en cuantos meses lo quieres financiar. Puedes solicitar financiación para el importe completo del curso o sólo una parte.</p>								
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
				<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordionFr" href="#collapse4" aria-expanded="false" aria-controls="collapse01"> ¿Cuánto vale financiar? </a>
				<span class="icon"></span>
				</h4>
			</div>
			<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<p>El coste de la financiación depende de los meses que quieras financiar el curso, independientemente de la cantidad a financiar. Por cada mes financiado la tarifa fija de interés es de mínimo 5€.</p>								
				</div>
			</div>
			
		</div>
						
	</div>
	<!-- .sidebar end -->

</div>







<?php foreach($this->elements->cursos()->result() as $i=>$c): ?>
<!-- Modal -->
<?php foreach($c->fechas->result() as $ii=>$f): ?>
<div class="modal fade model-quote cursoModal" id="cursoModal<?= $f->id ?>" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-graduation-hat"></i>
				</div>
				<div class="model-divider">
					<div class="model-title">
						<p>Inscripción al curso</p>
						<h6><?= $c->nombre ?></h6>
						<p><?= $f->poblacion ?></p>
						<p style="margin-top: -11px;"><?= strftime('%d %B %Y',strtotime($f->fecha)); ?></p>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<h4>¿Quieres realizar el curso? </h4>
				<ol>
					<li> Puedes cumplimentar este formulario y nosotros nos pondremos en contacto contigo</li>
					<li> Si lo deseas, puedes llamarnos al 93 803 19 24</li>
					<li> También dispones de WhatsApp 660 074 797, donde te responderemos al momento</li>
					<li> Puedes enviarnos un correo a curso@movilessat.es</li>
				</ol>
				<p>Tú decides, así de fácil te lo ponemos</p>
				<form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#pop-quote-result<?= $i.$ii ?>')" method="post">
					<input type="text" class="form-control" name="nombre" placeholder="Nombre completo" required/>
					<input type="email" class="form-control" name="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="telefono" placeholder="Teléfono" required/>
					<input type="text" class="form-control" name="extras[codigo_cupon]" placeholder="Código cupón (Si dispone)" />
					<input type="hidden" name="asunto" value="Inscripción para el curso <?= $c->nombre ?> en la población <?= $f->poblacion ?> para la fecha <?= strftime('%d %B %Y',strtotime($f->fecha)); ?>">
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Inscribirse</button>
					<!--Alert Message-->
					<div id="pop-quote-result<?= $i.$ii ?>" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>
<?php endforeach ?>
<?php endforeach ?>
<!-- .model-quote end -->
[footer]
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script> 
<script type="text/javascript" src="[base_url]theme/theme/assets/js/jquery.gmap.min.js"></script>