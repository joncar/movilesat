<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>1. Empezamos,
			<span class="color-theme"> selecciona la marca de tu dispositivo</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row">
	<?php foreach($marcas->result() as $m): ?>
		<!-- product #1 -->
		<div class="col-xs-6 col-sm-4 col-md-3 product-item clearfix" onclick="selMarca(<?= $m->id ?>)">
			<div class="product-img">
				<img src="<?= $m->foto ?>" alt="product">
				<div class="product-hover">
					<div class="product-cart">
						<a class="btn btn-secondary btn-block" href="javascript:selMarca(<?= $m->id ?>)">Seleccionar</a>
					</div>
				</div>
			</div>		
			<div class="product-bio">
				<p class="product-price">
					<?= $m->nombre ?>						
				</p>
			</div>		
		</div>
	<?php endforeach ?>
</div>
<!-- .row end -->