[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="[base_url]theme/theme/assets/images/page-title/2.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>PREGUNTAS FRECUENTES</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">FAQS</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Services
============================================= -->
<section>
	<div class="container">
		<div class="row">
			<div class="sidebar sidebar-full shortcode-8  col-xs-12 col-sm-12 col-md-3" style="background-color: rgba(63, 158, 182, 0.13) !important; padding:30px 30px 0 30px;">
				
								
				<!-- Download
				============================================= -->
				<div class="faqSidebar widget widget-download">
					<div class="widget-title">
						<h2 style="line-height: 30px; text-transform: uppercase; font-size:24px">¿Cómo funciona Móviles SAT en un solo click?</h2>
					</div>
					<div class="widget-content">
						
						<div class="col-xs-12 col-sm-12 col-md-12 feature p-0" style="margin-bottom:25px;">
							<div class="feature-icon" style="display: inline-block;">
								<img src="[base_url]theme/theme/assets/images/ser/22.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase" style="font-size:16px;">Selecciona dispositivo</h4>
							<p style="line-height: 20px;">Selecciona tu dispositivo y la averia <BR>para valorar la reparación.</p>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 feature p-0" style="margin-bottom:25px;">
							<div class="feature-icon" style="display: inline-block;">
								<img src="[base_url]theme/theme/assets/images/ser/23.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase" style="font-size:16px;">Comprueba el precio</h4>
							<p style="line-height: 20px;">Puedes comprobar el precio <BR>con solamente 2 clics, sin sorpresas</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 feature p-0" style="margin-bottom:25px;">
							<div class="feature-icon" style="display: inline-block;">
								<img src="[base_url]theme/theme/assets/images/ser/24.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase" style="font-size:16px;">Desplazamiento a tu domicilio</h4>
							<p style="line-height: 20px;">Un técnico se desplazará al sitio <BR>y hora indicado a reparar tu terminal</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 feature p-0" style="margin-bottom:25px;">
							<div class="feature-icon" style="display: inline-block;">
								<img src="[base_url]theme/theme/assets/images/ser/25.png" alt="" style="width:45px; margin-bottom: 14px;">
							</div>
							<h4 class="text-uppercase" style="font-size:16px;">Reparación dispositivo</h4>
							<p style="line-height: 20px;">Sin perderlo de vista, en 30 minutos<BR>tu terminal estará reparado</p>
						</div>
	
						<div class="col-xs-12">
							<div class="feature-img" style="height: 340px;">
								<img src="<?= base_url() ?>theme/theme/assets/images/features/31.png" alt="image"/>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- .sidebar end -->
			<div class="col-xs-12 col-sm-12 col-md-9 about-1">
				<div class="panel-group accordion" id="accordion02" role="tablist" aria-multiselectable="true">
					
					<?php foreach($this->db->get_where('faqs',array('idioma'=>$_SESSION['lang']))->result() as $t): ?>
						<!-- Panel 01 -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse<?= $t->id ?>" aria-expanded="false" aria-controls="collapse01"> <?= $t->titulo ?> </a>
									<span class="icon"></span>
								</h4>
							</div>
							<div id="collapse<?= $t->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<?= $t->respuesta ?>
								</div>
							</div>
						</div>
						<!-- .panel end -->
					<?php endforeach ?>
					
				</div>
				<!-- End .Accordion-->
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]