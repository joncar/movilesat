[menu]
<!-- Page Title
============================================= -->
<section class=" bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/111.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>contáctanos en</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">contacto</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Google Maps
============================================= -->
<section class="google-maps pb-0 pt-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 pr-0 pl-0">
				<div id="googleMap" style="width:100%;height:413px;">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- .google-maps end -->

<!-- Contact #2
============================================= -->
<section id="contact" class="contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-4">
					<div class="heading-bg heading-right">
						<p class="mb-0">QUEREMOS OIRTE!</p>
						<h2>Contactános</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 widgets-contact mb-60-xs">
						<div class="widget">
							<div class="widget-contact-icon pull-left">
								<i class="lnr lnr-store"></i>
							</div>
							<div class="widget-contact-info">
								<p class="">Visíta nuestras tiendas</p>
								<p class="text-capitalize font-heading">
									<a href="<?= base_url() ?>franquicias.html#resumen"><u>Nuestras Tiendas</u></a>
								</p>
							</div>
						</div>
						<!-- .widget end -->
						<div class="clearfix">
						</div>
						<div class="widget">
							<div class="widget-contact-icon pull-left">
								<i class="lnr lnr-envelope"></i>
							</div>
							<div class="widget-contact-info">
								<p class="text-capitalize ">email</p>
								<p class="text-capitalize font-heading"><a href="mailto:movilessat@movilessat.es">movilessat@movilessat.es</a></p>
							</div>
						</div>
						<!-- .widget end -->
						<div class="clearfix">
						</div>
						<div class="widget">
							<div class="widget-contact-icon pull-left" style="margin-bottom: 20px;">
								<i class="lnr lnr-phone"></i>
							</div>
							<div class="widget-contact-info">
								<p class="text-capitalize">Llámanos al</p>
								<p class="text-capitalize font-heading"><a href="tel:34938031924">+ 34 93 803 19 24</a><br>
								<span class="text-capitalize font-heading"><a href="tel:34660074797">+ 34 660 07 47 97</a></span>
							</div>
						</div>
						<!-- .widget end -->
					</div>
					<!-- .col-md-4 end -->
					<div class="col-xs-12 col-sm-12 col-md-8">
						<div class="row">
							<form action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'#contact-result'); return false;">
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" id="name" name="nombre" placeholder="Nombre" required/>
								</div>
								<div class="col-md-6">
									<input type="email" class="form-control mb-30" id="email" name="email" placeholder="Email" required/>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" id="telephone" name="telefono" placeholder="Teléfono" required/>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control mb-30" id="subject" name="titulo" placeholder="Tema" required/>
								</div>
								<div class="col-md-12">
									<textarea class="form-control mb-30" id="message" rows="2" name="extras[mensaje]" placeholder="Detalle del mensaje" required></textarea>
								</div>
								<label for="" style="font-weight: 300; margin-bottom: 20px;margin-left: 16px; font-size: 13px;">
									<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
								</label>
								<div class="col-md-12">
									<button type="submit" id="submit-message" class="btn btn-primary btn-black btn-block">Enviar Mensaje</button>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 mt-xs">
									<!--Alert Message-->
									<div id="contact-result">
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- .col-md-8 end -->
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script> 
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript">

	var styleCluster = 
	[
		{url:URL+'img/clusters/m1.png',textColor:'white',width:'53',height:'52',textSize:'18'},
		{url:URL+'img/clusters/m2.png',textColor:'white',width:'56',height:'55',textSize:'18'},
		{url:URL+'img/clusters/m3.png',textColor:'white',width:'66',height:'65',textSize:'18'},
		{url:URL+'img/clusters/m4.png',textColor:'white',width:'78',height:'77',textSize:'18'},
		{url:URL+'img/clusters/m5.png',textColor:'white',width:'90',height:'89',textSize:'18'}
	];
	var franqs = [];
	var content = document.getElementById('googleMap');
	var opts = {
		center: new google.maps.LatLng(40.2040038,-2.8388287),
		zoom:5,
		styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
	};

	var map = new google.maps.Map(content,opts);	
	<?php foreach($this->elements->franquicias()->result() as $i=>$f): ?>		
		
		franqs[<?= $i ?>] = new google.maps.Marker({
			position:new google.maps.LatLng('<?= $f->mapa[0] ?>','<?= $f->mapa[1] ?>'),
			map:map,
			icon:'<?= $f->confianza==1?base_url('img/map-marker2.png'):base_url('img/map-marker.png') ?>'
		});
		franqs[<?= $i ?>].contenido = '<b><?= $f->nombre ?></b><br/>Tel: +34<?= $f->telefono ?><br/>Email: <?= $f->email ?>';
		franqs[<?= $i ?>].infowindow = new google.maps.InfoWindow({
          content: franqs[<?= $i ?>].contenido
        });
		franqs[<?= $i ?>].addListener('click', function() {
          this.infowindow.open(map, franqs[<?= $i ?>]);
        });        
	<?php endforeach ?>
	var markerCluster = new MarkerClusterer(map, franqs,{imagePath: URL+'img/clusters/m','styles':styleCluster});
</script>