
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>6. Datos del
			<span class="color-theme"> solicitante</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row">
	<form method="post" onsubmit="return selDatos(this)">
		<!-- .col-md-4 end -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2>¿Quien solicita?</h2>
			<div class="row">			
				<div class="col-md-12">
					<input type="text" id="nombre" class="form-control mb-30" name="nombre" id="name" placeholder="Nombre" value="<?= $this->user->nombre ?>" readonly="true"/>
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="apellidos" id="apellidos" placeholder="Apellidos" value="<?= $this->user->apellidos ?>" readonly="true"/>
				</div>				
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="telefono" id="telephone" placeholder="Teléfono" value="<?= $this->user->telefono ?>" readonly="true"/>
				</div>
				<div class="col-md-12">
					<input type="email" class="form-control mb-30" name="email" id="email" placeholder="Email" value="<?= $this->user->email ?>" readonly="true"/>
				</div>
			</div>
		</div>
		<!-- .col-md-8 end -->
		<!-- .col-md-4 end -->
		
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2>Orden de trabajo</h2>
			<div class="row">
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="orden" id="orden" placeholder="Numero de orden" value="<?= @$_SESSION['carrito'][0]->datos['orden'] ?>" />
				</div>
				<div class="col-md-12">
					<textarea name="datos_reparacion" id="datos_reparacion" cols="30" rows="10" class="form-control mb-30" placeholder="Datos de reparación" style="height:134px"><?= @$_SESSION['carrito'][0]->datos['datos_reparacion'] ?></textarea>					
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" id="provincia" value="" readonly="" style="background-color: transparent;" />
					<input type="hidden" name="provincia" id="provinciaHid" value="">
				</div>				
			</div>
		</div>
		<!-- .col-md-8 end -->
		
		<div class="msj"></div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<button type="submit" id="submit-message" class="btn btn-primary btn-black btn-block">Registrar</button>
		</div>
	</form>
</div>
<!-- .row end -->


