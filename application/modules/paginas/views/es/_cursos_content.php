<div class="service-img" style="margin-bottom:0px">
	<div class="project-carousel" style="margin-bottom: 0">
		<div class="item">
			<img src="<?= $c->foto ?>" alt="Project">
		</div>
	</div>
</div>
<div class="col-xs-12 col-md-12 pl-0 top-bar" style=" margin-bottom:30px;">
	<ul class="list-inline top-widget" style="margin-left:0px;">
		<li class="top-social">
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-vimeo"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
		</li>
	</ul>
</div>
<div class="col-xs-12 col-md-12 pl-0">
	<div class="service-content">
		<h3><?= $c->nombre ?> "<?= $c->subtitulo ?>"</h3>
		<div class="row" style="margin-left:0; margin-right:0">
			<div class="col-xs-12 col-md-6 pl-0">
				<?= $c->texto_col1 ?>
			</div>
			<div class="col-xs-12 col-md-6 pr-0">
				<?= $c->texto_col2 ?>
			</div>
		</div>
		
	</div>
</div>
<?php $this->load->view('_cursos_fechas',array('c'=>$c)); ?>