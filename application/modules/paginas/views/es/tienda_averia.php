<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>4. Terminamos,
			<span class="color-theme"> averia de tu dispositivo</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row contenido">
	
	<!-- product #1 -->
	<div class="col-xs-6 col-sm-6 col-md-3 product-item clearfix">
		<div class="product-img link">
			<span class="checkboxRep"></span>
			<input type="checkbox" name="averia[]" value="" id="averia1" class="averias" style="display: none">
			<img src="<?= base_url() ?>theme/theme/assets/images/ser/1.jpg" alt="product">
			<div class="product-hover">
				<div class="product-cart">
					<a class="btn btn-secondary btn-block link" href="#">Seleccionar</a>
				</div>
			</div>
		</div>
		<!-- .product-img end -->
		<div class="product-bio">
			<p class="product-price">HUELLA</p>
			<h4>
			<a class="precio" href="#" style="font-size:16px"><?= rand(0,20) ?>€</a>
			</h4>
			<a class="btn btn-secondary descripcion_botom" href="#descripcionProblema" data-toggle="modal">Descripción del problema</a>
		</div>
		<!-- .product-bio end -->
	</div>
	
	
	
	
</div>

<div class="row">
	<div class="col-xs-12 col-md-12 msjAveria"></div>
	<div class="col-xs-12 col-md-12">
		<a href="javascript:doneAveria()" class="btn btn-primary btn-black btn-block">Confirmar</a>
	</div>
</div>
<!-- .row end -->