[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/222.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>Blog</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">noticias</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Blogs
============================================= -->
<section>
	<div class="container">
		<div class="row bloglist">
			
			[foreach:blog]
				<!-- Entry Article #1 -->
				<div class="col-xs-12 col-sm-6 col-md-4 entry">
					<div class="entry-img">
						<a href="[link]">
							<img src="[foto]" alt="title"/>
						</a>
					</div>
					<!-- .entry-img end -->
					<div class="entry-meta clearfix">
						<ul class="pull-left">
							<li class="entry-format">
								<i class="fa fa-pencil"></i>
							</li>
							<li class="entry-date">
								[fecha]
							</li>
						</ul>
						<ul class="pull-right text-right">
							<li class="entry-cat">Por:
								<span>
								<a href="#">[user]</a>
								</span>
							</li>								
							<li class="entry-views"><i class="fa fa-eye"></i> [vistas]</li>
						</ul>
					</div>
					<!-- .entry-meta end -->
					<div class="entry-title">
						<h3>
							<a href="[link]">[titulo]</a>
						</h3>
					</div>
					<!-- .entry-title end -->
					<div class="entry-content">
						<p>[texto]</p>
						<a class="entry-more" href="[link]">
							<i class="fa fa-plus"></i>
							<span>ver noticia</span>
						</a>
					</div>
					<!-- .entry-content end -->
				</div>
				<!-- .entry end -->
			[/foreach]
			[paginacion]			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]