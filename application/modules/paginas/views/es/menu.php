<div class="preloader">
	<div class="spinner">
		<div class="bounce1">
		</div>
		<div class="bounce2">
		</div>
		<div class="bounce3">
		</div>
	</div>
</div>
<header id="navbar-spy" class="full-header">
	<div id="top-bar" class="top-bar">
		<div class="container">
			<div class="row">
				<div class="hidden-sm col-xs-12 col-sm-12 col-md-6 hidden-xs">
					<ul class="list-inline top-contact">
						<li>
							<p>
								<a href="tel:34938031924">
									<i class="fa fa-phone"></i>Teléfono:
									<span>+34 93 803 19 24</span>
								</a>
							</p>
						</li>
						<li>
							<p>
								<a href="mailto:movilessat@movilessat.es">
									<i class="fa fa-envelope"></i>
									<span>movilessat@movilessat.es</span>
								</a>
							</p>
						</li>
						<li>
							<strong>
								<a href="<?= base_url() ?>faq.html">
									FAQ
								</a>
							</strong>
						</li>
					</ul>
				</div>
				<!-- .col-md-6 end -->
				<div class="col-xs-12 col-sm-12 col-md-6 text-right">
					<ul class="list-inline top-widget">
						<li class="top-social hidden-xs">
							<a href="https://www.facebook.com/movilessat/"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/movilessat"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/moviles_sat"><i class="fa fa-instagram"></i></a>
						
						</li>
						<li>
							<a class="button-quote" href="<?= base_url() ?>tienda.html" id="hmodelquote" style=" font-weight: 600;"><i class="fa fa-wrench"></i>  REPARA TU MÓVIL</a>
							<?php if(empty($_SESSION['user'])): ?>
								<a class="button-quote reverse" href="<?= base_url() ?>area-privada.html" id="hmodelquote" style=" font-weight: 600;"><i class="fa fa-lock"></i>  ZONA PRIVADA</a>
							<?php else: ?>
								<div class="dropdown button-quote reverse">
								  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fa fa-user"></i> Hola, <?= $this->user->nombre.' '.$this->user->apellidos ?>
								    <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" aria-labelledby="dLabel">
								    <li><a href="<?= base_url('area-privada.html') ?>">ZONA PRIVADA</a></li>
								    <li><a href="<?= base_url('main/unlog') ?>">SALIR</a></li>
								  </ul>
								</div>						
							<?php endif ?>
							<!-- Modal -->
							<div class="modal fade model-quote" id="hmodel-quote" tabindex="-1" role="dialog" aria-labelledby="hmodelquote">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
											<div class="model-icon">
												<i class="lnr lnr-apartment"></i>
											</div>
											<div class="model-divider">
												<div class="model-title">
													<p>¿TIENES DUDAS?</p>
													<h6>RELLENA Y ENVIA</h6>
												</div>
											</div>
										</div>
										<!-- .model-header end -->
										<div class="modal-body">
											<form id="head-quote-form" action="<?= base_url() ?>theme/theme/assets/php/sendheadquote.php" method="post">
												<input type="text" class="form-control" name="quote-name" id="quote-name" placeholder="Nombre" required/>
												<input type="email" class="form-control" name="quote-email" id="quote-email" placeholder="Email" required/>
												<input type="text" class="form-control" name="quote-telephone" id="quote-telephone" placeholder="Telefóno" required/>
												<textarea class="form-control" name="quote-message"  id="quote-message" placeholder="Mensaje" rows="2" required></textarea>
												<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
												<!--Alert Message-->
												<div id="head-quote-result" class="mt-xs">
												</div>
											</form>
										</div>
										<!-- .model-body end -->
									</div>
								</div>
							</div>
							<!-- .model-quote end -->
						</li>
					</ul>
				</div>
				<!-- .col-md-6 end -->
			</div>
		</div>
	</div>
	<nav id="primary-menu" class="navbar navbar-fixed-top style-1">
		<div class="row">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="logo" href="<?= base_url() ?>">
						<img src="<?= base_url() ?>theme/theme/assets/images/logo/logo-dark.png" alt="Yellow Hats">
					</a>
				</div>
				
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						<li <?= empty($link)?'class="active"':'' ?>>
							<a href="<?= base_url() ?>">Inicio</a>							
						</li>
						<!-- li end -->
						<li <?= !empty($link) && $link=='nosotros'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>nosotros.html">Empresa</a>							
						</li>
						<li <?= !empty($link) && $link=='servicios'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>servicios.html">Servicios</a>							
						</li>
						<li <?= !empty($link) && $link=='cursos'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>cursos.html">Cursos</a>							
						</li>
						<!-- li end -->
						<li <?= !empty($link) && $link=='franquicias'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>franquicias.html">Franquicias</a>							
						</li>
						<!-- li end -->
						<li <?= !empty($link) && $link=='blog'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>blog">Blog</a>							
						</li>
						<!-- li end -->
						<li <?= !empty($link) && $link=='contacto'?'class="active"':'' ?>>
							<a href="<?= base_url() ?>contacto.html">Contacto</a>							
						</li>
						<!-- li end -->
						<li class="visible-xs">
							<a href="<?= base_url() ?>tienda.html"><i class="fa fa-shopping-cart"></i> Carrito (<?php 
									$carrito = $this->carrito->getCarrito();         
									if(count($carrito)>0){
            							echo count($carrito);
									}else{
										echo 0;        
									}
        
							?>)</a>							
						</li>
						<!-- li end -->
					</ul>
					
					<!-- Mod-->
					<div class="module module-search pull-left">
						<div class="search-icon">
							<i class="fa fa-search"></i>
							<span class="title">Buscar</span>
						</div>
						<div class="search-box">
							<form class="search-form" method="get" action="<?= base_url('paginas/frontend/search') ?>">
								<div class="input-group">
									<input type="text" name="q" class="form-control" placeholder="¿que buscas?">
									<span class="input-group-btn">
									<button class="btn" type="button"><i class="fa fa-search"></i></button>
									</span>
								</div>
								<!-- /input-group -->
							</form>
						</div>
					</div>
					<!-- .module-search-->					
					<div class="hidden-xs module module-cart pull-left menubar-cart">
						<?php $this->load->view('_carrito',array(),FALSE,'tienda'); ?>						
					</div>
					<!-- .module-cart end -->
					
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</div>
	</nav>
</header>