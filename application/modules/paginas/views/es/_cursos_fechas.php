<?php if($c->fechas->num_rows()>0): ?>
<div class="col-xs-12 col-md-12 pl-0">
	<h3 style="margin-top: 20px;margin-bottom: 0px;"> FECHAS DE NUESTROS CURSOS</h3>
	<hr style="margin-bottom: -10px;">
</div>
<?php endif ?>
<div class="col-xs-12 col-md-12 pl-0">
	
	<div class="row single-project">
		
		<?php foreach($c->fechas->result() as $f): ?>
		<div class="col-xs-12 col-md-6" style="margin-top:30px;">
			<div class=" project-block">
				<div class="project-title">
					<h3 style="margin-bottom: -10px;"><?= $c->nombre ?> "<?= $c->subtitulo ?>"</h3>
					<h4><?= $f->poblacion ?></h4>
				</div>
				<div class="project-desc">
					<ul class="list-unstyled">
						<li>Fecha inicio:
							<span><?= strftime('%d %B %Y',strtotime($f->fecha)); ?></span>
						</li>
						<li>Duración:
							<span><?= $f->duracion ?></span>
						</li>
						<li>Horario:
							<span><?= $f->horario ?></span>
						</li>
						<li>Población:
							<span><?= $f->poblacion ?></span>
						</li>
						<li>Provincia:
							<span><?= $f->provincia ?></span>
						</li>
						<h3 style=" margin-top: 10px; margin-bottom: -10px">Precio:
						<span><?= moneda($f->precio) ?></span>
						</h3>
						
						<li>&nbsp;</li>
						<li>
							<button type="submit" class="btn btn-primary btn-black btn-block" data-toggle="modal" data-target="#cursoModal<?= $f->id ?>">INSCRÍBETE</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<?php endforeach ?>
	</div>
</div>