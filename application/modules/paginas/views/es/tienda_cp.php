<div id="cp" style="background: #f5f5f5; text-align: center;">
	<div class="row" style="margin-left: 0; margin-right: 0">
		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="product-num pull-left pull-none-xs">
				<h2>0. Antes de empezar,
				<span class="color-theme"> Indícanos tu código postal</span>
				</h2>
			</div>
		</div>
		<!-- .col-md-12 end -->
	</div>
	<!-- .row end -->
	<div class="row" style="margin-left: 0; margin-right: 0">
		<div class="col-xs-12">
			<form action="" onsubmit="sendCP(this); return false;">
				<div id="cpResponse"></div>
				<div class="form-group mx-sm-3 mb-2">
				    <label for="inputPassword2" class="sr-only">Código postal</label>
				    <input type="text" class="form-control" id="cp" placeholder="00000" size="5" maxlength="5" style="text-align: center">
				    <button type="submit" class="btn btn-primary mb-2">Enviar</button>
				</div>			
			</form>
		</div>
	</div>
	<!-- .row end -->
</div>