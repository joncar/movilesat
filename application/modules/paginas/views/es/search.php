<style>
    .buscado a {
    color: #e5027d !important;
    text-decoration: none;
    cursor: pointer;
}

.ZINbbc{
  margin-bottom: 30px !important;
}

.g .r{
    padding: 0 !important;
  margin: 0 !important;
}
</style>
[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="[base_url]theme/theme/assets/images/page-title/2.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>Resultados de busqueda</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Inicio</a>
						</li>
						<li class="active">Buscador</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Services
============================================= -->
<section>
	<div class="container">
		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-12 about-1">				
				<?= $resultado ?>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]