[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/444.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>CURSOS</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">cursos</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>



<!-- Service Block #4
============================================= -->
<!-- Service Block #8
============================================= -->
<section id="service-8" class="hidden-xs service service-2 service-8 curso-service p-0">	
	
	<div class="container-fluid bg-theme">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					<!-- Tab panes -->
					<div class="tab-content">
						
						<!-- Panel #3 -->
						<div role="tabpanel" class="tab-pane fade in active" id="design2">							
							<div class="bg-section" style="background-size:60%">
								<img src="[base_url]theme/theme/assets/images/services/full/2.jpg" alt="Background"/>
							</div>
							<div class="col-md-6" style="text-align: right; margin-top: -29.1px;">
								<img src="<?= base_url() ?>theme/theme/assets/images/call/2222.png">
							</div>
							<div class="col-md-6 col-content" style="min-height:auto">
								<h3>Cursos de reparación para todo tipo de perfiles.</h3>
								<p>¿Quieres aprender? Nosotros te ayudamos. 
								<br><br>Ahora por <b>*inscribirte</b> a cualquier curso, <b>te REGALAMOS 1 mes de soporte online por videoconferencia</b>, la respuesta a tus primeras dudas después del curso.</p>
								<a class="btn btn-secondary " href="#model-quote6" data-toggle="modal">
									inscripción <i class="fa fa-plus ml-xs"></i>
								</a>
							</div>
							<!-- .col-md-6 end -->
							
						</div>
						<!-- .tab-pane end -->
						
					</div>
					<!-- .tab-content end -->
				</div>
				<!-- .row end -->
			</div>
			<!-- .col-md-12 -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container-fluid end -->
</section>

<!-- Pricing #1
============================================= -->
<section id="pricing" class="pricing pb-0">
	<div class="container">
		<div class="row">
			
			<!-- Pricing Packge #1
			============================================= -->
			<div class="col-md-4 col-sm-6 col-xs-12 price-table">
				<div class="panel">
					<!--  Packge Body  -->
					<div class="panel-body" style="background:url(<?= base_url('theme/theme/assets/images/bg/11.png');  ?>); background-size:cover;">
						<h4>
						Curso <br>L1+L2 
						</h4>
						<p>Desmontaje y Electrónica</p>
					</div>
					<ul class="list-group">
						<li class="list-group-item text-capitalize">Para los que empiezan en el sector y quieren aprender una base sólida en las reparaciones. Desmontaje y electrónica básica, imprescindible para el comienzo seguro. ¿PREPARADO?<br>INICIATE...</li>
					
					</ul>
					<!--  Packge Footer  -->
					<div class="panel-footer">
						<a class="btn btn-secondary btn-block " href="<?= base_url() ?>cursos-detalles.html">Ver fechas <i class="fa fa-calendar ml-xs"></i></a>
					</div>
				</div>
			</div>
			<!-- Package End -->
			
			<!-- Pricing Packge #2
			============================================= -->
			<div class="col-md-4 col-sm-6 col-xs-12 price-table">
				<div class="panel">
					<!--  Packge Body  -->
					<div class="panel-body" style="background:url(<?= base_url('theme/theme/assets/images/bg/12.png');  ?>); background-size:cover;">
						<h4>
						Curso <br>L3
						</h4>
						<p>Electrónica Avanzada</p>
					</div>
					<ul class="list-group">
						<li class="list-group-item text-capitalize">Aprende a identificar averías electrónicas, a interpretar los esquemáticos, realizar tus própios esquemas. Repara lo que otros no aben por falta de conocimientos. <br>"La Placa Base"<br>ANIMATE Y EMPIEZA</li>
					
					</ul>
					<!--  Packge Footer  -->
					<div class="panel-footer">
						<a class="btn btn-secondary btn-block " href="<?= base_url() ?>cursos-detalles.html">Ver fechas <i class="fa fa-calendar ml-xs"></i></a>
					</div>
				</div>
			</div>
			<!-- Package End -->
			
			<!-- Pricing Packge #3
			============================================= -->
			<div class="col-md-4 col-sm-6 col-xs-12 price-table">
				<div class="panel">
					<!--  Packge Body  -->
					<div class="panel-body"  style="background:url(<?= base_url('theme/theme/assets/images/bg/13.png');  ?>); background-size:cover;">
						<h4>
						Curso <br>L4 
						</h4>
						<p>Reballing y Reparación de Pistas</p>
					</div>
					<ul class="list-group">
						<li class="list-group-item text-capitalize">Para aquellos que tenga esta asignatura pendiente, el Reballing, Reflow y Pistas... aprende todo lo relacionado con la técnica de soldadura más preciada por muchos técnicos. Aprende a reparar líneas dañadas.</li>
					
					</ul>
					<!--  Packge Footer  -->
					<div class="panel-footer">
						<a class="btn btn-secondary btn-block " href="<?= base_url() ?>cursos-detalles.html">Ver fechas <i class="fa fa-calendar ml-xs"></i></a>
					</div>
				</div>
			</div>
			<!-- Package End -->
			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>


<section id="cta-1" class="cta pb-0">
	<div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="cta-1 bg-theme  barraNegra">
					<div class="container">
						<div class="row">
							<div class="formacionChicoCurso col-xs-2 col-sm-12 col-md-1">
								<div class="cta-img">
									<img src="<?= base_url() ?>theme/theme/assets/images/call/cta-2.png" alt="cta">
								</div>
							</div>
							<!-- .col-md-2 end -->
							<div class="col-xs-12 col-sm-12 col-md-7 cta-devider text-center-xs">
								<div class="cta-desc">
									<p>PRÓXIMA CONVOCATORIA!</p>
									<h5>¿QUIERES HACER TU FORMACIÓN Ya?.</h5>
								</div>
							</div>
							<!-- .col-md-7 end -->
							<div class="col-xs-12 col-sm-12 col-md-2 pull-right pull-none-xs">
								<div class="cta-action">
									<a class="btn btn-secondary" href="<?= base_url() ?>cursos-detalles.html">VER FECHAS</a>
									
								</div>
							</div>
							
							<!-- .col-md-2 end -->
						</div>
					</div>
				</div>
				<!-- .cta-1 end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Shortcode #4 
============================================= -->
<section id="shortcode-4" class="shortcode-4 bg-gray" style=" background-color: #3f9eb6 !important;">
	<div class="container text-center">
		<div class="row">
			
			<!-- Section Body
			============================================= -->
			<div class="col-xs-12 col-sm-3 col-md-4">
				<div class="facts-box">
					<div class="counter">
						4
					</div>
					<h4 class="text-uppercase">años formando</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-4">
				<div class="facts-box">
					<div class="counter">
						240
					</div>
					<h4 class="text-uppercase">CURSOS REALIZADOS</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-4">
				<div class="facts-box last">
					<div class="counter">
						2600
					</div>
					<h4 class="text-uppercase">ALUMNOS FORMADOS</h4>
				</div>
			</div>
			
		</div>
	</div>
</section>

<!-- Team
============================================= -->
<section id="team" class="team2 pb-0">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 mb-0-sm">
				<div class="heading heading-4">
					<div class="heading-bg heading-right">
						<p class="mb-0">APROVECHA EL TIEMPO</p>
						<h2>APROVECHA NUESTROS PACKS</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
	<div class="container-fluid">
		<div class="row">
			
			
			<?php foreach($this->elements->cursos(array('pack'=>1,'destacado'=>1))->result() as $c): ?>
				<!-- Member #2 -->
				<div class="col-xs-12 col-sm-6 col-md-4 member-2">
					<div class="member-img">
						<img src="<?= $c->foto_destacado ?>" alt="member"/>
						<div class="member-overlay">
							<div class="member-info">
								<?php if(!empty($c->regalo)): ?>
									<div class="cursoDestacadoStick">
										Regalo
										<span><?= $c->regalo ?></span>
									</div>
								<?php endif ?>
								<div class="member-bio">
									<h3>PACK <br><?= $c->nombre ?></h3>
									<p><?= $c->descripcion_corta ?></p>
								</div>
								<!-- .member-bio end -->
								<div class="member-social">
									<a href="<?= base_url() ?>cursos-detalles.html" class="btn btn-primary btn-black btn-block"> <i class="fa fa-money" style="background:transparent; color:#fff;"></i> Ver precio</a>
								</div>
								<!-- .member-social end -->
							</div>
							<!-- .member-info end -->
						</div>
						<!-- .member-overlay end -->
					</div>
					<!-- .member-img end -->
					
				</div>
				<!-- .member end -->
			<?php endforeach ?>

			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Call To Action #8
============================================= -->
<section id="cta-8" class="bg-overlay bg-overlay-theme cta cta-6" style="overflow: hidden;">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/call/11.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="text-capitalize mb-0 color-white">CURSO  CON ÉXITO SEGURO</p>
				<h2 class="mb-xs color-white">+ DE 2600 ALUMNOS SATISFECHOS QUE <br>APRENDEN DE <br>LOS MEJORES!</h2>
				<a class="btn btn-secondary btn-white" href="<?= base_url() ?>cursos-detalles.html">PRÓXIMOS CURSOS</a>				
			</div>
			<!-- .col-md-8 end -->
			<div class="col-md-5 hidden-xs hidden-sm">
				<div class="cta-img" style=" right: -95px;">
					<img src="<?= base_url() ?>theme/theme/assets/images/call/222.png" alt="call to action"/>
				</div>
			</div>
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #cta-8 end -->

<!-- Projects
============================================= -->
<section id="projects" class="projects-grid projects-3col">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-3 mb-0 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿QUÉ OPINAN NUESTROS CLIENTES?</p>
						<h2>VIDEO TESTIMONIOS</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 projects-filter">
				
			</div>
			
		</div>
		<!-- .row end -->
			<!-- Project Item #3 -->
			<?php foreach($this->elements->testimonios_videos()->result() as $t): ?>
				<div class="col-xs-12 col-sm-6 col-md-4 project-item renovation landscaping">
					<div class="project-img">
						<iframe src="<?= $t->youtube ?>?w=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:100%; height: 198px;"></iframe>				
					</div>				
				</div>
			<?php endforeach ?>
		
			
		</div>
		<!-- .row end -->
		
	</div>
	<!-- .container end -->
	
</section>

<!-- Projects
============================================= -->
<section id="projects" class="projects-grid projects-3col bg-gray">
	<div class="container ">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="heading heading-3 mb-0 text-center">
					<div class="heading-bg">
						<p class="mb-0">IMÁGENES DE LOS CURSOS MÁS RECIENTES</p>
						<h2>Galería de cursos</h2>
					</div>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
		<div class="row">
			
			<!-- Projects Filter
					============================================= -->
			<div class="col-xs-12 col-sm-12 col-md-12 projects-filter">
				
			</div>
			<!-- .projects-filter end -->
		</div>
		<!-- .row end -->
		
		<!-- Projects Item
			============================================= -->
		<div id="projects-all" class="row lightgallery">
			
			<!-- Project Item #1 -->
			<?php foreach($this->elements->galeria(1)->result() as $g): ?>
			<div class="col-xs-6 col-sm-6 col-md-4 project-item interior"  data-src="<?= $g->foto ?>">
				<div class="project-img">
					<img class="" src="<?= $g->foto ?>" alt="interior" style="width:100%;"/>
					<div class="project-hover">
						<div class="project-meta">
							<!--<h6>Interior</h6>-->
							<h4>
								<a href="<?= $g->foto ?>"><?= $g->nombre ?></a>
							</h4>
						</div>
						<div>
							<a class="img-popup" href="<?= $g->foto ?>" title="<?= $g->nombre ?>"><i class="fa fa-plus"></i></a>
						</div>
					</div>
					<!-- .project-hover end -->
				</div>
				<!-- .project-img end -->
				
			</div>
			<!-- .project-item end -->
			<?php endforeach ?>
			
			
		</div>
		<!-- .row end -->
		
	</div>
	<!-- .container end -->
	
</section>
<!-- Google Maps
============================================= -->
<section class="google-maps pb-0 pt-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 pr-0 pl-0">
				<div id="googleMap" style="width:100%;height:0px;">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- .google-maps end -->

<!-- Modal -->
<div class="modal fade model-quote" id="model-quote6" tabindex="-1" role="dialog" aria-labelledby="modelquote">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<div class="model-icon">
					<i class="lnr lnr-graduation-hat"></i>
				</div>
				<div class="model-divider">
					<div class="model-title">
						<p>NOSOTROS TE AYUDAMOS</p>
						<h6>¿QUÉ CURSO TE INTERESA?</h6>
					</div>
				</div>
			</div>
			<!-- .model-header end -->
			<div class="modal-body">
				<form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#pop-quote-result')" method="post">
					<input type="text" class="form-control" name="nombre" id="name" placeholder="Tu nombre" required/>
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
					<input type="text" class="form-control" name="telefono" id="tel" placeholder="Teléfono" required/>
					<select name="extras[curso]" class="form-control" id="">
						<option value="">Curso que te interesa</option>
						<?php $this->db->order_by('orden','ASC'); foreach($this->db->get('cursos')->result() as $c): ?>
							<option value="<?= $c->pack==1?'PACK ':'' ?><?= $c->nombre ?>"><?= $c->pack==1?'PACK ':'' ?><?= $c->nombre ?></option>
						<?php endforeach ?>
					</select>
					<input type="hidden" name="asunto" value="Información sobre cursos">
					<textarea class="form-control" name="extras[mensaje]"  id="quote" placeholder="Mensaje" rows="2" required></textarea>
					<label for="">
						<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
					</label>
					<button type="submit" class="btn btn-primary btn-black btn-block">Enviar</button>
					<!--Alert Message-->
					<div id="pop-quote-result" class="mt-xs">
					</div>
				</form>
			</div>
			<!-- .model-body end -->
		</div>
	</div>
</div>
<!-- .model-quote end -->
[footer]
<script>
	$(document).on('ready',function(){
		$('.lightgallery').lightGallery({});		
	});
</script>
