[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/2.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>SERVICIOS</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Home</a>
						</li>
						<li class="active">Servicios</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Service Block #4
============================================= -->
<section class="service service-4">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
				<div class="heading heading-2 text-center">
					<div class="heading-bg">
						<p class="mb-0">¿QUÉ PODEMOS HACER?</p>
						<h2>TODAS NUESTRAS REPARACIONES</h2>
					</div>
					<h4 class="mb-0 mt-md" style=" font-size: 20px; font-weight: normal; color: #828282; font-family: 'Droid Serif', serif; font-style: italic; line-height: 0px; }" >¿En tu casa o en nuestros talleres?</h4>
					<p class="mb-0 mt-md" style="margin-top: 25px;" >Disponemos de talleres con personal cualificado para hacer frente a todas las reparaciones de tu dispositivo, <a href="[base_url]franquicias.html">BUSCA TU TALLER MÁS CERCANO</a>. Además, puedes solicitar la reparación donde nos digas. Tú decides, visítanos en uno de nuestros talleres o <a href="[base_url]tienda.html">SOLICITA</a> la reparación in-situ.</p>
</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-6 end -->
			
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					<!-- Service Block #1 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/1.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/1w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/1w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>conector carga</h4>
				<p>Esta reparación no soluciona problemas de placa base o batería, ni ningún otro problema salvo el indicado.</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					
					<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/2.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/2w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/2w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Cambiar pantalla</h4>
				<p>Si tienes el cristal agrietado, el táctil no funciona, la pantalla no da imagen, manchas en la pantalla o barras de colores...</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					
					<!-- Service Block #3 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/3.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/3w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/3w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>daños líquidos </h4>
				<p>Averías más comunes cuando se moja un móvil: El teléfono no enciende, la pantalla no responde correctamente, no carga, cámara, micrófono, altavoz, botones, auriculares, vibración, wifi o la cobertura no funcionan de repente.</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->

					<!-- Service Block #1 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/4.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/4w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/4w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Sustitución de batería</h4>
				<p>Suele fallar por desgaste, manipulación, golpe o por fallo eléctrico. Hace falta cambiarla cuando la duración de la batería no sea la adecuada, se descargue velozmente o bien no cargue.</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					
					<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/5.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/5w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/5w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Conector auricular</h4>
								<p>Síntomas habituales del jack de auriculares.<br> No detecta el jack de audio conectado.<br>No deja conectar el jack de auriculares<br>El audio reproducido es corrupto.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					
					<!-- Service Block #3 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/6.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/6w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/6w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Cámara de foto</h4>
								<p>Selecciona esta reparación si la cámara de tu terminal no funciona correctamente o se ve borrosa.<br> Reparación en menos de una hora.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->

				<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/7.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/7w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/7w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Micrófono de llamada</h4>
								<p>Si tienes estos síntomas, se escucha flojo, distorsionado, no funciona, selecciona esta reparación si el micrófono de tu terminal no funciona correctamente. </p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/8.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/8w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/8w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Lector SIM/SD</h4>
								<p>Cuando la tarjeta SIM no es reconocida o incluso la tarjeta SD, el fallo procede de la misma bandeja donde introducimos las tarjetas.
								Reparación de poca dificultad. </p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/9.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/9w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/9w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Botones volumen</h4>
								<p>Si le cuesta bajar o subir el volumen de su smartphone, va siendo hora de revisar los pequeños pulsadores y reemplazarlos por unos nuevos.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/10.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/10w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/10w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Botón de encendido</h4>
				<p>En ocasiones tenemos que ejercer mucha presión sobre el botón de encendido, es por ello que se ha de sustituir el pulsador Power. También puede ser problema de placa base, batería o cargador.</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/11.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/11w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/11w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Errores de sistema</h4>
				<p>Los síntomas más habituales son los bloqueos del terminal, lentitud de sistema o incluso que se quede el logo o se reinicia constantemente el sistema.</p>				
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/12.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/12w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/12w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Problema de cobertura</h4>
								<p>¿Se queda sin cobertura continuamente?<br>¿No puede realizar llamadas por falta de red?<br>Nuestros técnicos revisarán desde las antenas, hasta el circuito que gestiona la radio frecuencia.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/13.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/13w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/13w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Señal wifi/bluetooth</h4>
								<p>No detecta redes wifi, lo primero es conectarse por bluetooth a otro terminal para descartar que el circuito que gestiona las conexiones no está dañado. Cambio del IC o problemas de sistema.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/14.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/14w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/14w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Pantalla Táctil</h4>
								<p>La pantalla no responde a las pulsaciones que hacemos sobre los iconos, es síntoma de que el táctil no funciona. En muchos modelos se cambia el táctil, en otros, se cambia la pantalla completa.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					<!-- .col-md-4 end -->
					<!-- .col-md-4 end -->
									<!-- Service Block #2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 service-block">
						<div class="service-img">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/grid/15.jpg" alt="icons"/>
						</div>
						<div class="service-icon">
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/15w.png" alt="icon"/>
							<img src="<?= base_url() ?>theme/theme/assets/images/services/icons/i48/15w.png" alt="icon"/>
						</div>
						<div class="service-content">
							<div class="service-desc">
								<h4>Llamada vibrador</h4>
								<p>El vibrador es el encargado de poner nuestro terminal en silencio, tenemos la opción de la vibración y así notar que nos llaman. Si no funciona te lo cambiamos en menos de 1 hora.</p>
								<a class="read-more" href="[base_url]tienda.html"><i class="fa fa-plus"></i>
									<span>ir a reparación</span>
								</a>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
			<!-- .col-md-12 end -->
			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]