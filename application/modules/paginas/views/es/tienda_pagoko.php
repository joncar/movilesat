[menu]
<!-- Page Title
============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/22.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>Resultado de pago</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Inicio</a>
						</li>
						<li class="active">Resultado de pago</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>

<!-- Shortcode #10
============================================= -->
<section id="shortcode-10" class="shortcode-10">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12 col-md-12">				
				<div class="element-title" style="text-align: center">
	                <h4>¡Algo ha salido mal!</h4>
	                <p><i class="fa fa-times fa-5x" style="color:red"></i></p>
	                <p>Hemos intentado realizar tu pago pero no hemos podido realizarlo, las posibles causas pueden ser:</p>
	                <ul>
                        <li>Se ha cancelado la compra</li>
                        <li>Se ha rechazado tu tarjeta de crédito por el banco</li>
                        <li>Saldo insuficiente en tu tarjeta de crédito</li>
                    </ul>                                            
	            </div>			            
			</div>
		</div>
		
	</div>
	<!-- .container End -->
</section>
<!-- #shortcode end-->
[footer]