<link href="<?= base_url('css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url('css/plugin.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('css/login.css') ?>" rel="stylesheet" type="text/css" />
<div class="login" style="background-image:url(<?= base_url().'img/'.$this->db->get('ajustes')->row()->fondo ?>) !important; background-size: cover; margin: 0;padding: 60px 0;">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="index.html">
            <a href="<?= site_url() ?>" class="header-logo"><img src="<?= base_url().'img/'.$this->db->get('ajustes')->row()->logo ?>" style=" width: 306px" alt="" /></a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <?php if (empty($_SESSION['user'])): ?>

	    <?php if (!empty($msj)) echo $msj ?>

	    <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>

	    <form role="form" class="login-form" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
	        <h3 class="form-title font-green">Entrar</h3>
	        <div class="form-group">
	            <label class="control-label visible-ie8 visible-ie9">Email</label>
	            <input style="padding:10px" class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" /> 
	        </div>
	        <div class="form-group">
	            <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
	            <input style="padding:10px" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="pass" /> 
	        </div>        
	        <input type="hidden" name="redirect" value="<?= empty($_GET['redirect']) ? base_url('panel') : base_url($_GET['redirect']) ?>">
	        <div class="form-actions">
	            <button type="submit" class="btn green uppercase">Entrar</button>
	            <label class="rememberme check mt-checkbox mt-checkbox-outline">
	                <input type="checkbox" name="remember" value="1" />Recordar
	                <span></span>
	            </label>
	            <p align="center"><a href="javascript:;" id="forget-password" class="">Recordar contraseña?</a></p>
	        </div>        
	        <div class="create-account">
	            <p>
	                <!--<a href="javascript:;" id="register-btn" class="uppercase">Registrar usuario</a>-->
	            </p>
	        </div>
	    </form>
	<?php else: ?>
	    <div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large" style=" width: auto; padding-top: 20px">Entrar al sistema</a></div>
	<?php endif; ?>
	<?php $_SESSION['msj'] = null ?>


        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="index.html" method="post">
            <h3 class="font-green">Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
            </div>
        </form>
    </div>
    <div class="copyright"> <?= date("Y") ?> © EVA Software. </div>
    <script src="<?= base_url() ?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/select2.full.min.js" type="text/javascript"></script>    
    <script src="<?= base_url() ?>js/login.min.js" type="text/javascript"></script>
</div>