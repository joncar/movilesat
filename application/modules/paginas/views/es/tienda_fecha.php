<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>5. ¿Donde, y cuando
				<span class="color-theme"> quieres que vayamos?</span>
			</h2>
			<h5>Disponibilidad por provincias, dias y horas de lunes a viernes de 9 a 19 horas</h5>
			<h5 style="color:#1f789a; margin-bottom: -15px;margin-top: 45px; text-align:left !important;">Selecciona una provincia</h5>
			<div class="form-group">			
				<ul class="provincias">
					<?php foreach($this->elements->provincias()->result() as $p): ?>
						<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selProvincia($(this))" data-val="<?= $p->id ?>"><?= $p->nombre ?></a></li>
					<?php endforeach ?>				
				</ul>
			</div>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row" id="calentimDiv" style="display: none;">
	
	
	<!-- product #1 -->
	<div class="col-xs-12 col-sm-12 col-md-6 clearfix" style="margin-bottom: 30px;">
		
		<h5 style="color:#1f789a;margin-top: 30px;">Selecciona una fecha</h5>
		<div class="form-group" style="display: inline-block;">			
			<div class="input-group main-cal">				
				<input id="fecha_reparacion" type="text" placeholder="Fecha de reparación" name="fecha" class="form-control" value="<?= @$_SESSION['carrito'][0]->datos['fecha'] ?>">
				<input type="hidden" name="hora" value="<?= @$_SESSION['carrito'][0]->datos['hora'] ?>" id="hora_reparacion">			
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 clearfix" style="margin-bottom: 30px;" id="horaContent">
		<h5 class="horaLabel">Selecciona una hora</h5>
		<div class="form-group">			
			<ul class="timeHora">
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">09:00 - 10:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">10:00 - 11:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">11:00 - 12:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">12:00 - 13:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">13:00 - 14:00</a></li>
				<!--<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">14:00 - 15:00</a></li>-->
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">15:00 - 16:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">16:00 - 17:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">17:00 - 18:00</a></li>
				<li><a href="javascript:void(0)" class="btn btn-secondary pull-right" onClick="selHora($(this))">18:00 - 19:00</a></li>
			</ul>
		</div>

	</div>
	<div class="msj"></div>
	<div class="col-xs-12 col-md-12">
		<a href="javascript:selFecha(1)" class="btn btn-primary btn-black btn-block">Confirmar</a>
	</div>
	
</div>
<!-- .row end -->