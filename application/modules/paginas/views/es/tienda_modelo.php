<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>2. Ahora,
			<span class="color-theme"> selecciona el modelo de tu dispositivo</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row contenido">
	
	<!-- product #1 -->
	<div class="col-xs-6 col-sm-4 col-md-3 product-item clearfix">
		<div class="product-img">
			<img src="[foto]" alt="product">	
			<div class="product-hover">
				<div class="product-cart">
					<a class="btn btn-secondary btn-block versionButton" href="#">Seleccionar</a>
				</div>
			</div>			
		</div>
		<!-- .product-img end -->
		<div class="product-bio">
			<p class="product-price">
				[nombre]				
			</p>
		</div>
		<!-- .product-bio end -->
	</div>

</div>
<!-- .row end -->