
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>6. Datos del
			<span class="color-theme"> cliente y servicio</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row">
	<form method="post" onsubmit="return selDatos(this)">
		<!-- .col-md-4 end -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2>¿Quien solicita?</h2>
			<div class="row">			
				<div class="col-md-12">
					<input type="text" id="nombre" class="form-control mb-30" name="nombre" id="name" placeholder="Nombre" value="<?= @$_SESSION['carrito'][0]->datos['nombre'] ?>" />
				</div>
				<div class="col-md-12">
					<input type="email" class="form-control mb-30" name="email" id="email" placeholder="Email" value="<?= @$_SESSION['carrito'][0]->datos['email'] ?>" />
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="telefono" id="telephone" placeholder="Teléfono" value="<?= @$_SESSION['carrito'][0]->datos['telefono'] ?>" />
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="dni" id="dni" placeholder="DNI/NIE" value="<?= @$_SESSION['carrito'][0]->datos['dni'] ?>" />
				</div>
			</div>
		</div>
		<!-- .col-md-8 end -->
		<!-- .col-md-4 end -->
		
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2>¿Donde vamos a reparar?</h2>
			<div class="row">
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="direccion" id="name" placeholder="Dirección" value="<?= @$_SESSION['carrito'][0]->datos['direccion'] ?>" />
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" name="poblacion" id="email" placeholder="Población" value="<?= @$_SESSION['carrito'][0]->datos['poblacion'] ?>" />
				</div>
				<div class="col-md-12">
					<input type="text" class="form-control mb-30" id="provincia" value="" readonly="" style="background-color: transparent;" />
					<input type="hidden" name="provincia" id="provinciaHid" value="">
				</div>				
			</div>
		</div>
		<!-- .col-md-8 end -->
		
		<div class="msj"></div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<button type="submit" id="submit-message" class="btn btn-primary btn-black btn-block">Registrar</button>
		</div>
	</form>
</div>
<!-- .row end -->


