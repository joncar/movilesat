[menu]
<section class=" bg-overlay bg-overlay-gradient pb-0">
	<div class="bg-section" >
		<img src="<?= base_url() ?>theme/theme/assets/images/page-title/222.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title title-1 text-center">
					<div class="title-bg">
						<h2>detalle noticia</h2>
					</div>
					<ol class="breadcrumb">
						<li>
							<a href="<?= base_url() ?>">Inicio</a>
						</li>
						<li>
							<a href="<?= base_url('blog') ?>">blog</a>
						</li>
						<li class="active">noticia</li>
					</ol>
				</div>
				<!-- .page-title end -->
			</div>
			<!-- .col-md-12 end -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<section class="single-post">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
				<div class="row">
					
					<!-- Entry Article #1 -->
					<div class="col-xs-12 col-sm-12 col-md-12 entry">
						<div class="entry-img">
							<a class="img-popup" href="[link]">
								<img src="[foto]" alt="title"/>
							</a>
						</div>
						<!-- .entry-img end -->
						<div class="entry-meta clearfix">
							<ul class="pull-left">
								<li class="entry-format">
									<i class="fa fa-pencil"></i>
								</li>
								<li class="entry-date">
								[fecha]
							</li>
							</ul>
							<ul class="pull-right text-right">
								<li class="entry-cat"> 									
									<span class="entry-author">Por:
									<a href="#">[user]</a>
									</span>
								</li>								
								<li class="entry-views"><i class="fa fa-eye"></i> [vistas]</li>
							</ul>
						</div>
						<!-- .entry-meta end -->
						<div class="entry-title">
							<a href="#">
								<h3>[titulo]</h3>
							</a>
						</div>
						<!-- .entry-title end -->
						<div class="entry-content">
							[texto]
						</div>
						<!-- .entry-content end -->
						
						<div class="entry-share text-right text-center-xs">
							<span class="share-title">comparte este articulo: </span>
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/share?text=<?= urlencode($detail->titulo) ?>&url=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>"><i class="fa fa-twitter"></i></a>							
						</div>
						<!-- .entry-share end -->

					</div>
					<!-- .entry end -->
					
				</div>
				<!-- .row end -->
				
			</div>
			<!-- entry articles end -->
			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
[footer]