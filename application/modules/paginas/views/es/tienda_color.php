<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>3. Queda poco,
			<span class="color-theme"> Ahora selecciona el color de tu pantalla</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row contenido">
	
	<!-- product #1 -->
	<div class="col-xs-6 col-sm-4 col-md-3 product-item clearfix">
		<div class="product-img link">
			<div class="colorePreview" style="width:100%; height:150px; background:black; border: 2px solid black;"></div>
			<div class="product-hover">
				<div class="product-cart">
					<a class="btn btn-secondary btn-block link" href="javascript:selColor(1)">Seleccionar</a>
				</div>
			</div>
		</div>
		<!-- .product-img end -->
		<div class="product-bio">
			<p class="product-price">[nombre]</p>			
		</div>
		<!-- .product-bio end -->
	</div>
	<!-- .product-item clearfix end -->
	
</div>
<!-- .row end -->