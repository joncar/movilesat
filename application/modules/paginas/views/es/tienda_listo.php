<?php if(!empty($_SESSION['carrito'])): $car = $_SESSION['carrito'][0] ?>
<div class="col-xs-12 visible-xs" style="text-align: center;margin-bottom: 30px;">
	<a class="btn btn-secondary" href="#datos_cliente" data-toggle="tab" style="width: auto;padding: 0 20px;">Volver a seleccionar datos</a>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="product-num pull-left pull-none-xs">
			<h2>7. Ya casi 
			<span class="color-theme"> estamos terminando</span>
			</h2>
		</div>
	</div>
	<!-- .col-md-12 end -->
</div>
<!-- .row end -->
<div class="row">
	<div class="col-xs-6 col-md-6">
		<div class="cart-total-amount" style="display: inline-block">
			<h6>Tu dispositivo</h6>
			<div class="cart-product-img">
				<img src="<?= $car->modelo->foto ?>" alt="product" style="width:100%;">
			</div>
			<div class="cart-product-name">
				<h6><?= $car->modelo->nombre ?> <?= mb_strtolower($car->datos['color']) ?></h6>				
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6 cart-total-amount-parent"  style="margin-bottom: 60px;">
		<div class="cart-total-amount">
			<h6>Datos de contacto</h6>
			<ul class="list-unstyled" style="margin:4px">
				<li>Nombre :
					<span class="pull-right text-right"><?= $car->datos['nombre'] ?></span>
				</li>
				<li>Dirección :
					<span class="pull-right text-right"><?= $car->datos['direccion'] ?>, <?= $car->datos['poblacion'] ?></span>
				</li>
				<li>Provincia :
					<span class="pull-right text-right"><?= $this->db->get_where('provincias',array('id'=>$car->datos['provincia']))->row()->nombre ?></span>
				</li>
				<li>Dia y hora :
					<span class="pull-right text-right"><?= $car->datos['fecha'].' '.$car->datos['hora'] ?></span>
				</li>
				<li>Email :
					<span class="pull-right text-right"><?= $car->datos['email'] ?></span>
				</li>
				
				
			</ul>
		</div>
	</div>
</div>
<div class="row" style="margin-top:20px;">
	<div class="col-xs-12 mt-20">
		<h3>Reparaciones seleccionadas y precio</h3>
		<div class="cart-table table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr class="cart-product">
						<th class="cart-product-item">Equipo</th>
						<th class="hidden-xs cart-product-price">Precio</th>
						<th class="hidden-xs cart-product-quantity">Cantidad</th>
						<th class="hidden-xs cart-product-total">Total</th>
					</tr>
				</thead>
				<tbody>
					<?php $total = 0; foreach($_SESSION['carrito'] as $c): foreach($c->averias as $a): $total+= $a->precio*$c->cantidad; ?>
						<tr class="cart-product">
							<td class="cart-product-item">								
								<div class="cart-product-img">
									<img src="<?= $c->modelo->foto ?>" alt="product" style="width:60px; margin-right:10px;"/>
								</div>
								<div class="cart-product-name">
									<h5 class="carProductTableName" style=" margin-bottom: 5px; "><?= $c->modelo->nombre ?>, <?= $c->datos['color'] ?></h5><h6 style=" color: #8f8f8f;  "><?= $a->nombre ?>
									<br>
									<?= $c->datos['fecha'] ?>___<?= $c->datos['hora'] ?><br>
									<?php if(!empty($c->datos['problema'])): ?>
										<h5 style=" margin-top: 18px; font-size: 13px;text-decoration: underline; text-transform: initial;color: #5c5c5c; margin-bottom: 5px;">
											Descripción del problema 
										</h5>
										<small style=" margin-top:-15px; font-size: 13px; text-transform: initial;color: #5c5c5c;">
											<?= $c->datos['problema'] ?>
										</small>
									<?php endif ?>
								</div>
							</td>
							<td class="hidden-xs cart-product-price"><?= moneda($a->precio) ?></td>
							<td class="hidden-xs cart-product-quantity">
								<div class="product-quantity">
									<!--<a href="#"><i class="fa fa-minus"></i></a>-->
									<input type="text" data-id="<?= $c->id ?>" value="<?= empty($c->cantidad)?1:$c->cantidad ?>" name="cantidad" class="qty">
									<!--<a href="#"><i class="fa fa-plus"></i></a>-->
								</div>
							</td>
							<td class="hidden-xs cart-product-total"><?= moneda($a->precio*$c->cantidad) ?></td>
						</tr>
					<?php endforeach ?>
					<?php endforeach ?>
					<?php 
						//$iva = $total*0.21;
						$iva = 0;
						$subtotal = $total;
						$total = $subtotal+$iva;
					?>					
					<tr class="cart-product">
						<td  colspan="3" class="cart-product-item"style="border-color: #ececec;background: #1f789a;color: white; font-size: 18px;font-weight: 600;text-transform: uppercase;olor: white;" >
							<span class="hidden-xs">Precio Final</span>
							<span class="visible-xs">Precio final: <?= moneda($total) ?></span>
						</td>					
						<td class="hidden-xs cart-product-total" style="border-color: #ececec;background: #1f789a;color: white; font-size: 18px;font-weight: 600;text-transform: uppercase;olor: white;"><?= moneda($total) ?></td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
	
</div>

<form id="pagoForm" action="<?= base_url('tienda/frontend/checkout') ?>" method="post" <?= $total>0?'onsubmit="return goPay()"':'' ?>>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h5 style="color:#9e9e9e">Selecciona un método de pago</h5>		
				<div class="panel-group accordion" id="accordion02" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default" style="padding:0 20px;">
						<div class="pagoAccord panel-heading " role="tab" id="headingOne">
							<div class="panel-title">
								<div class="checkbox">
									<input type="radio" name="pasarela" value="1" <?= $total>0?'checked="checked"':'' ?> style="margin-right: 20px;" <?= $total==0?'disabled="true"':'' ?>> 
									<input type="hidden" name="token" id="sometoken" value="">
									<a class="accordion-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse0" aria-expanded="false" aria-controls="collapse01" style="color:#9e9e9e">
										<i class="fa fa-credit-card-alt" style=" font-size: 140%; margin-right: 5px; margin-left: -10px;"></i>  Targeta de crédito / Débito
									</a>								
								</div>
							</div>
						</div>
						<div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" style="font-size: 13px;font-weight: 400;line-height: 20px;">
								Mediante esta modalidad de pago Móviles SAT nunca recoge ni manipula ningún dato del cliente relativo a su número de tarjeta. Todos los datos son tratados directamente por la pasarela de pago seguro de Stripe, de forma que Móviles SAT siempre pueda ofrecer la mayor seguridad, transparencia y confidencialidad en la transacción. El pago de los pedidos se realiza directamente con la entidad bancaria, ofreciéndole de este modo la máxima seguridad.
								<br><br>En Móviles SAT siempre revisamos los pedidos realizados antes de efectuar el cobro, para asegurarnos de que todo el servicio solicitado está correcto. Por ello el pedido no se paga en el momento de finalizarlo, si no que la forma de pago con tarjeta funciona de la siguiente manera:
								<br><br>En el proceso de compra selecciona "Pago con tarjeta".
								El pedido llegará a nuestro departamento de ventas, donde se revisará y comprobará que todo el  correcto.
								Una vez comprobado, aceptamos el cobro de su operación realizado mediante tarjeta
								Al finalizar el pago, comenzamos la gestión del servicio solicitado.
							</div>
						</div>
					</div>

					<div class="panel panel-default" style="padding:0 20px;">
						<div class="pagoAccord panel-heading " role="tab" id="headingOne">
							<div class="panel-title">
								<div class="checkbox">
									<input type="radio" name="pasarela" value="4" style="margin-right: 20px;" <?= $total==0?'disabled="true"':'' ?>>
									<a class="accordion-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse4" aria-expanded="false" aria-controls="collapse41" style="color:#9e9e9e">
										<i class="fa fa-paypal" style=" font-size: 140%; margin-right: 5px; margin-left: -10px;"></i>  Targeta de crédito / PAYPAL
									</a>								
								</div>
							</div>
						</div>
						<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" style="font-size: 13px;font-weight: 400;line-height: 20px;">
								Mediante esta modalidad de pago Móviles SAT nunca recoge ni manipula ningún dato del cliente relativo a su número de tarjeta. Todos los datos son tratados directamente por la pasarela de pago seguro de Stripe, de forma que Móviles SAT siempre pueda ofrecer la mayor seguridad, transparencia y confidencialidad en la transacción. El pago de los pedidos se realiza directamente con la entidad bancaria, ofreciéndole de este modo la máxima seguridad.
								<br><br>En Móviles SAT siempre revisamos los pedidos realizados antes de efectuar el cobro, para asegurarnos de que todo el servicio solicitado está correcto. Por ello el pedido no se paga en el momento de finalizarlo, si no que la forma de pago con tarjeta funciona de la siguiente manera:
								<br><br>En el proceso de compra selecciona "Pago con tarjeta".
								El pedido llegará a nuestro departamento de ventas, donde se revisará y comprobará que todo el  correcto.
								Una vez comprobado, aceptamos el cobro de su operación realizado mediante tarjeta
								Al finalizar el pago, comenzamos la gestión del servicio solicitado.
							</div>
						</div>
					</div>

					<div class="panel panel-default" style="padding:0 20px;">						
						<div class="pagoAccord panel-heading" role="tab" id="heading2">
							<div class="panel-title">
								<div class="checkbox">
									<input type="radio" name="pasarela" value="2" style="margin-right: 20px;" <?= $total==0?'disabled="true"':'' ?>>
									<a class="accordion-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse2" aria-expanded="false" aria-controls="collapse02" style="color:#9e9e9e">
										<i class="fa fa-bank" style=" font-size: 150%; margin-right: 5px; margin-left: -10px;" ></i> Transferencia o ingreso bancario
									</a>
								</div>							
							</div>
						</div>
						<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
							<div class="panel-body" style="font-size: 13px;font-weight: 400;line-height: 20px;">
								Puedes realizar dicho pago cómodamente desde tu entidad bancaria.
								Si escojes esta opción de pago y una vez finalizado su pedido, tendrá que esperar a la elaboración de la factura pro forma que le enviaremos junto a todas las instrucciones para formalizar el pago.
							</div>
						</div>
					</div>
					<?php if($total==0): ?>
						<input type="hidden" name="pasarela" value="3">
					<?php endif ?>
				</div>
			<img src="<?= base_url('img/pago-seguro.png') ?>" alt="" style="width: 340px;">
		</div>
		<div class="col-xs-12 col-md-6 optExtras">
			<h5 style="color:#9e9e9e">¿Tu dispositivo ha sido reparado antes?</h5>		
			<div class="form-group">
				<div class="checkbox">
					<input type="checkbox" style="margin-left:0" name="reparado_antes"> <label for="">Mi dispositivo ha sido reparado anteriormente</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox">
					<input type="checkbox" style="margin-left:0" name="reparado_movilesat"> <label for="">Mi dispositivo ha sido reparado en servicio movilesat</label>
				</div>
			</div>
			<div class="form-group">
				<div class="checkbox">
					<input type="checkbox" style="margin-left:0" name="reparado_oficial"> <label for="">Mi dispositivo ha sido reparado en servicio oficial</label>
				</div>
			</div>
			<label for="" style="margin-top: 35px;">
				<input type="checkbox" value="1" name="politicas"> Acepto la política de tratamiento de datos
			</label>
			<div class="form-group" style=" font-size: 13px;color: rgb(255, 135, 73);">
				Al seguir adelante estas aceptando nuestros términos de privacidad
			</div>	
		</div>
		
	</div>

	<input type="hidden" name="total" value="<?= str_replace(',','.',$total) ?>" id="totalHidden">	
	<div class="row" style="margin-top:30px;">
		<div class="col-xs-12 col-sm-12 col-md-12 p-0">
			<div id="contact-result">
				<?php 
					echo @$_SESSION['msj'];
					unset($_SESSION['msj']);
				?>
			</div>
			<button type="submit" id="submit-message" class="btn btn-primary btn-black btn-block"><?= $total>0?'Reservar y Pagar':'Realizar consulta' ?></button>			
		</div>
		<div>&nbsp;</div>		
	</div>
</form>
<?php endif ?>