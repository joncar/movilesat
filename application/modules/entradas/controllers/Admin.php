<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function faqs(){            
            $crud = $this->crud_function('','');            
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles','ca'=>'Catalan'));
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        public function equipo(){            
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/equipo','width'=>'270px','height'=>'343px'));
            $crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        } 

         public function testimonios(){            
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/testimonios','width'=>'50px','height'=>'50px'));
            $crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        public function testimonios_videos(){            
            $crud = $this->crud_function('','');                        
            $crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        public function cursos(){            
            $crud = $this->crud_function('','');            
            $crud->columns('nombre','subtitulo','pack','precio','precio_tachado','orden');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles','ca'=>'Catalan'));
            $crud->field_type('foto','image',array('path'=>'img/cursos','width'=>'870px','height'=>'200px'));
            $crud->field_type('foto_destacado','image',array('path'=>'img/cursos','width'=>'550px','height'=>'390px'));
            $crud->set_field_upload('icono','img/cursos');
            $crud->display_as('icono','ICONO (22x24)');
            $crud->add_action('Fechas','',base_url('entradas/admin/cursos_fechas').'/');
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('entradas/admin/cursos_fechas').'/{id}/add";</script>');
            $crud = $crud->render();
            $this->loadView($crud);
        }      
        
        function cursos_fechas($id){
            $crud = $this->crud_function('','');            
            $crud->field_type('cursos_id','hidden',$id)
                 ->where('cursos_id',$id)
                 ->unset_columns('cursos_id')
                 ->set_clone();
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles','ca'=>'Catalan'));            
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        public function franquicias(){            
            $crud = $this->crud_function('','');
            $crud->columns('nombre','galeria_id');            
            $crud->field_type('mapa','map',array('lat'=>'40.1488626','lan'=>'-3.3947578','width'=>'300px','height'=>'300px'));            
            $crud->field_type('redes_sociales','tags');
            $crud->field_type('foto','image',array('path'=>'img/franquicias','width'=>'870px','height'=>'526px'));
            $crud->field_type('fotos','gallery',array('path'=>'img/franquicias','width'=>'870px','height'=>'526px'));
            $crud->set_clone();
            $crud = $crud->render();
            $crud->output = $this->load->view('franquicias',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }

        public function slider(){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/slider','width'=>'2048px','height'=>'948px'))
                 ->field_type('boton1','tags')
                 ->field_type('boton2','tags')
                 ->set_order('orden');
            $crud->set_clone();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function codigos_postales(){
            $crud = $this->crud_function('',''); 
            $crud->display_as('poblacion','Provincia')
                 ->display_as('localidad','Población');
            $crud->set_import();           
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
