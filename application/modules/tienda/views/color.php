<?= $output ?>

<script>
	var row = $($("#colores_field_box .row")[0]).clone();
	function addRow(){
		var d = row.clone();
		d.find('input').val('');
		$("#colores_field_box").append(d);
		fillField()
	}

	function removeRow(el){		
		$(el).parents('.rowe').remove();
		fillField()
	}

	$(document).on('change',".colorName,.colorHex",function(){
		fillField();
	});

	function fillField(){
		var html = '';	
		var x = 0;	
		$("#colores_field_box .row").each(function(){
			var nombre = $(this).find('.colorName').val();
			var color = $(this).find('.colorHex').val();
			if(nombre!=''){
				var l = nombre+':'+color+',';
				html+= l;
			}
			x++;
			console.log(x+'=='+$("#colores_field_box .row").length);
			if(x==$("#colores_field_box .row").length){
				$("#field-colores").val(html);
			}
		});
	}
</script>