<?php
    $carrito = $this->carrito->getCarrito();     
    $total = 0;
    $impuestos = 0;
?>

<div class="cart-icon">
	<i class="fa fa-shopping-cart"></i>
	<span class="title">shop cart</span>
	<span class="cart-label">
		<?php if(count($carrito)>0): ?>
            <?= count($carrito) ?>
        <?php else: ?>
            0        
        <?php endif ?>  
	</span>
</div>
<div class="cart-box">
	<div class="cart-overview">		
		<ul class="list-unstyled">
			<?php foreach($carrito as $c): ?>
				<li>
					<img class="img-responsive" src="<?= $c->modelo->foto ?>" alt="product"/>
					<div class="product-meta">
						<h5 class="product-title"><?= $c->averia->nombre ?></h5>
						<p class="product-price">Precio: <?= moneda($c->averia->precio) ?> </p>
						<p class="product-quantity">Cantidad: <?= $c->cantidad ?></p>
					</div>
					<a class="cancel" href="javascript:remCart(<?= $c->id ?>)">cancel</a>
				</li>
			<?php $total+= ($c->cantidad*$c->averia->precio); ?>
            <?php endforeach ?>			
		</ul>

	</div>
	<?php if(count($carrito)==0): ?> 
        <span style="color:black" class="hidden-xs">Carrito Vacio</span>
    <?php endif ?>
    <?php if($total>0): ?>
		<!--<div class="cart-total">
			<div class="total-desc">
				<h5>SUBTOTAL: </h5>
			</div>
			<div class="total-price">
				<h5><?= moneda($total) ?></h5>
			</div>
		</div>
		<div class="cart-total">
			<div class="total-desc">
				<h5>IVA 21%: </h5>
			</div>
			<div class="total-price">
				<h5><?= moneda($total*0.21) ?></h5>
			</div>
		</div>-->
		<div class="cart-total" style="margin-bottom: 22px;">
	        <div class="total-desc">
	            <h5>Total: </h5>
	        </div>
	        <div class="total-price">
	            <h5><?= moneda($total) ?></h5>
	        </div>
	    </div>
		<div class="clearfix">
		</div>
		<div class="cart-control">
			<a class="btn btn-primary" href="<?= base_url() ?>tienda.html">Ver carro</a>
			<a class="btn btn-secondary pull-right" href="<?= base_url() ?>tienda.html">check out</a>
		</div>
	<?php endif ?>
</div>
