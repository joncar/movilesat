<script src="<?= base_url() ?>js/daypilot-all.min.js?v=3092" type="text/javascript"></script>  
<style type="text/css">
    .scheduler_default_corner > div:last-child{
        display:none !important;
    }
</style>


<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class=""><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Listado</a></li>
    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i> Calendario</a></li>    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane " id="home">
    	<?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane active" id="profile">
    	<div id="dp"></div>
    </div>    
  </div>

</div>

<script type="text/javascript">
    var dp = new DayPilot.Scheduler("dp");

    // view
    dp.startDate = new DayPilot.Date("<?= date("Y-m-d") ?>");  // or just dp.startDate = "2013-03-25";
    dp.days = 365;
    dp.scale = "Day"; // one day
    dp.timeHeaders = [
        { groupBy: "Month"},
        { groupBy: "Day", format: "d"}
    ]

    // bubble, sync loading
    // see also DayPilot.Event.data.staticBubbleHTML property
    dp.bubble = new DayPilot.Bubble({
        onLoad: function(args) {
            var ev = args.source;
            args.html = "Pedido " + ev.text();
        }
    });

    dp.treeEnabled = true;
    dp.rowHeaderWidth = 150;
    <?php
    	$hab = array(
            array('name'=>'09:00 - 10:00','id'=>'09:00 - 10:00'),
            array('name'=>'10:00 - 11:00','id'=>'10:00 - 11:00'),
            array('name'=>'11:00 - 12:00','id'=>'11:00 - 12:00'),
            array('name'=>'12:00 - 13:00','id'=>'12:00 - 13:00'),
            array('name'=>'13:00 - 14:00','id'=>'13:00 - 14:00'),
            array('name'=>'15:00 - 16:00','id'=>'15:00 - 16:00'),
            array('name'=>'16:00 - 17:00','id'=>'16:00 - 17:00'),
            array('name'=>'17:00 - 18:00','id'=>'17:00 - 18:00'),
            array('name'=>'18:00 - 19:00','id'=>'18:00 - 19:00'),
            array('name'=>'19:00 - 20:00','id'=>'19:00 - 20:00')
        );
    ?>
    dp.resources = <?= json_encode($hab) ?>;

    var reservas = <?php 
    	$reservas = array();    	
    	
    	foreach($this->db->get_where('tienda_ventas',array('procesado >'=>0))->result() as $r){
            $r->fecha = date("Y-m-d",strtotime(str_replace('/','-',$r->fecha)));
    		$reservas[] = $r;
    	}
    	echo json_encode($reservas);
    ?>;
    for(var i in reservas){
    	var e = new DayPilot.Event({
    		start:new DayPilot.Date(reservas[i].fecha),
    		end:new DayPilot.Date(reservas[i].fecha),
    		id:reservas[i].id,
    		resource:reservas[i].hora,
    		text:'<span style="background:red; width:100%; height:50px; display:block;"></span>',
    		primary:reservas[i].hora,
    		precio:reservas[i].total,    		
    	});
    	dp.events.add(e);
    }

    dp.eventHoverHandling = "Bubble";
        
    // event moving
    
    // event resizing

    dp.onEventResized = function (args) {
    	/*console.log(args.e);
    	send(args);
        dp.message("Los dias de la reserva se han modificado");*/
    };
    
    dp.onEventClicked = function(args) {
    	if(confirm("¿Desea editar o ver este pedido?")){
    		document.location.href="<?= base_url('tienda/admin/ventas/edit/') ?>/"+args.e.id();
    	}        
    };

    dp.onEventMoved = function (args) {
        /*send(args);
        dp.message("La reserva se ha movido");*/
    };
    /*dp.onTimeHeaderClick = function(args) {
        alert("clicked: " + args.header.start);
    };*/
    
    dp.init();

</script>