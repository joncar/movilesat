<?php

class Tienda_model extends CI_Model{

	const NOPROCESADO = '-1';
    const PORPROCESAR = '1';
    const PROCESADO = '2';


	function __construct(){
		parent::__construct();
		$this->load->model('carrito');
		$this->load->model('user');
	}	

	function cleanVentas(){
		foreach($this->db->get_where('ventas',array('user_id'=>$this->user->id,'procesado'=>self::PORPROCESAR))->result() as $v){
			$this->db->delete('ventas',array('id'=>$v->id));
			$this->db->delete('ventas_detalles',array('ventas_id'=>$v->id));
		}
	}

	function saveVenta(){
		if(!empty($_SESSION['carrito']) && count($_SESSION['carrito'])>0){
            $total = 0; 
            foreach($_SESSION['carrito'] as $c){
                foreach($c->averias as $a){
                    $total+= $a->precio*$c->cantidad;
                }
            }            
            $iva = $total*0.21;
            $subtotal = $total-$iva;
            $total = $subtotal+$iva;
            $datos = $_SESSION['carrito'][0]->datos;

            $datos['subtotal'] = str_replace(',','.',$subtotal);
            $datos['iva'] = str_replace(',','.',$iva);
            $datos['total'] = str_replace(',','.',$total);
            $datos['procesado'] = self::PORPROCESAR;
            $datos['pasarela'] = $_POST['pasarela'];
            $datos['fecha_solicitud'] = date("Y-m-d H:i:s");

            $datos['reparado_antes'] = !empty($_POST['reparado_antes'])?1:0;
            $datos['reparado_movilesat'] = !empty($_POST['reparado_movilesat'])?1:0;
            $datos['reparado_oficial'] = !empty($_POST['reparado_oficial'])?1:0;            
            $datos['averias_id'] = $_SESSION['carrito'][0]->averias[0]->id;            
            unset($datos['token']);
            unset($datos['averias']);
            $this->db->insert('tienda_ventas',$datos);        	
            $venta = $this->db->insert_id();
            foreach($_SESSION['carrito'][0]->averias as $a){
                $this->db->insert('tienda_ventas_averias',array('tienda_ventas_id'=>$venta,'averias_id'=>$a->id));
            }
            return $venta;
        }
	}

    function saveVentaEmpresa($datos){
        if(!empty($datos)){
            $data = (array)$datos->datos;
            $datos = (array)$datos;
            $total = 0; 
            foreach($datos['averias'] as $a){
                $total+= $a->precio_privado*1;
            }            
            $iva = $total*0.21;
            $subtotal = $total-$iva;
            $total = $subtotal+$iva;            
            $data['subtotal'] = str_replace(',','.',$subtotal);
            $data['iva'] = str_replace(',','.',$iva);
            $data['total'] = str_replace(',','.',$total);
            $data['procesado'] = self::PORPROCESAR;
            $data['pasarela'] = 0;
            $data['fecha_solicitud'] = date("Y-m-d H:i:s");
            $data['reparado_antes'] = 0;
            $data['reparado_movilesat'] = 0;
            $data['reparado_oficial'] = 0;            
            $data['averias_id'] = $datos['averias'][0]->id;       
            $data['marcas_id'] = $datos['marca']->id;
            $data['modelos_id'] = $datos['modelo']->id;
            $data['versiones_id'] = $datos['version']->id;
            $averias = $datos['averias'];
            unset($data['token']);
            unset($data['averias']);
            unset($data['datos']);
            unset($data['marca']);
            unset($data['modelo']);
            unset($data['version']);
            $this->db->insert('tienda_ventas_empresas',$data);          
            $venta = $this->db->insert_id();
            $this->pagoOkEmpresa($venta);
            foreach($averias as $a){
                $this->db->insert('tienda_ventas_averias_empresas',array('tienda_ventas_id'=>$venta,'averias_id'=>$a->id));
            }
            return $venta;
        }
    }

    function cotizar($id){
        $this->db->select('tienda_ventas.*, tienda_marcas.nombre as marca, tienda_modelos.nombre as modelo, tienda_versiones.nombre as version, GROUP_CONCAT(averias.nombre SEPARATOR \',\') as averia, provincias.nombre as provincia');            
        $this->db->join('provincias','provincias.id = tienda_ventas.provincia');
        $this->db->join('tienda_marcas','tienda_marcas.id = tienda_ventas.marcas_id');
        $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas.modelos_id');
        $this->db->join('tienda_versiones','tienda_versiones.id = tienda_ventas.versiones_id');
        $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
        $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
        $this->db->join('averias','averias.id = tienda_averias.averias_id');
        $producto = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
        if($producto->num_rows()>0){      
            $producto = $producto->row();                            
            $producto->total = @moneda($producto->total);                
            $producto->fecha_compra = @strftime('%d %b %Y',strtotime($producto->fecha_solicitud));
            $producto->fecha = @strftime('%d %b %Y',strtotime($producto->fecha));
            $producto->producto = $producto->marca.' modelo '.$producto->modelo.', '.$producto->version.' color '.$producto->color.' Reparar o remplazar '.$producto->averia;            
            $formas = array('1'=>'Stripe','2'=>'Transferencia','3'=>'Interesado');
            $producto->forma_pago = $formas[$producto->pasarela];            
            get_instance()->enviarcorreo($producto,15,'info@movilessat.es');
        }            
            
    }

    function enviarCorreoCliente($id){
        $this->db->select('tienda_ventas.*, tienda_marcas.nombre as marca, tienda_modelos.nombre as modelo, tienda_versiones.nombre as version, GROUP_CONCAT(averias.nombre SEPARATOR \',\') as averia, provincias.nombre as provincia');            
        $this->db->join('provincias','provincias.id = tienda_ventas.provincia');
        $this->db->join('tienda_marcas','tienda_marcas.id = tienda_ventas.marcas_id');
        $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas.modelos_id');
        $this->db->join('tienda_versiones','tienda_versiones.id = tienda_ventas.versiones_id');
        $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
        $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
        $this->db->join('averias','averias.id = tienda_averias.averias_id');
        $producto = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));        
        if($producto->num_rows()>0){      
            $producto = $producto->row();                            
            $producto->total = @moneda($producto->total);                
            $producto->fecha_compra = @strftime('%d %b %Y',strtotime($producto->fecha_solicitud));
            $producto->fecha = @strftime('%d %b %Y',strtotime($producto->fecha));
            $producto->producto = $producto->marca.' modelo '.$producto->modelo.', '.$producto->version.' color '.$producto->color.' Reparar o remplazar '.$producto->averia;            
            $formas = array('1'=>'Stripe','2'=>'Transferencia','3'=>'Interesado');
            $producto->forma_pago = $formas[$producto->pasarela];            
            get_instance()->enviarcorreo($producto,14);
            get_instance()->enviarcorreo($producto,14,'info@movilessat.es');
        }            
            
    }

	function pagoOk($id){
        if(!empty($id)){
            $this->db->select('tienda_ventas.*, tienda_marcas.nombre as marca, tienda_modelos.nombre as modelo, tienda_versiones.nombre as version, GROUP_CONCAT(averias.nombre SEPARATOR \',\') as averia, provincias.nombre as provincia');            
            $this->db->join('provincias','provincias.id = tienda_ventas.provincia');
            $this->db->join('tienda_marcas','tienda_marcas.id = tienda_ventas.marcas_id');
            $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas.modelos_id');
            $this->db->join('tienda_versiones','tienda_versiones.id = tienda_ventas.versiones_id');
            $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
            $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
            $this->db->join('averias','averias.id = tienda_averias.averias_id');
            $producto = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
            if($producto->num_rows()>0){      
                $producto = $producto->row();                            
                $producto->total = @moneda($producto->total);                
                $producto->fecha_compra = @strftime('%d %b %Y',strtotime($producto->fecha_solicitud));
                $producto->fecha = @strftime('%d %b %Y',strtotime($producto->fecha));
                $producto->producto = $producto->marca.' modelo '.$producto->modelo.', '.$producto->version.' color '.$producto->color.' Reparar o remplazar '.$producto->averia;
                
                $formas = array('1'=>'Stripe','2'=>'Transferencia','3'=>'Interesado','4'=>'Paypal');
                $producto->forma_pago = $formas[$producto->pasarela];
                get_instance()->enviarcorreo($producto,11);
                get_instance()->enviarcorreo($producto,12,'info@movilessat.es');
            }
            $this->db->update('tienda_ventas',array('procesado'=>self::PROCESADO),array('id'=>$id));
            unset($id);
        }
    }
    
    function pagoNoOk($id,$tpv = ''){
        if(!empty($id)){                            
            $this->db->select('tienda_ventas.*, tienda_marcas.nombre as marca, tienda_modelos.nombre as modelo, tienda_versiones.nombre as version, GROUP_CONCAT(averias.nombre SEPARATOR \',\') as averia, provincias.nombre as provincia');            
            $this->db->join('provincias','provincias.id = tienda_ventas.provincia');
            $this->db->join('tienda_marcas','tienda_marcas.id = tienda_ventas.marcas_id');
            $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas.modelos_id');
            $this->db->join('tienda_versiones','tienda_versiones.id = tienda_ventas.versiones_id');
            $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
            $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
            $this->db->join('averias','averias.id = tienda_averias.averias_id');
            $producto = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
            if($producto->num_rows()>0){    
                $producto = $producto->row();                
                get_instance()->enviarcorreo($producto,13,'info@movilessat.es');
                $this->db->update('tienda_ventas',array('procesado'=>self::NOPROCESADO),array('id'=>$id));
            }            
            unset($id);
        }
    }

    function pagoOkEmpresa($id){
        if(!empty($id)){
            $this->db->select('tienda_ventas_empresas.*, tienda_marcas.nombre as marca, tienda_modelos.nombre as modelo, tienda_versiones.nombre as version, GROUP_CONCAT(averias.nombre SEPARATOR \',\') as averia, provincias.nombre as provincia');            
            $this->db->join('provincias','provincias.id = tienda_ventas_empresas.provincia');
            $this->db->join('tienda_marcas','tienda_marcas.id = tienda_ventas_empresas.marcas_id');
            $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas_empresas.modelos_id');
            $this->db->join('tienda_versiones','tienda_versiones.id = tienda_ventas_empresas.versiones_id');
            $this->db->join('tienda_ventas_averias_empresas','tienda_ventas_averias_empresas.tienda_ventas_id = tienda_ventas_empresas.id');
            $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias_empresas.averias_id');
            $this->db->join('averias','averias.id = tienda_averias.averias_id');
            $producto = $this->db->get_where('tienda_ventas_empresas',array('tienda_ventas_empresas.id'=>$id));
            if($producto->num_rows()>0){      
                $producto = $producto->row();                            
                $producto->total = @moneda($producto->total);                
                $producto->fecha_compra = @strftime('%d %b %Y',strtotime($producto->fecha_solicitud));
                $producto->fecha = @strftime('%d %b %Y',strtotime(str_replace('/','-',$producto->fecha)));
                $producto->producto = $producto->marca.' modelo '.$producto->modelo.', '.$producto->version.' color '.$producto->color.' Reparar o remplazar '.$producto->averia;                
                get_instance()->enviarcorreo($producto,16);
                get_instance()->enviarcorreo($producto,16,'info@movilessat.es');
            }
            $this->db->update('tienda_ventas',array('procesado'=>self::PROCESADO),array('id'=>$id));
            unset($id);
        }
    }
}