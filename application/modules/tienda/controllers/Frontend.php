<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();       
            $this->load->model('querys');    
            $this->load->model('elements'); 
            $this->load->model('tienda_model');
            $this->load->library('form_validation'); 
        }        
        
        function addCart(){
        	$this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');            
            $this->form_validation->set_rules('telefono','Teléfono','required');   
            $this->form_validation->set_rules('direccion','Dirección','required'); 
            $this->form_validation->set_rules('poblacion','Población','required');     
            $this->form_validation->set_rules('provincia','Provincia','required');   
            $this->form_validation->set_rules('marcas_id','Marca','required');   
            $this->form_validation->set_rules('modelos_id','Modelo','required');   
            $this->form_validation->set_rules('versiones_id','Versión','required');   
            $this->form_validation->set_rules('color','Color','required');   
            $this->form_validation->set_rules('averias','Avería','required'); 
            $this->form_validation->set_rules('fecha','Fecha','required'); 
            $this->form_validation->set_rules('hora','Hora','required');
            $this->form_validation->set_rules('cp','Código Postal','required');
            if($this->form_validation->run()){                
            	$data = (object)array();            	
            	$data->datos = $_POST;  
                $_SESSION['carrito'] = array();      
                if(is_json($data->datos['averias']) && !empty(json_decode($data->datos['averias'])) && $data->datos['averias']!='[null]'){
                	$data->marca = $this->elements->marcas(array('id'=>$_POST['marcas_id']))->row();
                	$data->modelo = $this->elements->modelos(array('id'=>$_POST['modelos_id']))->row();
                	$data->version = $this->elements->versiones(array('id'=>$_POST['versiones_id']))->row();
                    $averias = $data->datos['averias'];
                    $data->averias = array();
                    foreach(json_decode($averias) as $d){
                        $data->averias[] = $this->elements->averias(array('tienda_averias.id'=>$d->id))->row();
                    }
                    $data->id = $data->datos['modelos_id'].$data->datos['versiones_id'];                
                	$data->cantidad = 1;                	
                	$_SESSION['carrito'] = empty($_SESSION['carrito'])?array():$_SESSION['carrito'];
                	$_SESSION['carrito'][] = $data;
                }
            	$car = $this->load->view($this->theme.'tienda_listo',array(),TRUE,'paginas');
            	$carm = $this->load->view('_carrito',array(),TRUE,'tienda');
            	$total = 0; 
        		foreach($_SESSION['carrito'] as $c){
                    foreach($c->averias as $a){
                        $total+= $a->precio*$c->cantidad;
                    }            		
            	}
            	//$iva = $total*0.21;
                $iva = 0;
				$subtotal = $total;
				$total = moneda($subtotal+$iva);

            	echo json_encode(array('success'=>true,'car'=>$car,'total'=>$total,'carm'=>$carm));
            }else{
            	echo json_encode(array('success'=>false,'error_message'=>$this->form_validation->error_string()));
            }
        }

        function getCart(){        	
        	$this->load->view($this->theme.'tienda_listo',array(),FALSE,'paginas');
        }


        function ver_carro(){
            $car = $this->load->view($this->theme.'tienda_listo',array(),TRUE,'paginas');
            $carm = $this->load->view('_carrito',array(),TRUE,'tienda');
            $total = 0; 
            foreach($_SESSION['carrito'] as $c){
                foreach($c->averias as $a){
                    $total+= $a->precio*$c->cantidad;
                }                   
            }
            //$iva = $total*0.21;
            $iva = 0;
            $subtotal = $total;
            $total = moneda($subtotal+$iva);
            echo json_encode(array('success'=>true,'car'=>$car,'total'=>$total,'carm'=>$carm));
        }

        function checkout(){    
            if(!empty($_SESSION['envio'])){
                $_POST = $_SESSION['envio'];
                //unset($_SESSION['envio']);
            }
            
            $this->form_validation->set_rules('pasarela','Pasarela de pago','required')
                                  ->set_rules('politicas','Politicas de privacidad','required');            
            if($this->form_validation->run() && !empty($_SESSION['carrito']) && count($_SESSION['carrito']>0)){
                unset($_POST['politicas']);

                switch($_POST['pasarela']){
                    case '1'://Stripe
                        $this->pagarStripe();
                    break;
                    case '2'://Transferencia
                        $this->pagarTransferencia();
                    break;
                    case '3'://Transferencia
                        $this->consultarVenta();
                    break;
                    case '4'://Paypal
                        $this->pagarPaypal();
                    break;
                }

            }else{
                $_SESSION['msj'] = $this->error($this->form_validation->error_string());
                header("Location:".base_url('tienda').'.html#listo');
            }

        }

        function consultarVenta(){                    
            $id = $this->tienda_model->saveVenta();
            $this->tienda_model->cotizar($id);
            unset($_SESSION['carrito']);
            redirect('tienda/frontend/pagoOk');                
        }

        function pagarTransferencia(){            
            $id = $this->tienda_model->saveVenta();
            $this->db->select('tienda_ventas.*');                        
            $detalle = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
            if($detalle->num_rows()>0){                               
                $this->tienda_model->enviarCorreoCliente($id);
                unset($_SESSION['carrito']);
                redirect('tienda/frontend/pagoOk');
            }else{
                redirect('tienda.html');
            }
        }

        function testPago($id){
            $this->tienda_model->pagoOk($id);
        }

        function pagarStripe(){            
            if(!empty($_POST['token'])){
                $token = json_decode($_POST['token']);                
                $id = $this->tienda_model->saveVenta();
                $this->db->select('tienda_ventas.*');                        
                $detalle = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
                if($detalle->num_rows()>0){                               
                    $detalle = $detalle->row();
                    $total = $detalle->total;
                    $total = trim(str_replace(',','.',$total));
                    $total*= 100;
                    require_once APPPATH.'libraries/stripe/init.php';
                    //\Stripe\Stripe::setApiKey("sk_test_KgowKrvR8TACdtiThSEHyv0o"); //Test key
                    \Stripe\Stripe::setApiKey("sk_live_6HIPGWrolOwr86C2f0Kz1Snp");

                    $charge = \Stripe\Charge::create([
                        'amount' => $total,
                        'currency' => 'EUR',
                        'source' => $token->id,
                        'receipt_email' => $token->email,
                    ]);
                    $this->db->update('tienda_ventas',array('comentario_pago'=>'Respuesta de stripe '.$charge['status']),array('id'=>$id));
                    if($charge['status']=='succeeded'){                    
                        $this->tienda_model->pagoOk($id);
                        unset($_SESSION['carrito']);
                        redirect('tienda/frontend/pagoOk');
                    }else{
                        $this->db->update('tienda_ventas',array('procesado'=>'-1'),array('id'=>$id));
                        $_SESSION['msj'] = $this->error('Pago no aprobado');
                        redirect('tienda.html');
                    }
                }else{
                    redirect('tienda.html');
                }
            }else{
                $_SESSION['msj'] = $this->error('Pago no aprobado');
                redirect('tienda.html');
            }
        }

        function pagarPaypal(){            
            $id = $this->tienda_model->saveVenta();
            $this->db->select('tienda_ventas.*,tienda_modelos.nombre as modelo');                        
            $this->db->join('tienda_modelos','tienda_modelos.id = tienda_ventas.modelos_id');
            $detalle = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$id));
            if($detalle->num_rows()>0){                               
                $detalle = $detalle->row();
                $total = $detalle->total;
                $total = trim(str_replace(',','.',$total));
                $detalle->productos = 'Reparación de '.$detalle->modelo;
                $detalle->ventas_id = $id;
                unset($_SESSION['carrito']);
                $this->load->view('goPaypal',array('total'=>$total,'detalle'=>$detalle));
            }else{
                redirect('tienda.html');
            }
        }   

        public function addToFav($prod = ''){
            if($this->user->log && !empty($prod) && is_numeric($prod) && $prod>0){                
                $favs = $this->db->get_where('favoritos',array('albums_id'=>$prod,'user_id'=>$this->user->id));
                if($favs->num_rows()==0){
                    $this->db->insert('favoritos',array('albums_id'=>$prod,'user_id'=>$this->user->id));
                }
            }
        }

        public function refreshCartForm(){
            $this->load->view('includes/fragmentos/carritoForm');
        }
        
        public function carrito($venta_id = ''){
            $this->loadView('carrito');
        }

        public function addToCart($prod = '',$cantidad = '',$return = TRUE,$sumarcantidad = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && is_numeric($cantidad) && $cantidad>0){                
                $sumarcantidad = $sumarcantidad=='0'?FALSE:$sumarcantidad;
                $this->db->where('albums.id',$prod);
                foreach($_SESSION['carrito'] as $c){
                    if($c->id==$prod){
                        $c->cantidad = $cantidad;
                        $this->carrito->setCarrito($c,$sumarcantidad);
                    }
                }                
            }
            if($return){
                $total = 0; 
                foreach($_SESSION['carrito'] as $c){
                    $total+= $c->averia->precio*$c->cantidad;
                }
                //$iva = $total*0.21;
                $iva = 0;
                $subtotal = $total;
                $total = moneda($subtotal+$iva);
                echo json_encode(array('car'=>$this->load->view('_carrito',array(),TRUE),'total'=>$total));                
            }
        }
        
        public function addToCartArray(){
            $response = 'success';
            foreach($_POST['id'] as $n=>$v){
                $this->addToCart($v,$_POST['cantidad'][$n]);
            }
            $total = 0;
            foreach($_SESSION['carrito'] as $n=>$v){                        
                $total+= ($v->precio*$v->cantidad);
            }
            echo $response;
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view($this->theme.'tienda_listo',array(),FALSE,'paginas');
        }

        public function delToCartNav($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carrito');   
        }

        function getForm(){
            $this->load->view($this->theme.'tienda_listo',array(),FALSE,'paginas');
        }

        function procesarPagoTest(){
            $this->tienda_model->pagoOk(57);
        }

        function procesarPago(){   
            //correo('joncar.c@gmail.com','TPV Response',print_r($_POST,TRUE));
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;

            //$this->tienda_model->pagoOk(47271);
            /*$_POST = array(
                'Ds_SignatureVersion' => 'HMAC_SHA256_V1',
                'Ds_Signature' => 'e8jB-l2B0Bx63nDCW8z3xpTwxplKWX5XSeIXQE_6FN4=',
                'Ds_MerchantParameters' => 'eyJEc19EYXRlIjoiMTklMkYxMiUyRjIwMTgiLCJEc19Ib3VyIjoiMTElM0E1OSIsIkRzX1NlY3VyZVBheW1lbnQiOiIxIiwiRHNfQ2FyZF9Db3VudHJ5IjoiNzI0IiwiRHNfQW1vdW50IjoiNDAwMCIsIkRzX0N1cnJlbmN5IjoiOTc4IiwiRHNfT3JkZXIiOiIwMDAyLTQ5IiwiRHNfTWVyY2hhbnRDb2RlIjoiMzQ5MDIxNTQzIiwiRHNfVGVybWluYWwiOiIwMDEiLCJEc19SZXNwb25zZSI6IjAwMDAiLCJEc19NZXJjaGFudERhdGEiOiIiLCJEc19UcmFuc2FjdGlvblR5cGUiOiIwIiwiRHNfQ29uc3VtZXJMYW5ndWFnZSI6IjEiLCJEc19BdXRob3Jpc2F0aW9uQ29kZSI6IjEzNjk1MyIsIkRzX0NhcmRfQnJhbmQiOiIxIn0='
            );*/
            if (!empty($_POST)){                
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                                
                //$decodec = $_POST["Ds_MerchantParameters"];  //Eliminar siempre que se pase a produccion                              
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){
                    //$id = substr($decodec->Ds_Order,3);    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                                    
                    $this->tienda_model->pagoOk($id);
                }else{
                    //$id = substr($decodec->Ds_Order,3);  
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                  
                    $this->tienda_model->pagoNoOk($id,$decodec);
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }

        function procesarPagoPaypal(){
            //correo('joncar.c@gmail.com','TPV Response',print_r($_POST,TRUE));
            if (!empty($_POST)){                
                $d = $_POST;                              
                if(!empty($d['payment_status']) && ($d['payment_status']=='Pending' || $d['payment_status']=='Completed') && !empty($d['item_number']) && is_numeric($d['item_number'])){
                    //$id = substr($decodec->Ds_Order,3);    
                    $id = $d['item_number'];                                    
                    $this->tienda_model->pagoOk($id);
                }else{
                    //$id = substr($decodec->Ds_Order,3);  
                    $id = $d['item_number'];                  
                    $this->tienda_model->pagoNoOk($id,$decodec);
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }

        function pagoOk($id = ''){
            $this->loadView(array('view'=>'pagoOk','title'=>'Resultado de compra'));
        }

        function pagoKo($id = ''){
            $this->loadView(array('view'=>'pagoKo','title'=>'Resultado de compra'));
        }

        function getProvinciaDisponibilidad(){
            $pr = empty($_POST['provincia'])?1:$_POST['provincia'];
            $reservas = $this->elements->getHoras($pr);
            $reservas = array('res'=>$reservas);
            $end = $reservas['res'][count($reservas['res'])-1]->fecha;
            if(empty($end)){
                $end = date("Y-m-d");
            }
            $end = date("Y-m-d");
            $end = date("Y-m-d",strtotime($end.' +14 days'));
            $reservas['end'] = $end;
            echo json_encode($reservas);
        }

        function consultarCP($response = 0){
            if($response == 0){
                $msj = array('success'=>false,'msj'=>$this->error('El servicio a domicilio no esta operativo en tu zona, puedes contactar por whatsapp o teléfono al <a href="tel:+34660074797">660074797</a> que posibilidad tenemos para ti'));
            }else{
                $msj = array('success'=>false,'msj'=>$this->error('El servicio a domicilio no esta operativo en ese código postal, ruego contactar llamando al <a href="tel:+34938235455">93 823 54 55</a> y te confirmaremos la disponibilidad'));
            }
            if(!empty($_POST['cp'])){
                if(is_numeric($_POST['cp'])){
                    $cp = $this->db->get_where('codigos_postales',array('cp'=>$_POST['cp']));
                    if($cp->num_rows()>0){
                        $msj = array('success'=>true);
                    }
                }else{
                    $msj = array('msj'=>$this->error('Solo se admiten números en el código postal'));
                }
            }
            echo json_encode($msj);
        }

        function addVentaEmpresa(){
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');            
            $this->form_validation->set_rules('telefono','Teléfono','required');   
            $this->form_validation->set_rules('orden','Numero de orden','required');             
            $this->form_validation->set_rules('provincia','Provincia','required');   
            $this->form_validation->set_rules('marcas_id','Marca','required');   
            $this->form_validation->set_rules('modelos_id','Modelo','required');   
            $this->form_validation->set_rules('versiones_id','Versión','required');   
            $this->form_validation->set_rules('color','Color','required');   
            $this->form_validation->set_rules('averias','Avería','required'); 
            $this->form_validation->set_rules('fecha','Fecha','required'); 
            $this->form_validation->set_rules('hora','Hora','required');
            $this->form_validation->set_rules('cp','Código Postal','required');
            if($this->form_validation->run()){                
                $data = (object)array();                
                $data->datos = $_POST;                     
                $car = '';             
                if(is_json($data->datos['averias']) && !empty(json_decode($data->datos['averias'])) && $data->datos['averias']!='[null]'){
                    $data->marca = $this->elements->marcas(array('id'=>$_POST['marcas_id']))->row();
                    $data->modelo = $this->elements->modelos(array('id'=>$_POST['modelos_id']))->row();
                    $data->version = $this->elements->versiones(array('id'=>$_POST['versiones_id']))->row();
                    $data->averias = json_decode($data->datos['averias']);                    
                    $venta = $this->tienda_model->saveVentaEmpresa($data);
                    $car = $this->load->view($this->theme.'tienda_listo_empresa',array('car'=>$data),TRUE,'paginas');
                }                
                echo json_encode(array('success'=>true,'car'=>$car));
            }else{
                echo json_encode(array('success'=>false,'error_message'=>$this->form_validation->error_string()));
            }
        }
    }
?>
