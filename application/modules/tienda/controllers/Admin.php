<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 

        function provincias($x = '',$y = ''){            
            $crud = $this->crud_function("","");               
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        
        function marcas($x = '',$y = ''){
        	$this->as['marcas'] = 'tienda_marcas';
            $crud = $this->crud_function("","");   
            $crud->field_type('foto','image',array('path'=>'img/tienda','width'=>'270px','height'=>'270px'))
                 ->add_action('Modelos','',base_url('tienda/admin/modelos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function modelos($x = '',$y = ''){
        	$this->as['modelos'] = 'tienda_modelos';
            $crud = $this->crud_function("","");                 
            $crud->field_type('foto','image',array('path'=>'img/tienda','width'=>'270px','height'=>'270px'))
                 ->where('tienda_marcas_id',$x)
                 ->field_type('tienda_marcas_id','hidden',$x)
                 ->unset_columns('tienda_marcas_id');
            $crud->add_action('Versiones','',base_url('tienda/admin/versiones').'/');
            $crud->set_order('orden');
            $crud = $crud->render();
            $crud->header = new ajax_grocery_crud();
            $crud->header->set_table('tienda_marcas')->set_subject('Marcas')->set_theme('header_data')->where('tienda_marcas.id',$x)->set_url('tienda/admin/marcas/');
            $crud->header = $crud->header->render(1)->output;
            $crud->output = $this->load->view('modelos',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }

        function versiones($x = '',$y = ''){
            $this->as['versiones'] = 'tienda_versiones';
            $crud = $this->crud_function("","");   
            $crud->field_type('tienda_modelos_id','hidden',$x)
                 ->where('tienda_modelos_id',$x);            
            $crud->callback_field('colores',function($val){
                $s = '<input type="hidden" name="colores" id="field-colores" value="'.$val.'">';
                $l = '<div class="row rowe">
                    <div class="col-xs-6">
                        <input type="text" class="colorName" placeholder="Nombre" value="[nombreValue]">
                    </div>
                    <div class="col-xs-6">
                        <input type="color" class="colorHex" placeholder="Color" value="[colorValue]">
                        <a href="javascript:addRow()" style="color:green">
                            <i class="fa fa-plus-circle"></i>
                        </a>
                        <a href="javascript:void(0)" onclick="removeRow(this)" style="color:red">
                            <i class="fa fa-minus-circle"></i>
                        </a>
                    </div>
                </div>';
                if(empty($val)){                    
                    $l = str_replace('[nombreValue]','',$l);
                    $l = str_replace('[colorValue]','',$l);
                }else{
                    $colores = explode(',',$val);
                    $p = '';
                    foreach($colores as $c){
                        if(!empty($c)){
                            list($nombre,$color) = explode(':',$c);
                            $pp = $l;
                            $pp = str_replace('[nombreValue]',$nombre,$pp);
                            $pp = str_replace('[colorValue]',$color,$pp);
                            $p.= $pp;
                        }
                    }
                    $l = $p;
                }
                return $s.$l;
            });
            $crud->add_action('Averias','',base_url('tienda/admin/averias').'/');
            $crud = $crud->render();
            $crud->output = $this->load->view('color',array('output'=>$crud->output),TRUE,'tienda');
            $this->loadView($crud);
        }

        function maestra_averias($x = '',$y = ''){
            $this->as['maestra_averias'] = 'averias';
            $crud = $this->crud_function("","");  
            $crud->set_subject('Averías');            
            $crud->field_type('foto','image',array('path'=>'img/tienda','width'=>'270px','height'=>'270px'));                        
            $crud = $crud->render();
            $crud->title = 'Averías';
            $this->loadView($crud);
        }

        function averias($x = '',$y = ''){
        	$this->as['averias'] = 'tienda_averias';
            $crud = $this->crud_function("","");  
            $crud->field_type('tienda_versiones_id','hidden',$x)
                 ->where('tienda_versiones_id',$x);
            $crud->set_order('orden');
            $crud->field_type('foto_real','image',array('path'=>'img/tienda','width'=>'270px','height'=>'270px'));             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function reservas($x = '',$y = ''){            
            $crud = $this->crud_function("","");             
            $crud->field_type('hora','dropdown',array(
                '09:00 - 10:00'=>'09:00 - 10:00',
                '10:00 - 11:00'=>'10:00 - 11:00',
                '11:00 - 12:00'=>'11:00 - 12:00',
                '12:00 - 13:00'=>'12:00 - 13:00',
                '13:00 - 14:00'=>'13:00 - 14:00',
                '14:00 - 15:00'=>'14:00 - 15:00',
                '15:00 - 16:00'=>'15:00 - 16:00',
                '16:00 - 17:00'=>'16:00 - 17:00',
                '17:00 - 18:00'=>'17:00 - 18:00',
                '18:00 - 19:00'=>'18:00 - 19:00',
                '19:00 - 20:00'=>'19:00 - 20:00',
            ));
            $crud->set_clone()
                 ->set_order('orden');
            $crud = $crud->render();
            $crud->output = '<div class="alert alert-info">Módulo para desactivar fechas y horas del calendario de reservas, (Si elige una fecha sin horario se deshabilita el día completo, Si la provincia esta vacia - se aplica a todas las provincias)</div>'.$crud->output;
            $this->loadView($crud);
        }

        function ventas($x = '',$y = ''){
            $this->as['ventas'] = 'tienda_ventas';
            $crud = $this->crud_function("",""); 
            get_instance()->x = $y; 
            $crud->set_relation('marcas_id','tienda_marcas','nombre')
                 ->set_relation('modelos_id','tienda_modelos','nombre')             
                 ->set_relation('versiones_id','tienda_versiones','nombre')
                 ->set_relation('averias_id','tienda_averias','averias_id')
                 ->set_relation('je1dd0c5e.averias_id','averias','nombre')
                 ->display_as('je1dd0c5e.averias_id','Avería')
                 ->display_as('marcas_id','Marca')
                 ->display_as('modelos_id','Modelo')
                 ->display_as('versiones_id','Versión')
                 ->display_as('total','Precio')
                 ->display_as('averias_id','Avería');
            $crud->columns('marcas_id','modelos_id','versiones_id','je1dd0c5e.averias_id','fecha','hora','nombre','email','total','procesado');
            $crud->callback_field('averias_id',function($val){
                if(!is_numeric(get_instance()->x)){
                    get_instance()->db->select('tienda_averias.id, averias.nombre');
                    get_instance()->db->join('averias','averias.id = tienda_averias.averias_id');
                    return form_dropdown_from_query('averias_id','tienda_averias','id','nombre',$val);
                }else{
                    $this->db->select('GROUP_CONCAT(averias.nombre SEPARATOR \',\') as nombre');                    
                    $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
                    $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
                    $this->db->join('averias','averias.id = tienda_averias.averias_id');
                    $val = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>get_instance()->x))->row()->nombre;                     
                    return form_input('averias_id',$val,'id="field-averias_id" class="form-control"');
                }
            });
            $crud->callback_column('s69d5efa9',function($val,$row){
                $this->db->select('GROUP_CONCAT(averias.nombre SEPARATOR \',\') as nombre');                    
                $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
                $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
                $this->db->join('averias','averias.id = tienda_averias.averias_id');
                return $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$row->id))->row()->nombre;
            });
            $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-info">Pagado</span>'; break;
                  case '3': return '<span class="label label-success">Enviado</span>'; break;
              }
            });
            $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Pagado'));
            $crud->field_type('pasarela','dropdown',array(1=>'Stripe','2'=>'Transferencia','3'=>'Interesado','4'=>'Paypal'));                  
            $crud = $crud->render();
            if(empty($x) || $x=='list' || $x=='success'){
                $crud->output = $this->load->view('ventas_list',array('output'=>$crud->output),TRUE);
            }
            $this->loadView($crud);
        }

        function tienda_ventas_empresas($x = '',$y = ''){            
            $crud = $this->crud_function("",""); 
            get_instance()->x = $y; 
            $crud->set_relation('marcas_id','tienda_marcas','nombre')
                 ->set_relation('modelos_id','tienda_modelos','nombre')             
                 ->set_relation('versiones_id','tienda_versiones','nombre')
                 ->set_relation('averias_id','tienda_averias','averias_id')
                 ->set_relation('je1dd0c5e.averias_id','averias','nombre')
                 ->display_as('je1dd0c5e.averias_id','Avería')
                 ->display_as('marcas_id','Marca')
                 ->display_as('modelos_id','Modelo')
                 ->display_as('versiones_id','Versión')
                 ->display_as('total','Precio')
                 ->display_as('averias_id','Avería');
            $crud->columns('marcas_id','modelos_id','versiones_id','je1dd0c5e.averias_id','fecha','hora','nombre','email','total','procesado');
            $crud->callback_field('averias_id',function($val){
                if(!is_numeric(get_instance()->x)){
                    get_instance()->db->select('tienda_averias.id, averias.nombre');
                    get_instance()->db->join('averias','averias.id = tienda_averias.averias_id');
                    return form_dropdown_from_query('averias_id','tienda_averias','id','nombre',$val);
                }else{
                    $this->db->select('GROUP_CONCAT(averias.nombre SEPARATOR \',\') as nombre');                    
                    $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
                    $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
                    $this->db->join('averias','averias.id = tienda_averias.averias_id');
                    $val = $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>get_instance()->x))->row()->nombre;                     
                    return form_input('averias_id',$val,'id="field-averias_id" class="form-control"');
                }
            });
            $crud->callback_column('s69d5efa9',function($val,$row){
                $this->db->select('GROUP_CONCAT(averias.nombre SEPARATOR \',\') as nombre');                    
                $this->db->join('tienda_ventas_averias','tienda_ventas_averias.tienda_ventas_id = tienda_ventas.id');
                $this->db->join('tienda_averias','tienda_averias.id = tienda_ventas_averias.averias_id');
                $this->db->join('averias','averias.id = tienda_averias.averias_id');
                return $this->db->get_where('tienda_ventas',array('tienda_ventas.id'=>$row->id))->row()->nombre;
            });
            $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-info">Pagado</span>'; break;
                  case '3': return '<span class="label label-success">Enviado</span>'; break;
              }
            });
            $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Pagado'));
            $crud->field_type('pasarela','dropdown',array(1=>'Stripe','2'=>'Transferencia','3'=>'Interesado','4'=>'Paypal'));                  
            $crud = $crud->render();
            if(empty($x) || $x=='list' || $x=='success'){
                $crud->output = $this->load->view('ventas_list_empresas',array('output'=>$crud->output),TRUE);
            }
            $this->loadView($crud);
        }

        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','hidden',1)
                 ->display_as('status','Activo')
                 ->where('admin = 0','ESCAPE',TRUE);
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');            
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                //$crud->field_type('cedula','hidden');
            }
            $crud->callback_before_insert(function($post){
                $post['admin'] = 0;
                $post['password'] = md5($post['password']);
                return $post;

            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>3));
                return $post;

            });
            $crud->callback_before_update(function($post,$primary){
                $post['admin'] = 0;
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->columns('nombre','apellidos','telefono','email','status');
            $output = $crud->render();
            $this->loadView($output);
        }



    }
?>
