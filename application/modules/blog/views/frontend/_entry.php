<?php
	$_GET['page'] = empty($_GET['page'])?1:$_GET['page'];
	$ant = $_GET['page']==1?base_url('blog').'?page=1':base_url('blog').'?page='.($_GET['page']-1);
	$next = $_GET['page']==1?base_url('blog').'?page=2':base_url('blog').'?page='.($_GET['page']+1);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 pager mb-30-xs mb-30-sm">
        <?php if($current_page>1): ?>
            <div class="page-prev">
                <a href="<?= $ant ?>"><i class="fa fa-angle-left"></i></a>
            </div>
        <?php endif ?>
        <?php if($total_pages > 1 || $current_page!=$total_pages): ?>
            <div class="page-next">
                <a href="<?= $next ?>"><i class="fa fa-angle-right"></i></a>
            </div>
        <?php endif ?>
    </div>
</div>