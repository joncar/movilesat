$(document).on('ready',function(){		
		$(document).on('change',"select.version",function(){
			var modelo = $(this).find('option:selected').data('modelo');
			var id = $(this).val();
			selVersion(id,modelo);
		});
	});

	function initCalendar(){
		if(cal){
			$(".calentim-input").remove();
		}
		cal = true;
		var userLanguage = navigator.language || navigator.userLanguage;
	    if(userLanguage == "en") userLanguage = "fr";
		var calentim = $("#fecha_reparacion").calentim({
			inline: true,
			singleDate: true,		
			startOnMonday: true,
			format: "DD/MM/YYYY",
			showTimePickers: false,
			locale: userLanguage,
			startDate:moment(),
			calendarCount:1,		
	        disableDays: function(day){          
	          	if(day.day() == 0 || day.day()==6){
	          		return true;
	          	}
	          	var month = parseInt(day.month())+1;
	          	var dia = parseInt(day.date());
	          	//Si ya pasaron
	          	if(day<new Date()){
	          		return true;
	          	}

	          	//Si son luego del limite
	          	if(day>end){
	          		return true;
	          	}
	          	
          		//Si no estan reservados
	          	month = month<10?'0'+month:month;
	          	dia = dia<10?'0'+dia:dia;
	          	var fecha = day.year()+'-'+month+'-'+dia;
	          	var des = false;	          	
	          	for(var i in reservas){  	          		
	          		if(reservas[i].fecha==fecha && !reservas[i].hora){
	          			des = true;
	          		}
	          	}
	          	return des;
	        },
	        onafterselect:function(){
	        	$('#hora_reparacion').val('');
				$(".timeHora a").removeClass('btn-primary').addClass('btn-secondary');			
	        	habilitarHoras();
	        	var x = $("#horaContent").offset().top;
				x-=90;
				$('html,body').animate({scrollTop:x+'px'},300);
	        }	      
		});
	}


	
	divs.modelo = $("#modelo .contenido > div").clone(); $("#modelo .contenido > div").remove();
	divs.color = $("#color .contenido > div").clone(); $("#color .contenido > div").remove();
	divs.averia = $("#averia .contenido > div").clone(); $("#averia .contenido > div").remove();

	function habilitarHoras(){
		$(".timeHora a").attr('disabled',false);        	
      	var fecha = $("#fecha_reparacion").val();
      	fecha = fecha.split('/');
      	fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
    	for(var i in reservas){          		
      		if(reservas[i].fecha==fecha && reservas[i].hora){
      			$(".timeHora a").each(function(){      				
      				if($(this).html()==reservas[i].hora){
      					$(this).attr('disabled',true);
      				}
      			});
      		}
      	}
	}
	habilitarHoras();

	function problema(){
		selecteds.problema = $("#problemaText").val();
		$("#descripcionProblema").modal('hide');
		$(".modal-backdrop").remove();
	}

	function fixScroll(){
		var x = $("#contenidoShop").offset().top;
		x-=50;
		$('html,body').animate({scrollTop:x+'px'},300);
	}
	function selMarca(id){		
		$("#marcaLink").removeClass('disabled');
		$("#modeloLink").removeClass('disabled');
		$('a[href="#modelo"]').tab('show');
		selecteds = marcas[id];	
		selecteds.problema = '';	
		$(".modalVersiones").html(marcas[id].allVersions);		
		$("#marcaLink .product-price").html(selecteds.nombre);
		$("#marcaLink img").attr('src',selecteds.foto).css({'height':'59px','width': 'auto'});

		$("#modelo .contenido").html('');
		var x = 0;
		for(var i in selecteds.modelos){
			var d = divs.modelo.clone();
			d.find('img').attr('src',selecteds.modelos[i].foto);
			d.find('.product-price').html(selecteds.modelos[i].nombre);
			var enc = false;
			first = 1;
			for(var k in selecteds.modelos[i].versiones){
				if(!enc){
					first = selecteds.modelos[i].versiones[k].id;
					enc = true;
				}
			}
			d.find('.versionButton').attr('href',"javascript:selVersion('"+first+"','"+selecteds.modelos[i].id+"')");
			/*var o = '<option>Selecciona la versión</option>';
			for(var k in selecteds.modelos[i].versiones){
				o+= '<option data-modelo="'+selecteds.modelos[i].id+'" value="'+selecteds.modelos[i].versiones[k].id+'">'+selecteds.modelos[i].versiones[k].nombre+'</option>';
			}
			d.find('select').html(o);*/
			$("#modelo .contenido").append(d);
			x++;
			if(x==4){
				$("#modelo .contenido").append('<div class="col-xs-12" style="text-align:center;margin-top: -30px;margin-bottom: 40px;"><a href="#versionMovil" data-toggle="modal">¿No sabes cual es tu modelo? Pulsa aquí</a></div>');
				x = 0;
			}
		}
		fixScroll();
	}

	function selVersion(id,modelo){		
		$("#colorLink").removeClass('disabled');
		$('a[href="#color"]').tab('show');
		var mod = Object.values(selecteds.modelos);
		selecteds.modelo = mod.find(x=>x.id == modelo);		
		selecteds.version = selecteds.modelo.versiones[id];		
		$("#modeloLink .product-price").html(selecteds.modelo.nombre+', '+selecteds.version.nombre);
		//Colores
		$("#color .contenido").html('');
		var colores = selecteds.version.colores.split(',');
		for(var i in colores){
			var color = colores[i].split(':');
			if(color[0]!=''){
				var d = divs.color.clone();
				d.find('.product-price').html(color[0]);
				d.find('.colorePreview').css('background',color[1]);
				d.find('.link').attr({'href':'javascript:selColor(\''+color[0]+'\',\''+color[1]+'\')','onclick':'selColor(\''+color[0]+'\',\''+color[1]+'\')'});
				$("#color .contenido").append(d);
			}
		}		

		//Averias
		var averias = selecteds.version.averias;		
		$("#averia .contenido").html('');
		for(var i in averias){
			var d = divs.averia.clone();
			var a = averias[i];
			d.find('input[type="checkbox"]').attr('id','averia'+a.id);
			d.find('input[type="checkbox"]').val(a.id);
			d.find('.checkboxRep').attr('id','averiaRep'+a.id);
			d.find('img').attr('src',a.foto);
			d.find('.product-price').html(a.nombre);
			d.find('.precio').html(a._precio_privado);
			d.find('.link').attr({'href':'javascript:selAveria('+a.id+')','onclick':'selAveria('+a.id+')'});
			$("#averia .contenido").append(d);
		}

		fixScroll();
	}

	function selColor(nombre,hex){		
		$("#averiaLink").removeClass('disabled');
		$("#colorLink .product-price").html(nombre);
		$("#colorLink .product-img > div").css('background',hex);
		$('a[href="#averia"]').tab('show');
		selecteds.colorNombre = nombre;
		selecteds.colorHex = hex;
		fixScroll();
	}

	function selAveria(id){		
		if(typeof selecteds.averia == 'undefined'){
			selecteds.averia = [];	
		}
		var enc = false;
		for(var i in selecteds.averia){
			if(selecteds.averia[i].id==id){
				selecteds.averia.splice(i,1);
				$(document).find('#averia'+id).prop('checked',false);
				$(document).find('#averiaRep'+id).html('');
				enc = true;
			}
		}
		if(!enc){
			var idA = -1;
			for(var i in selecteds.version.averias){
				if(selecteds.version.averias[i].id==id){
					idA = i;
				}
			}
			if(parseFloat(selecteds.version.averias[idA].precio_privado)==0){
				$(document).find('#consultarPrecio').modal('toggle');
			}else{
				selecteds.averia.push(selecteds.version.averias[idA]);
				$(document).find('#averia'+id).prop('checked',true);
				$(document).find('#averiaRep'+id).html('<i class="fa fa-check"></i>');
			}
		}		
		var nombre = '';
		for(var i in selecteds.averia){
			nombre+= ', '+selecteds.averia[i].nombre;
		}		
		nombre = nombre.substring(2);
		$("#averiaLink .product-price").html(nombre);
		$("#averiaLink img").attr('src',selecteds.averia.foto).css({'height':'59px','width': 'auto'});
		fixScroll();	
		calcularPrecio();	
	}

	function calcularPrecio(){
		var averias = selecteds.averia;
		var total = 0;
		for(var i in averias){
			total+= parseFloat(averias[i].precio_privado);
		}
		var _total = total.toFixed(2);
		_total = _total.replace('.',',');
		_total = _total.replace(',00','');
		$("#totalSidebar").html(_total+'€');
	}

	function doneAveria(){
		if(typeof selecteds.averia != 'undefined' && selecteds.averia.length>0){
			$("#fechaLink").removeClass('disabled');
			$('a[href="#fecha"]').tab('show');
		}else{
			$(".msjAveria").html('<div class="alert alert-danger">Selecciona una avería antes de continuar con el registro</div>');
		}
	}

	function selProvincia(el){
		$('#provincia').val(el.html());
		$("#provinciaHid").val(el.data('val'));
		$(".provincias a").removeClass('btn-primary').addClass('btn-secondary');
		el.removeClass('btn-secondary').addClass('btn-primary');
		$.post(URL+'tienda/frontend/getProvinciaDisponibilidad',{provincia:el.data('val')},function(data){
			data = JSON.parse(data);
			reservas = data.res;
			end = new Date(data.end);
			initCalendar();
		});
		$("#calentimDiv").show();
		
		var x = $("#calentimDiv").offset().top;
		x-=90;
		$('html,body').animate({scrollTop:x+'px'},300);
		$("#hora_reparacion").val('');
		$("#fecha_reparacion").val('');
		$(".timeHora a").removeClass('btn-primary').addClass('btn-secondary');
	}

	function selHora(el){
		if(!el.attr('disabled')){
			$('#hora_reparacion').val(el.html());
			$(".timeHora a").removeClass('btn-primary').addClass('btn-secondary');
			el.removeClass('btn-secondary').addClass('btn-primary');			
		}
	}

	function selFecha(){
		var fecha = $("#fecha_reparacion").val();
		var hora = $("#hora_reparacion").val();
		if(fecha!='' && hora!=''){
			$("#datosClienteLink").removeClass('disabled');
			$('a[href="#datos_cliente"]').tab('show');
			selecteds.fecha_reparacion = fecha;
			selecteds.hora_reparacion = hora;
			$("#fechaLink .product-price").html(selecteds.fecha_reparacion+' '+selecteds.hora_reparacion);
			$("#fecha .msj").removeClass('alert alert-danger');
			fixScroll();
		}else{
			$("#fecha .msj").addClass('alert alert-danger').html('Selecciona una fecha y una hora para hacer la visita');
		}
	}

	function selDatos(form){
		var f = new FormData(form);
		f.append('marcas_id',selecteds.id);
		f.append('modelos_id',selecteds.modelo.id)
		f.append('versiones_id',selecteds.version.id);
		f.append('color',selecteds.colorNombre);
		f.append('colorhex',selecteds.colorHex);
		f.append('averias',JSON.stringify(selecteds.averia));
		f.append('fecha',selecteds.fecha_reparacion);
		f.append('hora',selecteds.hora_reparacion);
		f.append('problema',selecteds.problema);	
		f.append('cp',$("input#cp").val());	
		remoteConnection('tienda/frontend/addVentaEmpresa',f,function(data){
			data = JSON.parse(data);
			console.log(data);
			if(data.success){
				$("#datosClienteLink .product-price").html($("#nombre").val());
				$("#datos_cliente .msj").removeClass('alert alert-danger');
				$("#listoLink").removeClass('disabled');
				$('a[href="#listo"]').tab('show');
				$("#listo").html(data.car);
				$("#totalSidebar").html(data.total);
				$(".menubar-cart").html(data.carm);
				fixScroll();
			}else{
				$("#datos_cliente .msj").addClass('alert alert-danger').html(data.error_message);
			}
		});
		
		return false;
	}

	$(document).on('change',".qty",function(){
        $.post('<?= base_url() ?>tt/addToCart/'+$(this).data('id')+'/'+$(this).val()+'/1/0',{},function(data){                        
        	data = JSON.parse(data);

            $(".menubar-cart").html(data.car);
            $("#totalSidebar").html(data.total);
            $.post('<?= base_url() ?>tt/getForm',{},function(data){
                $("#listo").html(data);
            });
        });        
    });

    //Ya existe el carrito?
    sesion = <?php 
    	if(!empty($_SESSION['carrito']) && count($_SESSION['carrito'])>0){
    		echo json_encode($_SESSION['carrito'][0]);
    	}else{
    		echo '{}';
    	}
    ?>;    
    if(typeof(sesion.datos)!=='undefined'){
    	selMarca(sesion.datos.marcas_id);
    	selVersion(sesion.datos.versiones_id,sesion.datos.modelos_id);
    	selColor(sesion.datos.color,sesion.datos.colorhex);
    	console.log(sesion);
    	if(typeof sesion.averias != 'undefined'){
    		for(var i in sesion.averias){
    			selAveria(sesion.averias[i].id);
    		}
    	}


    	selProvincia($(".provincias a[data-val='"+sesion.datos.provincia+"']"));
    	$("#fecha_reparacion").val(sesion.datos.fecha);
    	$("#hora_reparacion").val(sesion.datos.hora);
    	selFecha();    	
    	remoteConnection('tienda/frontend/ver_carro',new FormData(),function(data){
			data = JSON.parse(data);
			if(data.success){
				$("#datosClienteLink .product-price").html($("#nombre").val());
				$("#datos_cliente .msj").removeClass('alert alert-danger');
				$("#listoLink").removeClass('disabled');
				$('a[href="#listo"]').tab('show');
				$("#listo").html(data.car);
				$("#totalSidebar").html(data.total);
				$(".menubar-cart").html(data.carm);
			}else{
				$("#datos_cliente .msj").addClass('alert alert-danger').html(data.error_message);
			}
		});
    }

    <?php 
    	//$key = 'pk_test_7N7iJCn3TtQhiCKb4KlAkxYC'; 
    	$key = 'pk_live_ypyQSU4xEOkNc1sqDQdEn89p'; 	
    ?>
    function goPay(){
    	if($("#listo input[name='politicas']:checked").val()==1){
	    	if($("input[name='pasarela']:checked").val()==1){
	    		var handler = StripeCheckout.configure({
				  key: '<?= $key ?>',
				  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
				  locale: 'auto',
				  token: function(token) {
				    $("#sometoken").val(JSON.stringify(token));
				    $("#pagoForm").attr('onsubmit','');
				    $(".preloader").show();


				    $("#pagoForm").submit();
				  }
				});
	    		var total = parseFloat($("#totalHidden").val());
	    		total*= 100;
				handler.open({
				    name: 'Movilessat Technical Service S.L',
				    description: '2 widgets',
				    currency: 'eur',
				    amount: total
				 });
	    	}else{
	    		$("#sometoken").val(JSON.stringify(token));
			    $("#pagoForm").attr('onsubmit','');
			    $(".preloader").show();

			    $("#pagoForm").submit();
	    	}
    	}else{
    		$("#contact-result").html('<div class="alert alert-danger">Debes aceptar las políticas de privacidad para proceder con el pago</div>')
    	}
    	return false;
    }

    function sendCP(f){
    	$.post(URL+'tienda/frontend/consultarCP/1',{cp:$("input#cp").val()},function(data){
    		data = JSON.parse(data);
    		if(!data.success){
    			$("#cpResponse").html(data.msj);
    		}else{
    			$("#cpResponse").html('');
    			$("#codigoLink").removeClass('disabled');
    			$('a[href="#marca"]').tab('show');
    			$("div#cp").hide();
    		}
    	});
    }